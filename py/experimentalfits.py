import numpy as np
import matplotlib.pyplot as plt
import fits

# Read the experiments
height = 30e-9
Ex115 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_115C.csv", height)
Ex120 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_120C.csv", height)
Ex125 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_125C.csv", height)
Ex130 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_130C.csv", height)

# Methods performing fits on this data, giving images and information ready to
# use in tex

def show_incubationtime():
    axes = Ex120.plot_N()
    fig = axes.figure
    line = axes.lines[0]
    line.set_linestyle(":")
    axes.set_xlim(-120, 3500)
    axes.set_ylim(-5, 120)
    axes.set_title("Sb${}_2$Te, 120°C")
    fig.canvas.draw()
    fig.tight_layout()
    return fig

def fit_v(Ex=Ex130, i=2):
    vfit = Ex.velocityFit()
    vfit.fitall()
    vfit.info()
    axes = vfit.plot(i)
    Ex.plot_r(i, axes)
    [fitline, expline] = axes.lines
    expline.set_color("red")
    fitline.set_color("black")
    axes.legend()
    fig = axes.figure
    fig.tight_layout()
    return fig

def fit_v_all():
    print("$T [\\degC]$ & $v \\ [\\frac{\\text{nm}}{\\text{s}}]$")
    for T,E in zip([115,120,125,130], [Ex115,Ex120,Ex125,Ex130]):
        vfit = E.velocityFit()
        vfit.fitall()
        print("${}$ & ${:.4}+-{:.2} ({:.2})".format(T, vfit.v*1e9, vfit.uv*1e9,
              np.mean(vfit.uvs))*1e9)

def fit_J_115():
    jfit = Ex115.nucleationRateFit([1.7e3,9.7e3,20e3]).fit()
    jfit.info()
    axes = jfit.plotJ()
    axes.set_xlim(-500,31800)
    axes.set_ylim(-0.001, 0.048)
    axes.ticklabel_format(style="sci", scilimits=(0,0), useOffset=False,
                          axis="x")
    fig = axes.figure
    fig.tight_layout()
    return fig

def fit_Nc_115():
    jfit = Ex115.nucleationRateFit([1.7e3,9.7e3,20e3]).fit()
    jfit.info()
    axes = jfit.plotN()
    axes.ticklabel_format(style="sci", scilimits=(0,0), useOffset=False,
                          axis="x")
    axes.figure.tight_layout()
    return axes.figure

def comp_120():
    axes = Ex120.plot_fc()
    axes.lines[0].set_markersize(6)
    fit = Ex120.jmakFit().fit(2)
    rec = Ex120.jmakBuild(2)
    print("f_c:", fit.k, "+-", fit.uk)
    print("v & J:", rec.k, "+-", rec.uk)
    fit.plot(axes)
    rec.plot(axes)
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure

def comp_125():
    axes = Ex125.plot_fc()
    axes.lines[0].set_markersize(6)
    fit = Ex125.jmakFit().fit(2)
    rec = Ex125.jmakBuild(2)
    print("f_c:", fit.k, "+-", fit.uk)
    print("v & J:", rec.k, "+-", rec.uk)
    fit.plot(axes)
    rec.plot(axes)
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure

# Combine the measurements and perform fits to their relations

S = fits.SeriesOfMeasurements(np.array([115,120,125,130]) + 273.15,
                              [Ex115,Ex120,Ex125,Ex130], 2)
S.load()
V = fits.ArrheniusFit(S).fit()
C = fits.SimpleCntFit(S, 296.4e6, 837, V.Ea, 9e-2).fit()

def fit_cnt():
    C.info()
    fig, axes = plt.subplots(1,1)
    axes.set_xlim(384.7,405.3)
    axes.set_ylim(-1e12,4e13)
    C.plot(axes)
    axes.lines[0].set_marker("s")
    axes.legend()
    fig.tight_layout()
    return fig

def fit_arrhenius():
    V.fit().info()
    axes = V.plot()
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure

def fit_arrhenius_lin():
    V.linfit().info()
    axes = V.plot_lin()
    axes.set_xticks(np.linspace(2.48e-3, 2.58e-3, 3))
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure

def fit_mobility(L=296.4e6, Tm=837):
    mf = S.mobilityFit(lambda T: L*(Tm-T)/(2*Tm)*2*T/(Tm+T) ).fit()
    mf.info()
    axes = mf.plot()
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure


# Generate and save figures
show_incubationtime().savefig("../img/exp_incubationtime.png")
fit_v().savefig("../img/exp_v.png")
fit_J_115().savefig("../img/exp_J_115.png")
fit_Nc_115().savefig("../img/exp_Nc_115.png")
comp_120().savefig("../img/exp_jmak_120.png")
comp_125().savefig("../img/exp_jmak_125.png")
fit_cnt().savefig("../img/exp_cnt.png")
fit_arrhenius().savefig("../img/exp_v_arrhenius.png")
fit_arrhenius_lin().savefig("../img/exp_v_arrhenius_lin.png")
