"""
This file provides some general mathematical methods:
    linreg - Linear regression
    weightedmean - Weighted mean value of values with uncertainties
    chisq_ndf - Chisquared per degree of freedom for given data, function and
        parameters
    enlargedLinspace - A linspace (n evenly spaced values from a to b) enlarged
        symetrically by a factor
"""

import numpy as np

# Linear regression
def linreg(x, y, uncert_y):
    """
    Linear regression with uncertainty in y. Fits onto y=m*x+c.
    Arguments:
        x - Numpy array of x values
        y - Numpy array of y values
        uncert_y - Numpy array of uncertainties in y
    Returns: m, sigma_m, c, sigma_c, correlation, chisquared
    """
    sum_w   = np.sum(1./uncert_y**2)
    sum_x  = np.sum(x/uncert_y**2)
    sum_y  = np.sum(y/uncert_y**2)
    sum_x2 = np.sum(x**2/uncert_y**2)
    sum_xy = np.sum(x*y/uncert_y**2)
    delta = sum_w*sum_x2-sum_x**2
    m = (sum_w*sum_xy-sum_x*sum_y)/delta
    c = (sum_x2*sum_y-sum_x*sum_xy)/delta
    uncert_m = np.sqrt(sum_w/delta)
    uncert_c = np.sqrt(sum_x2/delta)
    cov = -sum_x/delta
    corr = cov/(uncert_m*uncert_c)
    chisq  = np.sum(((y-(m*x+c))/uncert_y)**2)
    return m, uncert_m, c, uncert_c, corr, chisq

def weightedmean(x, ux):
    """
    Weighted mean value of values x with uncertainties ux.
    Returns mean with uncertainty.
    """
    y = np.sum(x/ux**2)/np.sum(1/ux**2)
    uy = np.sqrt(1/np.sum(1/ux**2))
    return y, uy

def chisq_ndf(x,y,uncert_y,f,popt):
    """
    Returns the chisquared per degree of freedom for data x, y with
    uncertainty uncert_y and a function f(x,*popt) with parameters popt.
    """
    return np.sum(((y-f(x,*popt))/uncert_y)**2) / (len(x) - len(popt))

def enlargedLinspace(left,right,num=50,factor=1):
    """
    Get evenly spaced numbers over the interval from left to right, enlarged by
    a factor.
    Default factor is one, default amount of points is 50.
    """
    shift = (right-left)*(factor-1)
    return np.linspace(left-shift, right+shift, num)