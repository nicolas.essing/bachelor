"""
This file provides classes to analyse experimental or simulated data on the
crystallisation of a phase change material.
The first type of classes represent measurements:
    Measurement - Parent class for all types of measurements
    Experiment - Class for experimental results as I got from Julian
    Simulation - Class for simulation results as returned from the export
        methods in my simulation code.
The second type represents fits to several quantities:
    VelocityFit - Fit to the grain sizes
    NucleationRateFit - Fit to the number of counted nuclei
    JmakFit - Fit of a JMAK model to the crystallised fraction
    JmakBuild - Conterpart to JmakFit: Get a crystallised fraction
        from JMAK model and fit results for v and J
"""

import csv
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
from .scripts import linreg, weightedmean, chisq_ndf

NaN = float('nan')

def lininterp(a):
    """
    Linear inperpolation of an array.
    The length of the returned array is one smaller as the one of the given.
    Each element is the mean of two neighbouring elements of a.
    """
    return (a[1:] + a[:-1]) / 2

class Measurement:
    """
    A class for a measurement for one temperature as I got from Julian or as
    returned from one simulation run.
    Properties:
      times - times of measurements [s]
      nuclei - number of seen nuclei
      radii - 2D array of radii [m]
      afract - amorphous fraction [1]
      cfract - crystallized fraction [1]
    """

    def plot_fc(self, axes=None, **kwargs):
        """ Plot the crystallised fraction over time. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.plot(self.times, self.cfract, **dict({"linestyle":"none",
                  "marker":"s", "label":"data"}, **kwargs))
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$f_c$")
        return axes
        
    def plot_N(self, axes=None):
        """ Plot the counted nuclei over time. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.plot(self.times, self.nuclei, linestyle="none", marker="s",
                  label="data")
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$N$")
        return axes
    
    def plot_r(self, i, axes=None):
        """ Plot the size of the i-th nucleus over time. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.plot(self.times, self.radii[i]*1e6, linestyle="none", marker="s",
                  label="$r_{{{}}}$".format(i))
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$r \\ [\\mu m]$")
        return axes
    
    def jmakFit(self):
        """ Return an object for a JMAK fit to measurement. """
        return JmakFit(self)
    
    def velocityFit(self):
        """ Return an object for a velocity fit to measurement. """
        return VelocityFit(self)
    
    def nucleationRateFit(self, splitat=None):
        """ Return an object for a nucleation rate fit to measurement. """
        return NucleationRateFit(self,splitat)
    
    def jmakBuild(self, dimensions):
        """ 
        Return an object giving a function for the crystallised fraction
        calculated in JMAK theory with velocity and nucleation rate from fits.
        """
        return JmakBuild(self.velocityFit().fitall(),
                         self.nucleationRateFit(splitat=[]).fit(),
                         dimensions)
    
    def timeoffset(self, time):
        """
        Consider a time offset. A positive time offset adds this time up front
        with initial conditions: No grains, Nothing nucleated.
        A negative time offset will remove the time from the simulation and
        change the times accordingly.
        """
        if time > 0:
            self.times = self.times + time
            self.times = np.insert(self.times, 0, 0)
            self.nuclei = np.insert(self.nuclei, 0, 0)
            self.radii = np.insert(self.radii, 0, 0, axis=1)
            self.afract = np.insert(self.afract, 0, 1)
            self.cfract = np.insert(self.cfract, 0, 0)
        else:
            cond = self.times > -time
            self.times = np.compress(cond, self.times) + time
            self.nuclei = np.compress(cond, self.nuclei)
            self.radii = np.compress(cond, self.radii, axis=1)
            self.afract = np.compress(cond, self.afract)
            self.cfract = np.compress(cond, self.cfract)
        return self


class VelocityFit:
    """
    A class to fit a linear growht velocity to one measurement, to get
    information about the fit and to visualise it.
    Properties:
        vs  - Array of velocities for the individual grains [m/s]
        uvs - Array of uncertainties to vs [m/s]
        v   - Mean velocity [m/s]
        uv  - Uncertainty to v [m/s]
    """
    
    def __init__(self, M):
        """ Build an object for a measurement M. """
        self.M = M
        # Extract the necessary data
        self.rs = M.radii
        self.ts = M.times
        # Initialise results
        self.vs = np.zeros(len(M.radii)) * NaN
        self.uvs = np.zeros(len(M.radii)) * NaN
        self._cs = np.zeros(len(M.radii)) * NaN
        self.v, self.uv = NaN, NaN
    
    def fit(self, i, maxtime=None):
        """
        Fit a linear groth to the i-th grain.
        If the growth is not linear anymore after a time maxtime, the fit only
        takes earlier points into account.
        """
        if maxtime is None:
            maxtime = float('inf')
        # Extract points where t < tmax
        cond = np.logical_and(np.logical_not( np.isnan(self.rs[i]) ),
                              self.ts < maxtime)
        ts = np.compress(cond, self.ts)
        rs = np.compress(cond, self.rs[i])
        # Linear regression
        v, uv, c, _, _, chiq = linreg(ts, rs, np.ones(rs.shape))
        # Scale to make chisquared/ndf equals 1
        chiq = chiq/(len(ts)-2)
        uv = uv * np.sqrt(chiq)
        # Save results
        self.vs[i], self.uvs[i], self._cs[i] = v, uv, c
        return self
    
    def fitall(self):
        """ Fit all grains and calculate mean growht velocity """
        for i,_ in enumerate(self.vs):
            self.fit(i)
        self.v, self.uv = np.mean(self.vs), np.std(self.vs)
        return self
    
    def info(self):
        """ Print some information about this fit. """
        print("Velocity fit:")
        for i, (v, uv) in enumerate(zip(self.vs, self.uvs)):
            print("  {}-th grain: v={}+-{}".format(i,v,uv))
        print("mean uncertainty:", np.mean(self.uvs))
        print("overall result: v =", self.v, "+-", self.uv)
        return self
    
    def plot(self, i, axes=None):
        """ Plot the fit to the i-th grain """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        ts, rs, v, c = self.ts, self.rs[i], self.vs[i], self._cs[i]
        ts = np.compress(np.logical_not(np.isnan(rs)), ts)
        axes.plot(ts, (v*ts+c)*1e6, label="$v_{{{}}}\\cdot t$".format(i),
                  alpha=0.7)
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$r \\ [\\mu m]$")
        return axes

class NucleationRateFit:
    """
    Fit one or more nucleation rates on the data.
    Properties:
        Js - List of (J,uJ) tupels where J is a nucleation rate [1/s] and uJ
            its uncertainty
        domainboundaries - List of (tmin, tmax) tupels defining the borders of
            the domains a constant rate was fittet to
    """
    
    def __init__(self, M, splitat):
        """ Build an object for a measurement M. """
        self.M = M
        
         # If no points to split at are given, create an empty list
        if splitat is None:
            splitat = []
        # Calculate domain boundarys: Insert first time at front, cut out every
        # time thats out of available times and insert last time at end
        splitat = np.insert(splitat, 0, M.times[0])
        splitat = np.compress(splitat<M.times[-1], splitat)
        splitat = np.append(splitat,M.times[-1])
        self.domainboundaries = list(zip(splitat[0:-1], splitat[1:]))
        
        # Extract necessary data
        self._dN = np.diff(M.nuclei)
        self._dt = np.diff(M.times)
        self._tm = lininterp(M.times) # interpolated times
        self._ta = M.times            # all times
        self._fa = lininterp(M.afract)
        self._Js = self._dN/self._dt * 1/self._fa
        self._uJs = np.sqrt(np.mean(self._Js)/(self._dt * self._fa))
        
        self.Js = [(NaN,NaN)]
    
    def fit(self):
        """ Perform the fit. """
        res = []
        # For each of the domains
        for tmin, tmax in self.domainboundaries:
            # extract data in this domain
            cond = np.logical_and(self._tm >= tmin, self._tm <= tmax)
            Js_i = np.compress(cond, self._Js)
            dN_i = np.compress(cond, self._dN)
            dt_i = np.compress(cond, self._dt)
            fa_i = np.compress(cond, self._fa)
            # Assume poisson uncertainty with the mean rate on all points,
            # override the original guessed uncertainty
            uJ_i = np.sqrt(np.maximum(np.mean(dN_i),1))/dt_i * 1/fa_i
            self._uJs[cond] = uJ_i
            # Mean rate
            J, uJ = weightedmean(Js_i, uJ_i)
            res.append((J,uJ))
        self.Js = res
        return self
    
    def info(self):
        """ Print some information about this fit. """
        for (tmin, tmax), (J,uJ) in zip(self.domainboundaries, self.Js):
            print("from",tmin,"to",tmax,":")
            print("   rate:",J,"+-",uJ)
        return self
    
    def plotN(self, axes=None):
        """ Plot the integrated rates. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        Js, dt = self._Js, self._dt
        # Integrate again
        N2 = np.cumsum(Js * dt)
        N2 = np.insert(N2,0,0)
        # Assume poisson uncertainty
        uN2 = np.maximum(np.sqrt(N2), 1)
        axes.errorbar(self._ta, N2, uN2, linestyle=":", label="data")
        xs, ys = [self.domainboundaries[0][0]], [0]
        for (tmin, tmax), (J,_) in zip(self.domainboundaries, self.Js):
            xs.append(tmax)
            ys.append(ys[-1] + J*(tmax-tmin))
        plt.plot(xs, ys, label="integrated mean rate")
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$N_{corrected}$")
        return axes
    
    def plotJ(self, axes=None):
        """ Plot momentary and mean rates. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        t, Js, uJs = self._tm, self._Js, self._uJs
        axes.errorbar(t, Js, uJs, linestyle="none", marker="_")
        for (tmin, tmax), (J,_) in zip(self.domainboundaries, self.Js):
            axes.hlines(J,tmin,tmax)
        axes.set_xlabel("$t\\ [s]$")
        axes.set_ylabel("$\\frac{1}{f_a}\\frac{\\Delta N}{\\Delta t}$")
        return axes


class JmakFit:
    """
    A class to fit a JMAK model to the crystallised fraction of a measurement.
    Properties:
        n     - JMAK exponent [1]
        k     - JMAK coefficient [s^(-n)]
        un    - Uncertainty on n [1]
        uk    - Uncertainty on k [s^(-n)]
        corr  - Correlation coefficient of n and k
        chisq - Chisquared of the fit
    """
    
    def __init__(self, M):
        """ Build an object for a measurement M. """
        self.M = M
        # Initialise used values
        self.n, self.un = NaN, NaN
        self.k, self.uk = NaN, NaN
        self.corr, self.chisq = NaN, NaN
    
    def linearise(self):
        """
        Get the linearised vaiables:
            xs = log( t )
            ys = log( - log( 1 - f_c ))
            sigmas = const / ( log(f_a) * f_a )
        """
        # Extract points where log() is well defined
        M = self.M
        cond = np.logical_and( np.logical_and( M.afract > 0,
                                               M.afract < 1),
                               M.times > 0)
        # Linearise
        xs = np.log( np.compress(cond, M.times))
        fs = np.compress(cond, M.afract)
        ys = np.log( - np.log( fs ))
        # Assume equal uncertainty (=1) on area values, transform to y
        sigmas = np.abs(np.ones(xs.shape)/(np.log(fs)*fs))
        return xs, ys, sigmas
    
    def linfit(self):
        """
        Perform a fit to the linearised problem and save the result.
        """
        xs, ys, sigmas = self.linearise()
        n, un, c, uc, corr, chisq = linreg(xs, ys, sigmas)
        chisqndf = chisq / (len(xs)-2)
        # correct uncertainties, calculate k
        un, uc = un*np.sqrt(chisqndf), uc*np.sqrt(chisqndf)
        k, uk = np.exp(c), np.exp(c)*uc
        self.n, self.un = n, un
        self.k, self.uk = k, uk
        self.corr, self.chisq = corr, 1
        return self
    
    def fit(self, dimensions=None):
        """
        Perform a fit on the original data, considering also values where the
        linear problem is not defined
        """
        # Initial values from linear problem
        if self.k is NaN:
            self.linfit()
        n0, k0 = self.n, self.k
        # Get data
        times, fa = self.M.times, self.M.afract
        # Define function to fit onto
        fitfunc = lambda t,n,k: np.exp(-k*t**n)
        if dimensions is None:
            popt, pcov = scipy.optimize.curve_fit(fitfunc, times, fa,
                                                  p0=[n0,k0])
            corr = pcov[0,1]/np.sqrt(pcov[0,0]*pcov[1,1])
        else:
            # If a dimension is given, calculate exponent and put it into
            # the fitfunction
            n = dimensions + 1
            fitfunc2 = lambda t,k: fitfunc(t,n,k)
            popt, pcov = scipy.optimize.curve_fit(fitfunc2, times, fa, p0=[k0])
            # Fill popt and pcov to the same size as from above
            popt = np.insert(popt, 0, n)
            pcov = np.array([[0, 0], [0, pcov[0,0]]])
            corr = 0
        # Get chisquared and correct uncertainties, save
        chisq = chisq_ndf(times, fa, np.ones(fa.shape), fitfunc, popt)
        pcov = pcov*chisq
        self.n, self.k = popt
        self.un, self.uk = np.sqrt(pcov[0,0]), np.sqrt(pcov[1,1])
        self.corr, self.chisq = corr, 1
        return self
    
    def info(self):
        """ Print some information about the fit. """
        print("f_a = exp(-k*t^n)")
        print("<=> ln(-ln(f_a)) = n * ln(t) + ln(k)")
        print("Result:")
        print("  n:", self.n, "+-", self.un)
        print("  k:", self.k, "+-", self.uk)
        print("  correlation:", self.corr)
        return self
    
    def resfun(self, t):
        """
        Return the crystalline fraction to a time t as calculated from the
        model and the results from the fit.
        """
        return 1-np.exp(-self.k*t**self.n)
    
    def linplot(self, axes=None):
        """ Plot the linearised problem and the fit. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        xs, ys, _ = self.linearise()
        n, k = self.n, self.k
        c = np.log(k)
        axes.plot(xs, ys, linestyle="none", marker="s", label="data")
        axes.plot(xs, n*xs+c, label="fit")
        axes.set_xlabel('$\ln(t \\ [s])$')
        axes.set_ylabel('$\ln( - \ln( 1 - f_c ))$')
        return axes
    
    def plot(self, axes=None):
        """ Plot the fit. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        plottimes = np.linspace(np.min(self.M.times), np.max(self.M.times), 100)
        axes.plot(plottimes, self.resfun(plottimes), label="JMAK fit")
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$f_c$")
        return axes


class JmakBuild:
    """
    This class gives a counterpart to JmakFit coming from a defined velocity
    and nucleation rate.
    Properties:
        M  - Measurement
        n  - JMAK exponent [1]
        k  - JMAK coefficient [s^(-n)]
        un - Uncertainty on n [1]
        uk - Uncertainty on k [s^(-n)]
    """
    
    def __init__(self, vFit, jFit, dimensions):
        """
        Initialise. Needs a velocity and a nucleation rate fit object
        and a number of dimensions to use.
        """
        # Save the fits
        self.M = jFit.M
        self.vFit = vFit
        self.jFit = jFit
        # Extract velocity with uncertainty
        v, uv = vFit.v, vFit.uv
        if v is NaN:
            raise AttributeError("Velocity not yet fitted.")
        # Try to get a single value for J
        try:
            [(dNdt, udNdt)] = jFit.Js
        except ValueError as err:
            print("Probably more than one one nucleation rate was fitted.")
            raise err
        # Try to get an area and a height
        try:
            A, h = self.M.A, self.M.h
        except AttributeError as err:
            print("Data regarding the volume missing in the used measurement.")
            raise err
        # Calculate J
        J  =  dNdt / (A*h)
        uJ = udNdt / (A*h)
        # Calculate JMAK formula according to choosen dimension
        if dimensions == 1:
            self.k = J*A*v
            self.uk = np.sqrt( (uJ*A*v)**2 + (J*A*uv)**2 )
        elif dimensions == 2:
            self.k = np.pi/3 * J * h * v**2
            self.uk = np.pi/3 * np.sqrt( (uJ*h*v**2)**2 + (2*J*h*v*uv)**2 )
        elif dimensions == 3:
            self.k = np.pi/3 * J * v**3
            self.uk = np.pi/3 * np.sqrt( (uJ*v**3)**2 + (3*J*v**2*uv)**2 )
        else:
            raise ValueError("Wrong number of dimensions")
        # Exponent with uncertainty
        self.n, self.un = dimensions + 1, 0
    
    def resfun(self, t):
        """
        Time dependtent equation for the crystallised fraction from the fit
        results.
        """
        return 1-np.exp(-self.k*t**self.n)
    
    def plot(self, axes=None):
        """ Plot the fit. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        plottimes = np.linspace(np.min(self.M.times), np.max(self.M.times), 100)
        axes.plot(plottimes, self.resfun(plottimes),
                  label="JMAK $\\left(v,\\ \\frac{dN}{dt} \\right)$")
        axes.set_xlabel("$t \\ [s]$")
        axes.set_ylabel("$f_c$")
        return axes


# Calculate a length from the values in pixels, according to the mail I got.
# First method is the correction I got in the second mail
correctedpx = lambda px: 300/72*px
px2len = lambda p: (0.3876 * correctedpx(p) - 0.3802) * 1e-6

class Experiment(Measurement):
    """
    Class for experimental data as I got from Julian.
    Additional properties:
      columntitles - column titles from the csv
      aarea - amorphous area [m^2]
      A - overall observed area [m^2]
      h - thickness of the material [m]
    """
    
    def __init__(self, filename, height):
        """ Read all data from the file and build variables of interest. """
         
        # Open file and build csv reader
        file = open(filename, newline="")
        reader = csv.reader(file, delimiter=",")
        # First line has titles to the columns
        self.columntitles = list(map(lambda x: x.strip(), next(reader)))
        # Rest is data structured in columns
        columns = [ [] for _ in self.columntitles]
        for row in reader:
            for column, value in zip(columns, row):
                # Replace empty cells with nan to parse them to float
                if value == "":
                    value = "nan"
                column.append(value.replace(",","."))
        # First column are times in minutes, convert to seconds
        self.times = np.array(columns[0], dtype=np.float) * 60
        # Second column is amporphous area. The square root is a length and can
        # be converted according to the mail, then square the result again
        self.aarea = (px2len(np.sqrt(np.array(columns[1], dtype=np.float))))**2
        # Third column has number of grains, here no value means probably zero
        self.nuclei = np.array(columns[2], dtype=np.float)
        self.nuclei[np.isnan(self.nuclei)] = 0
        # Rest of the file are radii of grains
        self.radii = px2len( np.array(columns[3:], dtype=np.float) )
        # Overall observed area
        self.A = max(self.aarea)
        self.h = height
        # Fractions of amorphous and crystaline area
        self.afract = self.aarea/self.A
        self.cfract = 1-self.afract


class Simulation(Measurement):
    """
    Class for simulation results as received from my code.
    Additional properties:
        columntitles - column titles from the csv
    """
    
    def __init__(self, filename):
        """ Read all data from the file and build variables of interest. """
        
        # Open file and build csv reader
        file = open(filename, newline="")
        reader = csv.reader(file, delimiter=",")
        # First line has titles to the columns
        self.columntitles = list(map(lambda x: x.strip(), next(reader)))
        # Rest is data structured in columns
        lines = []
        for row in reader:
            lines.append(row)
        data = np.array(lines, dtype=np.float)
        # First column are times
        self.times = data[:,0]
        # Second column has number of grains
        self.nuclei = data[:,1]
        # Third column is crystalline fraction
        self.cfract = data[:,2]
        self.afract = 1 - self.cfract
        # Rest of the file are sizes of grains
        self.radii = np.sqrt(data[:,3:]/np.pi).transpose()


__all__ = ["Measurement", "Experiment", "Simulation", "VelocityFit",
           "NucleationRateFit", "JmakFit", "JmakBuild"]
