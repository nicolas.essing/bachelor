"""
Analysis of experiments on phase change materials.
"""

# Change matplotlib settings
import matplotlib.pyplot as plt
from cycler import cycler
colors = ['blue','red','green','orange','purple']
plt.rcParams.update({'font.size': 16,
                     'lines.linewidth': 2,
                     'lines.markersize': 5,
                     'lines.markeredgewidth': 2,
                     'axes.prop_cycle': cycler('color', colors)})

# Import classes for single measurements and fits
from .measurements import *
from .measurements import __all__

# Import classes for combining several measurements
from .series import *
from .series import __all__ as __all2__

# Correct list of available classes
__all__.extend(__all2__)
del __all2__

# Import classes for comparisons of simimlar experiments, simulations and
# theoretical predicted behaviour
from .comparisons import *
from .comparisons import __all__ as __all3__

__all__.extend(__all3__)
del __all3__
