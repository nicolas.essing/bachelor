"""
Classes to compare measurements, for example an experiment to a simulation or
a simulation to theoretical predicted behaviour from the values put in.
"""

import numpy as np
import matplotlib.pyplot as plt

class ExperimentComparison:
    """
    Class to compare an experiment with a simulation trying to recreate the
    experiment.
    """
    
    def __init__(self, Exp, Sim):
        """ Build a comparison for an experiment Exp and a simulation Sim. """
        self.E = Exp
        self.S = Sim
    
    def plot_fc(self, axes=None):
        """ Comparison of the crystallised fraction. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.plot(self.E.times, self.E.cfract, linestyle="none", marker="s",
                  label="Experiment")
        axes.plot(self.S.times, self.S.cfract, label="Simulation")
        axes.set_xlabel("$t\\ [s]$")
        axes.set_ylabel("$f_c$")
        return axes
    
    def plot_N(self, axes=None):
        """ Comparison of the grain count. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.plot(self.E.times, self.E.nuclei, linestyle="none", marker="s",
                  label="Experiment")
        axes.plot(self.S.times, self.S.nuclei, label="Simulation")
        axes.set_xlabel("$t\\ [s]$")
        axes.set_ylabel("$N$")
        return axes
    
    def plot_J(self, axes=None):
        """ Comparison of the nucleation rate. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        Ef = self.E.nucleationRateFit()
        Sf = self.S.nucleationRateFit()
        Sf.plotJ(axes)
        Ef.plotJ(axes)
        return axes
    
    def plot_Nc(self, axes=None):
        """ Comparison of the corrected grain count. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        Ef = self.E.nucleationRateFit()
        Sf = self.S.nucleationRateFit()
        Ef.plotN(axes)
        Sf.plotN(axes)
        return axes
    
    def compare_J(self, splitat=None, axes=None):
        """ Comparison of the nucleation rate. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        Ef = self.E.nucleationRateFit(splitat).fit()
        Sf = self.S.nucleationRateFit(splitat).fit()
        print("Experiment:")
        Ef.info()
        print()
        print("Simulation:")
        Sf.info()
        Sf.plotN(axes)
        Ef.plotN(axes)
        return axes
    
    def overviewplot(self):
        fig, axes = plt.subplots(2,2)
        fig.set_size_inches([12,8])
        self.plot_fc(axes[0,0])
        axes[0,0].legend()
        self.plot_N(axes[0,1])
        axes[0,1].legend()
        self.plot_J(axes[1,0])
        axes[1,0].legend()
        self.plot_Nc(axes[1,1])
        axes[1,1].legend()
        fig.tight_layout()
        return axes


class TheoryComparison:
    def __init__(self, M, J, v):
        """ A measurement M, used nucleation rate J and velocity v """
        self.M = M
        self.J = J
        self.v = v
        self.jFit = M.nucleationRateFit(splitat=[])
    
    def fit(self):
        self.jFit.fit()
        return self
    
    def plot_Nc(self, axes=None):
        """
        Compare fitted nucleation rate with set one in corrected count
        """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        self.jFit.plotN(axes)
        axes.plot(self.M.times, self.M.times*self.J, label="set")
        return axes
    
    def plot_J(self, axes=None):
        if axes is None:
            fig, axes = plt.subplots(1,1)
        self.jFit.plotJ(axes)
        axes.plot(self.M.times, self.J*np.ones(self.M.times.shape),
                  label="set")
        return axes
    
    def info(self):
        self.fit()
        [(J,uJ)] = self.jFit.Js
        print("Nucleation rate:")
        print(" relative deviation from used value:", (J-self.J)/uJ*100, "%")
        return self

__all__ = ["ExperimentComparison", "TheoryComparison"]