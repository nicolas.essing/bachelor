"""
This file provides classes to fit temperature dependent models to data obtained
from several measurements at different temperatures.
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
from .scripts import linreg, chisq_ndf, enlargedLinspace

# Some physial constants
kB = 1.380649e-23
eV = 1.6021766208e-19

class SeriesOfMeasurements:
    """
    A class for a series of several measurements at different temperatures.
    """
    
    def __init__(self, temperatures, measurements, dimensions):
        """
        Initialise a series of measurements (given as a list) at different
        temperatures (given as a list).
        Analyse assuming a given number of dimensions.
        """
        self.Ts = np.array(temperatures)
        self.Ms = measurements
        self.dimensions = dimensions
    
    def load(self):
        """
        Do all the fits for all the measurements.
        """
        self.vs = np.zeros(len(self.Ms))
        self.uvs  = np.zeros(len(self.Ms))
        self.Js  = np.zeros(len(self.Ms))
        self.uJs  = np.zeros(len(self.Ms))
        for i,M in enumerate(self.Ms):
            vFit = M.velocityFit().fitall()
            self.vs[i], self.uvs[i] = vFit.v, vFit.uv
            volume = M.A * M.h
            jFit = M.nucleationRateFit().fit()
            [(J,uJ)] = jFit.Js
            self.Js[i], self.uJs[i] = J/volume, uJ/volume
        return self
    
    def arrheniusFit(self):
        """
        Return an arrhenius behaviour fit for the growth velocities of this
        series of measurements.
        """
        return ArrheniusFit(self)
    
    def simpleCntFit(self, L, Tm, Ea, IE):
        """
        Return an object to fit classical nucleation theory to the rates of
        this series, using given values for the latent heat L, the melting
        temperature Tm and an initial guess for the interfacial energy IE.
        """
        return SimpleCntFit(self, L, Tm, Ea, IE)
    
    def mobilityFit(self, dGv):
        """
        Return an arrhenius behaviour fit for the mobility v/dGv of this
        series of measurements, using a given temperature dependent function
        for the free energy.
        """
        return MobilityFit(self, dGv)


class ArrheniusFit:
    """ Class to fit an Arrhenius behaviour to the growth velocity. """
    
    def __init__(self, series):
        """ Build an object of a given series of measurements. """
        self.Ts = series.Ts
        self.vs = series.vs
        self.uvs = series.uvs
    
    def linfit(self):
        """ Fit in the linearised problem. """
        m, um, c, uc, _, _ = linreg(1/self.Ts, np.log(self.vs), self.uvs)
        self.vinf, self.uvinf = np.exp(c), np.exp(c)*uc
        self.Ea, self.uEa = -m*kB, um*kB
        return self
    
    def fit(self):
        """ Perform a fit. """
        self.linfit()
        fitfunc = lambda x,a,b: a*np.exp(-b/x)
        Ts, vs, uvs = self.Ts, self.vs, self.uvs
        p0 = [self.vinf, self.Ea/kB]
        popt, pcov = scipy.optimize.curve_fit(fitfunc, Ts, vs, p0=p0,
                                              sigma=uvs, absolute_sigma=True)
        self.vinf, self.Ea = popt[0], popt[1]*kB
        self.uvinf, self.uEa = np.sqrt(pcov[0,0]), np.sqrt(pcov[1,1])*kB
        return self
    
    def v(self,T):
        """
        Velocity as a function of temperature according to the fit result.
        """
        return self.vinf * np.exp(-self.Ea/kB/T)
    
    def info(self):
        """ Print some information. """
        print("Fitting v = vinf*exp(-Ea/kB/T)")
        print("vinf = {} +- {}".format(self.vinf, self.uvinf))
        print("Ea   = {} +- {}".format(self.Ea, self.uEa))
        Ea, uEa = [x/eV for x in [self.Ea, self.uEa]]
        print("   = (",Ea,"+-",uEa,")eV")
        print("chisq/ndf:", chisq_ndf(self.Ts, self.vs, self.uvs,
                                      (lambda x,a,b: a*np.exp(-b/x)),
                                      [self.vinf, self.Ea/kB]))
        return self
    
    def plot_lin(self, axes=None):
        """ Plot the linearised problem. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        invTs = 1/self.Ts
        m, c = -self.Ea/kB, np.log(self.vinf)
        axes.errorbar(invTs, np.log(self.vs), self.uvs/self.vs,
                      linestyle="none", marker="_", label="data")
        axes.plot(invTs, m*invTs+c, label="fit")
        axes.set_xlabel("$1/(T \\ [K])$")
        axes.set_ylabel("$\\log(v \\ [m/s])$")
        return axes
    
    def plot(self, axes=None):
        """ Show the data and fit. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.errorbar(self.Ts, self.vs, self.uvs, linestyle="none", marker="_",
                     label="data")
        pxs = np.linspace(np.min(self.Ts)-1,np.max(self.Ts)+1,100)
        axes.plot(pxs,self.v(pxs), label="fit")
        axes.set_xlabel("$T \\ [K]$")
        axes.set_ylabel("$v \\ [m/s]$")
        return axes


# Function for bulk free energy and critical energy
def bulkFreeEnergy(T,L,Tm):
    return L*(Tm-T)/Tm*2*T/(Tm+T)

def criticalFreeEnergy(T,IE,L,Tm):
    return 16*np.pi*IE**3/(3*bulkFreeEnergy(T,L,Tm)**2)

def nucleationrate(T,a,IE,Ea,L,Tm):
    return ( 1/(3*np.pi**2*a) * np.sqrt(kB*T*IE) *
             np.exp(-(Ea + criticalFreeEnergy(T,IE,L,Tm))/kB/T) )

class SimpleCntFit:
    """
    A simple fit of classical nucleation theory onto the nucleation rates,
    using given values for latent heat L and melting temperature Tm and leaving
    the prefactor b and interfacial energy IE as fit parameters.
    """
    
    def __init__(self, series, L, Tm, Ea, IE):
        """
        Build an object of a given series of measurements, using given values
        for the latent heat L, melting temperature Tm, activation energy Ea
        and an initial guess for the interfacial energy IE.
        """
        self.Ts = series.Ts
        self.Js = series.Js
        self.uJs = series.uJs
        self.L, self.Tm, self.Ea, self.IE = L, Tm, Ea, IE
    
    def fit(self):
        """ Perform the fit. """
        Ts, Js, uJs = self.Ts, self.Js, self.uJs
        # Fitfunction with L and Tm from literature
        fitfunc = lambda T,a,IE: nucleationrate(T,a,IE,self.Ea,self.L,self.Tm)
        # Guess for IE is a given value, choose initial prefactor to fit some
        # arbitary point. Do the fit.
        a0 = fitfunc(Ts[len(Js)//2],1,self.IE) / Js[len(Js)//2]
        popt, pcov = scipy.optimize.curve_fit(fitfunc, Ts, Js, maxfev=60000,
                                              p0=[a0,self.IE], sigma=uJs)
        self.a, self.IE = popt
        self.ua, self.uIE = np.sqrt(pcov[0,0]), np.sqrt(pcov[1,1])
#        self.a, self.IE = a0, self.IE
#        self.ua, self.uIE = 1, 1
        return self
    
    def J(self,T):
        """
        Nucleation rate as a temperature dependent function from this fit.
        """
        return nucleationrate(T,self.a,self.IE,self.Ea,self.L,self.Tm)
    
    def info(self):
        """ Print some information. """
        print("Fitting J = 1/(3*pi^2*a) * sqrt(kB*T*IE) * exp(-(Ea + dGc)/kB/T)")
        print("        dGc = 16*pi*IE^3/(3*dGv)")
        print("        dGv = L*(Tm-T)/Tm*2*T/(Tm+T)")
        print("a: ",self.a,"+-",self.ua)
        print("IE:",self.IE,"+-",self.uIE)
        return self
    
    def plot(self, axes=None):
        """ Plot the problem and fit. """
        Ts, Js = self.Ts, self.Js
        if axes is None:
            fig, axes = plt.subplots(1,1)
            plotrange = np.min(Ts), np.max(Ts)
        else:
            plotrange = axes.get_xlim()
        axes.plot(Ts, Js, linestyle="none", marker=".", label="mean rates")
        pxs = enlargedLinspace(plotrange[0], plotrange[1], 200, 1.05)
        axes.plot(pxs, self.J(pxs), label="fit")
        axes.set_xlabel("$T \\ [K]$")
        axes.set_ylabel("$J \\ [1/m^3/s]$")
        return axes

class MobilityFit:
    """ Fitting an Arrhenius behaviour to the mobility M = v / dGv """
    
    def __init__(self, series, dGv):
        """
        Build an object of a given series of measurements and a given model for
        the free energy dGv as a function fo temperature.
        """
        self.Ts = series.Ts
        self.vs = series.vs
        self.uvs = series.uvs
        self.Ms = self.vs / dGv(self.Ts)
        self.uMs = self.uvs / dGv(self.Ts)
    
    def linfit(self):
        """ Fit the linear problem. """
        m, um, c, uc, _, _ = linreg(1/self.Ts, np.log(self.Ms), self.uMs)
        self.Minf, self.uMinf = np.exp(c), np.exp(c)*uc
        self.Ea, self.uEa = -m, um
        return self
    
    def fit(self):
        """ Perform a fit. """
        self.linfit()
        fitfunc = lambda x,a,b: a*np.exp(-b/x)
        Ts, Ms, uMs = self.Ts, self.Ms, self.uMs
        p0 = [self.Minf, self.Ea]
        popt, pcov = scipy.optimize.curve_fit(fitfunc, Ts, Ms, p0=p0,
                                              sigma=uMs, absolute_sigma=True)
        self.Minf, self.Ea = popt[0], popt[1]
        self.uMinf, self.uEa = np.sqrt(pcov[0,0]), np.sqrt(pcov[1,1])
        return self
    
    def M(self,T):
        """
        Mobility as temperature dependent function according to this fit results.
        """
        return self.Minf * np.exp(-self.Ea/T)
    
    def info(self):
        """ Print some information. """
        print("Fitting M = Minf*exp(-Ea/T)")
        print("Minf = {} +- {}".format(self.Minf, self.uMinf))
        print("Ea   = {} +- {}".format(self.Ea, self.uEa))
        Ea, uEa = [x/eV*kB for x in [self.Ea, self.uEa]]
        print("   = (",Ea,"+-",uEa,")eV/kB")
        print("chisq/ndf:", chisq_ndf(self.Ts, self.Ms, self.uMs,
                                      (lambda x,a,b: a*np.exp(-b/x)),
                                      [self.Minf, self.Ea]))
        return self
    
    def plot_lin(self, axes=None):
        """ Plot the linearised problem. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        invTs = 1/self.Ts
        m, c = -self.Ea, np.log(self.Minf)
        axes.errorbar(invTs, np.log(self.Ms), self.uMs/self.Ms,
                      linestyle="none", marker="_", label="data")
        axes.plot(invTs, m*invTs+c, label="fit")
        axes.set_xlabel("$1/(T \\ [K])$")
        axes.set_ylabel("$\\log(M \\ \\left[\\frac{m^3}{J s}\\right])$")
        return axes
    
    def plot(self, axes=None):
        """ Show a plot. """
        if axes is None:
            fig, axes = plt.subplots(1,1)
        axes.errorbar(self.Ts, self.Ms, self.uMs, linestyle="none", marker="_",
                     label="data")
        pxs = np.linspace(np.min(self.Ts)-1,np.max(self.Ts)+1,100)
        axes.plot(pxs,self.M(pxs), label="fit")
        axes.set_xlabel("$T \\ [K]$")
        axes.set_ylabel("$M \\ \\left[\\frac{m^3}{J s}\\right]$")
        return axes        

__all__ = ["SeriesOfMeasurements", "ArrheniusFit", "SimpleCntFit",
           "MobilityFit"]
