"""
Testing the analysis algorithms by comparison with some simulations.
"""

import numpy as np
import matplotlib.pyplot as plt
import fits

# Read all done simulations, give the nucleation rate that was put in
Comparisons = []
setrates = np.array([8e13,6e14,4e15]) * 4000**2 * 6.28e-8**3
for file, J, v in [ ("../data/testana/testana_1.csv", setrates[0], 6e-8),
                    ("../data/testana/testana_6.csv", setrates[0], 6e-8),
                    ("../data/testana/testana_2.csv", setrates[1], 6e-8),
                    ("../data/testana/testana_7.csv", setrates[1], 6e-8),
                    ("../data/testana/testana_3.csv", setrates[2], 6e-8),
                    ("../data/testana/testana_8.csv", setrates[2], 6e-8) ]:
    S = fits.Simulation(file)
    Comparisons.append(fits.TheoryComparison(S, J, v))


def test_nucleationratefit_bias():
    # Test the method overall: Calculate all differences from the input values
    deltas = np.zeros(len(Comparisons))
    for i, C in enumerate(Comparisons):
        [(J,uJ)] = C.fit().jFit.Js
        deltas[i] = (J - C.J) / (np.abs(J + C.J) / 2)
    print("relative deviation of fit result and value set in the simulation")
    print("mean:", np.mean(deltas)*100, "%")
    print("std:", np.std(deltas)*100, "%")
    print("sqrt(mean(squared difference)):",
          np.sqrt(np.mean(np.array(deltas)**2))*100, "%")

def show_nucleationrate_test():
    print("Set:", Comparisons[2].J)
    axes1 = Comparisons[2].fit().plot_Nc()
    axes1.lines[0].set_color("tab:blue")
    axes1.lines[1].set_label("fit")
    axes1.legend()
    axes1.figure.tight_layout()
    print("first fit:")
    Comparisons[2].jFit.info()
    axes2 = Comparisons[3].fit().plot_Nc()
    axes2.lines[1].set_label("fit")
    axes2.set_xlim(axes1.get_xlim())
    axes2.set_ylim(axes1.get_ylim())
    axes2.legend()
    axes2.figure.tight_layout()
    print("second fit:")
    Comparisons[3].jFit.info()
    return axes1.figure, axes2.figure


# Generate and save images
fig1, fig2 = show_nucleationrate_test()
fig1.savefig("../img/simtest_J_1.png")
fig2.savefig("../img/simtest_J_2.png")
