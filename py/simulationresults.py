"""
Comparing the experimental data to the simulations with parameters from them.
"""

import numpy as np
import matplotlib.pyplot as plt
import fits

# Read the experimental data
height = 30e-9
Ex115 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_115C.csv", height)
Ex120 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_120C.csv", height)
Ex125 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_125C.csv", height)
Ex130 = fits.Experiment("../data/experiment/Sb2Te_asd_20W_130C.csv", height)

# Read the simulations
S115 = fits.Simulation("../data/sb2te_115.csv")
S120 = fits.Simulation("../data/sb2te_120.csv")
S125 = fits.Simulation("../data/sb2te_125.csv")
S130 = fits.Simulation("../data/sb2te_130.csv")

# Read the simulations with constant nucleation rate
S115s = fits.Simulation("../data/sb2te_simple_115.csv")
S120s = fits.Simulation("../data/sb2te_simple_120.csv")
S125s = fits.Simulation("../data/sb2te_simple_125.csv")
S130s = fits.Simulation("../data/sb2te_simple_130.csv")

# Time offsets coming from nucleation rates starting with zero
S115.timeoffset(1.7e3)

# Build the comparisons
C15 = fits.ExperimentComparison(Ex115, S115)
C20 = fits.ExperimentComparison(Ex120, S120)
C25 = fits.ExperimentComparison(Ex125, S125)
C30 = fits.ExperimentComparison(Ex130, S130)
C15s = fits.ExperimentComparison(Ex115, S115s)
C20s = fits.ExperimentComparison(Ex120, S120s)
C25s = fits.ExperimentComparison(Ex125, S125s)
C30s = fits.ExperimentComparison(Ex130, S130s)

# Nucleation rate fits
#C15.compare_J([1.2e3,10e3,19e3,28e3])
#C20.compare_J([1000,8888])
#C25.compare_J([2000,3500])
#C30.compare_J([250,1100,1900])

def _analyse(C):
    axes1 = C.plot_fc()
    axes1.ticklabel_format(style="sci", scilimits=(0,0), useOffset=False,
                          axis="both")
    axes1.legend()
    axes1.figure.tight_layout()
    axes2 = C.plot_N()
    axes2.ticklabel_format(style="sci", scilimits=(0,0), useOffset=False,
                          axis="x")
    axes2.legend()
    axes2.figure.tight_layout()
    return axes1.figure, axes2.figure

def comp_jmak_115():
    jb = Ex115.jmakBuild(2)
    axes = Ex115.plot_fc()
    jb.plot(axes)
    S115.plot_fc(axes)
    axes.lines[0].set_label("Experiment")
    axes.lines[1].set_label("JMAK")
    axes.lines[2].set_marker("")
    axes.lines[2].set_linestyle("solid")
    axes.lines[2].set_label("Simulation")
    axes.ticklabel_format(style="sci", scilimits=(0,0), useOffset=False,
                          axis="both")
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure

def comp_jmak_125():
    jb = Ex125.jmakBuild(2)
    axes = Ex125.plot_fc()
    jb.plot(axes)
    S125.plot_fc(axes)
    axes.lines[0].set_label("Experiment")
    axes.lines[1].set_label("JMAK")
    axes.lines[2].set_marker("")
    axes.lines[2].set_linestyle("solid")
    axes.lines[2].set_label("Simulation")
    axes.ticklabel_format(style="sci", scilimits=(0,0), useOffset=False,
                          axis="both")
    axes.legend()
    axes.figure.tight_layout()
    return axes.figure

# Generate and save images
for C, name in zip([C15, C20, C15, C30, C15s, C20s, C25s, C30s],
             ["115", "120", "125", "130", "115_s", "120_s", "125_s", "130_s"]):
    fig1, fig2 = _analyse(C)
    fig1.savefig("../img/simres_sb2te_{}_fc.png".format(name))
    fig2.savefig("../img/simres_sb2te_{}_N.png".format(name))
comp_jmak_115().savefig("../img/simres_sb2te_jmak_115.png")
comp_jmak_125().savefig("../img/simres_sb2te_jmak_125.png")
