"""
This file provides classes to compare the parameters of GST and Sb2Te
"""

import numpy as np
from numpy import pi, sqrt, exp, log10
import matplotlib.pyplot as plt

# Some physical constants
kB = 1.380649e-23
eV = 1.6021766208e-19
zeroC = 273.15

class PCM:
    """
    This class defines some properties that are calculated independently of the
    other physical models.
    """
    
    @property
    def tau(self):
        """ Relaxation time constant [s] """
        return 1/self.M/self.W
    
    @property
    def r_c(self):
        """ Critical nucleus radius [m] """
        return 2*self.IE/self.dGv
    
    @property
    def eps(self):
        """ gradient energy coefficient """
        return sqrt(6*self.IE*self.DIT)
    
    @property
    def W(self):
        """ double well depth """
        return 3*self.IE/self.DIT
    
    @property
    def dGv(self):
        """ bulk free energy """
        return self.L*(self.Tm - self.T)*2*self.T/self.Tm/(self.Tm+self.T)

class GST( PCM ):
    """
    The properties of this class are the parameters of GST.
    """
    
    IE = 0.06           # interfaial energy, J/m^2
    DIT = 0.5e-9        # diffuse interface thickness, m
    Tm = 889            # melting temperature, K
    L = 625000000       # latent heat, J/(m^3)
    
    Vm = 2.9e-28  # volume of a monomer, m^3
    ninf = 0.012  # constant in viscosity equation, Pa*s
    m = 140       # fragility
    Tg = 472      # temperature at which viscosity is 1012Pa
    jd = 2.99e-10 # jump distance, m
    Ea = 2.3      # activation energy,eV
    Tglass = 534  # glass transition temperature, K
    wa = 180      # weting angle, degree (0<=wa<=180, 180 means homogeneous)
    
    def __init__(self, T):
        self.T = T
        
        # Precalculate some constants that are used more than once
        dGv00 = self.L*(self.Tm - self.Tglass)*2*self.Tglass/self.Tm/(self.Tm+self.Tglass)
        n00 = (10**(log10(self.ninf)+(12-log10(self.ninf))*(self.Tg/self.Tglass)*
               exp((self.m/(12-log10(self.ninf))-1)*(self.Tg/self.Tglass-1))))
        self.n0 = n00/exp(self.Ea*eV/kB/self.Tglass);
        self.vinf = (4*(kB*self.Tglass)*(1-exp(-dGv00*self.Vm/kB/self.Tglass))/
            (3*pi*self.jd*self.jd*n00*dGv00)/exp(-self.Ea*eV/kB/self.Tglass))
    
    @property
    def M(self):
        """ mobility """
        return ((self.T <= self.Tglass) * 
                    (sqrt(2*self.W)/6/self.eps*self.vinf*
                    exp(-self.Ea*1.6e-19/(kB*self.T)))
                +(self.T>self.Tglass) * 
                    (sqrt(2*self.W)/6/self.eps*4*(kB*self.T)*
                    (1-exp(-self.dGv*self.Vm/(kB*self.T)))/
                    (3*pi*self.jd*self.jd*self.n*self.dGv))
                )
    
    @property
    def n(self):
        """ viscosity """
        return np.fmax((self.T<=self.Tglass) *
                            self.n0*exp(self.Ea*eV/(kB*self.T)), 
                        (self.T>self.Tglass) *
                           (10**(log10(self.ninf)+(12-log10(self.ninf))
                           *(self.Tg/self.T)*exp((self.m/(12-log10(self.ninf))-1)*
                           (self.Tg/self.T -1))))
                )
    
    @property
    def J(self):
        """ nucleation rate [1/m^3/s] """
        rcrit = 2 * self.IE / self.dGv
        nc = 4/3*pi * (rcrit)**3 /self.Vm
    
        jf = (kB*self.T)/(3*pi*self.jd**3*self.n)
        I0 = (4*jf*(nc**(2/3))*sqrt(self.dGv*self.Vm/
              (6*pi*nc*(kB*self.T)))/self.Vm)
        dGc = 16/3*pi*(self.IE**3)/(self.dGv*self.dGv)
        return I0*exp(-dGc/(kB*self.T));
    
    @property
    def v(self):
        """ groth velocity [m/s] """
        return 6*self.M*self.DIT*self.dGv


class SbTe( PCM ):
    IE = 5.5e-2
    DIT = .5e-9
    L = 878.9e6
    Tm = 817
    
    def __init__(self, T):
        self.T = T
    
    @property
    def M(self):
        """ mobility """
        return self.v/(6*self.DIT*self.dGv)
    
    @property
    def v(self):
        """ groth velocity [m/s] """
        a = 4.55725664239e+14
        b = -21122.9264158
        return a*exp(b/self.T)
    
    @property
    def J(self):
        """ nucleation rate [1/m^3/s] """
        a = 4.82729439195e+67
        b = -52052.3553573 
        return a*exp(b/self.T)

#Ts = np.arange(300, 600, 5)
Ts = np.arange(200, 850, 1)
gst = GST(Ts)
sbte = SbTe(Ts)

#plt.semilogy(Ts, gst.v, label="GST")
#plt.semilogy(Ts, sbte.v, label="SbTe")
#plt.legend()