"""
Overall performance test results.
"""

import numpy as np
import matplotlib.pyplot as plt

# Set font size and stuff for images in the tex document
plt.rcParams.update({'font.size': 14,
                     'lines.linewidth': 2,
                     'lines.markersize': 6})

# Number of simulation steps, time needed py each program. Different lines are
# different runs.
numberofsteps = [0, 150, 300, 450]
zhiyangstimes = [[0.865849, 12.591858, 23.241035, 34.562356],
                 [0.747356, 13.213664, 21.791110, 29.720424],
                 [0.594743, 13.393875, 24.355205, 34.251106]]
mycodetimes =  [[0.297410, 4.313865, 7.210557, 11.215001],
                [0.292255, 5.786568, 10.418592, 15.050989],
                [0.198605, 4.611776, 8.825867, 12.488939]]

# Mean over different runs
zhiyangstimes = np.mean(zhiyangstimes, axis=0)
mycodetimes = np.mean(mycodetimes, axis=0)

fig, axes = plt.subplots(1,1)
axes.plot(numberofsteps, zhiyangstimes, marker="s", label="original code")
axes.plot(numberofsteps, mycodetimes, marker="s", label="new code")
axes.set_xlabel("number of steps")
axes.set_ylabel("time [s]")
axes.legend()
fig.tight_layout()
fig.savefig("../img/perf_overall.png")

# Mean differences and analyse ratio
mine = np.mean(np.diff(mycodetimes))
zhiy = np.mean(np.diff(zhiyangstimes))
print("time per 150 steps:")
print("original:", zhiy)
print("new:", mine)
print("{:.0f}% faster, {:.0f}% less time".format((zhiy/mine-1)*100, mine/zhiy*100))