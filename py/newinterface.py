"""
A look at the new nucleus form I use.
"""

import numpy as np
from numpy import tanh, cosh, exp, log
import matplotlib.pyplot as plt

# plot range
xs = np.linspace(-2,10,400)

# The new form I use
def f(x,a,b):
    lim = b*log(1+exp(2*a/b))
    y0 = b * log(cosh(a/b)) / lim
    return ( b*log(cosh((a-x)/b))+x*tanh((a-x)/b) ) / lim + 1-y0

# The analytical solution for a planar interface
def g(x,a,b):
    return 1/2*(1+tanh((a-x)/b))

# Compare the new shape for different parameters
#plt.plot(xs,f(xs,1,1), label="1, 1")
#plt.plot(xs,f(xs,2,1), label="2, 1")
#plt.plot(xs,f(xs,1,2), label="1, 2")

# Take a value for the radius and one for the thickness and compare the shapes
r_c, DIT = .3, 1
plt.plot(xs,f(np.abs(xs),r_c,DIT), label="new")
plt.plot(xs,g(np.abs(xs),r_c,DIT), label="old")

plt.hlines([0,1],-2,10,alpha=.5)
plt.vlines([0],0,1,alpha=.5)
plt.legend()