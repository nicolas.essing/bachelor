\chapter{Implemented Simulations}
The simulation was extended by several physical models and implemented for
different materials.
For \sbte, there is experimental data available the results are compared to.
For other materials, this chapter only lists the used models and parameters
and shows that the simulation model is able to work with parameters
in the order of magnitude necessary for these materials.
%TODO explain unphysical values for r_c
%TODO Differences to JMAK model are pointed out (either for each material or
% in an own section for dimensionless parameters

% This cryptic stuff in the section title is necessary as the
% hyperref package does not want math mode in links
\section{\texorpdfstring{\sbte}{Sb2Te}}
  \label{sec:sbteresults}
  Two simulations for \sbte{} were implemeted using both physical and
  numerical values for $r_c$.
  For the velocity, an Arrhenius model is used together with the parameters
  obtained from the fit presented in section~\ref{sec:fit_v}.
  The nucleation rate is implemented as the fitted classical nucleation theory
  model, which assumed an Arrhenius behaviour for the viscosity.
  Thus, the model is only applicable for lower temperatures.
  
  For the nucleation rate and if a physical value for $r_c$ is used,
  $\dGv$ need to be calculated.
  Together with the fit results, the following parameters were used:
  \begin{center}
    \begin{tabular}{c|c|c}
    Parameter & Value & Reference \\
    \hline
    $L$ & $\SI{47.2}{\J\per\g} \times \SI{6.28}{\g\per\cm\tothe{3}} = \SI{296.4e6}{\J\per\m\tothe{3}}$ & \citep{expdata}, \citep{sb2teStructure} \\
    $T_m$ & $\SI{838}{\K}$ & \citep{expdata} \\
    $v_\infty$ & $\SI{1.9 e15}{\m\per\s}$ & fit on $v$ (\citep{expdata}) \\
    $E_a$ & $\SI{1.82}{\eV}$ & fit on $v$ (\citep{expdata})\\
    $\lambda^3 \eta_\infty \left(\frac{3 V_m}{4 \pi}\right)^{(2/3)}$ & $\SI{1.2 e99}{\J\m\squared\s}$ & fit on $N$(\citep{expdata}) \\
    $\gamma$ & $\SI{7.29 e-2}{\J\per\m\squared}$ & fit on $N$ (\citep{expdata}) \\
    \end{tabular}
  \end{center}
  
  This implementation was used to simulate the experimental setup.
  Based on the available computational capacity, a two dimensional simulation
  with a grid point spacing of $h = \SI{200}{\nm}$ was choosen.
  As for $T \approx \SI{400}{\K}$, the critical radius varies from
  $2$ to $\SI{3}{\nm}$, a purely numerical value of $r_c = \SI{2}{\um}$
  was used.
  The result of these simulation is compared to the experimental data in
  figure~\ref{fig:sbte_res_const_J}.
  While for some experiments this gave acceptable results,
  huge deviations occured whenever unusual behaviour of the nucleation rate
  was observed.
  
  \begin{figure}[tbp]
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_115_s_fc.png}
    \caption{$\SI{115}{\celsius}$}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_125_s_fc.png}
    \caption{$\SI{125}{\celsius}$}
    \end{subfigure}
    \caption{Comparison of the crystallized fraction over time between
	simulation results and experimental data. For these simulations,
	a nucleation rate calculated from the fit to classical nucleation
	theory was used. For $\SI{115}{\celsius}$, where the nucleation
	rate in the experiment was approximately constant, the curves are
	similar. For $\SI{125}{\celsius}$ however, where the nucleation rate
	varies strongly, a considerable deviation is observed.}
    \label{fig:sbte_res_const_J}
  \end{figure}
  
  Motivated by this, the values obtained from fitting different nucleation
  rates for several time intervals were used.
  The parameter $J$ in the simulation was manually altered after according
  time intervals.
  The results of this method are compared to the experiment in
  figures~\ref{fig:sbte_res_115} and~\ref{fig:sbte_res_125} and show better
  consistence.
  Furthermore, the simulation describes the data better than the JMAK model
  does, which can be seen in figure~\ref{fig:cmp_sim_jmak}.
  While only small deviations can be seen for some temperatures,
  the advantage becomes obvious whenever the nucleation rate varies strongly
  over time.
  This is first of all a confirmation of the phase field model.
  As it is however not possible to extrapolate the behaviour for different
  temperatures and samples from these results,
  it is mostly shown that more data is necessary.
  
  Comparisons of experimental and simulated data to the temperatures
  not shown here can be found in the appendix.
  
  % 115C went quite well, JMAK nearly as good
  % 120C has systematically less grains and thus nucleates systematically
  %      slower, not good, but no spectacular differences. JMAK looks alike,
  %      but shifted left (fits a little better)
  % 125C is pretty consistend in N, but shows a systematic difference in f_c.
  %      Good example for problems. JMAK has even more problems
  % 130C has not many data points and some weird behaviour, should not be
  %      shown. Much better than JMAK
  
  \begin{figure}[tbp]
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_115_fc.png}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_115_N.png}
    \end{subfigure}
    \caption{Comparison of experimental data and simulation results
	($\SI{115}{\celsius}$).
	Different nucleation rates were used, which can be seen as kinks in the
	number of grains. Apart from some time in the middle, where there are
	considerably more nuclei in the simulation than in the experiment,
	both are in good agreement. As the nucleation is a statistical process,
	this is not necessary significant and might look different with
	different random numbers.}
    \label{fig:sbte_res_115}
  \end{figure}
  
  \begin{figure}[tbp]
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_125_fc.png}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_125_N.png}
    \end{subfigure}
    \caption{Comparison of experimental data and simulation results
	($\SI{125}{\celsius}$).
	The number of nuclei in the experiment shows unusual behaviour, which
	is approximated by three different nucleation rates.
	There are small but systematic differences visible in the crystallized
	fraction.}
    \label{fig:sbte_res_125}
  \end{figure}
  
  \begin{figure}[tp]
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_jmak_115.png}
    \caption{$\SI{115}{\celsius}$}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\columnwidth}
    \includegraphics[width=\columnwidth]{../img/simres_sb2te_jmak_125.png}
    \caption{$\SI{125}{\celsius}$}
    \end{subfigure}
    \caption{Comparison of JMAK model and simulation to experimental data.
	For $\SI{115}{\celsius}$, there are barly differences visible.
	For $\SI{125}{\celsius}$ however, where some unusual behaviour in the grain count
	appears, the simulation describes the data significantly better.}
    \label{fig:cmp_sim_jmak}
  \end{figure}

\clearpage
\section{AIST}
  For AIST, two simulation models models were implemented during this thesis.
  The first uses only physical reasonable parameters.
  For temperatures around $\SI{400}{\K}$, the critical radius is smaller than
  a nanometer, which strongly restricts the size of systems that can be
  simulated.
  The second implementation uses both $\zeta$ and $r_c$ as numerical
  parameters with physical values for $v$ and $J$.
  
  The velocity was calculated from the Frenkel model over the whole
  temperature range.
  For the viscosity, a combination of an Arrhenius model for temperatures
  below $T_glass$ and the MYEGA model in the first form for higher
  temperatures was used.
  This glass transition temperature was estimated from the intersect of the
  Arrhenius model for the velocity reported in ref.~\citep{SbTeArrhenius}
  with the one reported in ref.~\citep{AISTpaper} (calculated from Frenkel
  model with a MYEGA viscosity) to be $T_{glass} = \SI{480}{\K}$.
  The volume per atom was estimated from the latent heat per volume and the
  one per atom, $L \cdot V_m = \SI{173}{\meV}$~\citep{AistExperiments}.
  The nucleation rate was obtained from classical nucleation theory.
  All parameters are listed in the following table:
  \begin{center}
    \begin{tabular}{c|c|c}
    Parameter & Value & Reference \\
    \hline
    $L$ & $\SI{878.9}{\J\per\cm\tothe{3}}$ & \citep{AISTpaper} \\
    $T_m$ & $\SI{808}{\K}$ & \citep{AistExperiments} \\
    
    $E_a$ & $\SI{2.90}{\eV}$ & \citep{SbTeArrhenius} \\
    $v_\infty$ & $\SI{3.03e24}{\m\per\s}$ & \citep{SbTeArrhenius} \\
    
    $\eta_\infty$ & $\SI{1.22 e-3}{\Pa\s}$ & \citep{AISTpaper} \\
    $T_g$ & $\SI{445}{\K}$ & \citep{AISTpaper} \\
    $m$ & $128$ & \citep{AISTpaper} \\
    
    $V_m$ & $\SI{3.2 e-26}{\m\tothe{3}}$ & \citep{AistExperiments, AISTpaper} \\
    $\lambda$ & $\SI{1}{\angstrom}$ & \citep{AistExperiments} \\
    $r_{atom}$ & $\SI{1.5}{\angstrom}$ & \citep{AistExperiments} \\
    $R_{hyd}$ & $ \SI{0.5}{\angstrom}$ & \citep{AistExperiments} \\
    
    $\gamma$ & $\SI{55}{\milli\J\per\m\squared}$ & \citep{AISTpaper}
    \end{tabular}
  \end{center}

\clearpage
\section{GeTe}
  For GeTe, the only values for the interfacial energy that could be found
  are values from $0.2$ to $\SI{0.4}{\J\per\m\squared}$ obtained from
  ab initio simulations~\citep{geteAbInitioSimulations}.
  This is an order of magnitude larger than the values for similar materials
  and leads to vanishing nucleation in the classical theory.
  Nucleation was thus implemented using an Arrhenius function with values
  obtained from ref.~\citep{geteNucleationAndGrowth}.
  These were obtained from experiments around $\SI{400}{\K}$ and do
  most probably not describe the behaviour of this material for larger
  temperatures.
  
  For the velocity, the Frenkel model with neglected temperature dependence of
  the prefactor was used,
  together with a two-part viscosity.
  For high temperatures, the MYEGA model in the second form is used,
  while for lower an Arrhenius behaviour is assumed.
  From the intersect of these models, the transition temperature $T_{glass}$
  was estimated to be $\SI{454}{\K}$.
  
  As the value for the interfacial energy was not accepted, a physical value
  for $r_c$ is not known.
  The implementation uses a numerical value.
  
  The prefactor in the Arrhenius model for the viscosity is called $\eta_0$
  here to distinguish it from the parameter in the MYEGA model.
  The nucleation rate is calculated as $J = J_\infty \exp(\frac{-E_J}{k_B T})$.
  Concluding, the used parameters are:
  \begin{center}
    \begin{tabular}{c|c|c}
    Parameter & Value & Reference \\
    \hline
    $L$ & $\SI{17.9}{\kJ\per\mol} / \SI{33.11}{\cm\tothe{3}\per\mol} = \SI{540.6}{\J\per\m\tothe{3}}$ & \citep{geteExperimentsNew} \\
    $V_m$ & $\SI{55}{\angstrom\tothe{3}}$ & \citep{geteExperimentsNew} \\
    $T_m$ & $\SI{1000}{\K}$ & \citep{geteExperimentsNew} \\
    
    $\tilde{A}$ & $\SI{e-2}{\Pa\m}$ & \citep{geteExperimentsNew} \\
    
    $\eta_\infty$ & $\SI{0.631}{\milli\Pa\s}$ & \citep{geteExperimentsNew} \\
    $T_g$ & $\SI{432.1}{\K}$ & \citep{geteExperimentsNew} \\
    $m$ & $130.7$ & \citep{geteExperimentsNew} \\
    
    $E_a$ & $\SI{1.85}{\eV}$ & \citep{NucleationTheory} \\
    $\eta_0$ & $\SI{1.94e-14}{\Pa\s}$ & \citep{NucleationTheory} \\
    
    $E_J$ &  $\SI{4.3}{\eV}$ & \citep{geteNucleationAndGrowth} \\
    $J_\infty$ & $\SI{5.4e32}{\per\m\squared\per\s}$ & \citep{geteNucleationAndGrowth}
    \end{tabular}
  \end{center}
