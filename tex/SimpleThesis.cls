% A simple class for a thesis.
%
% Builds on the KOMA class scrreprt.
% Needs the following packages:
%  - natbib
%  - tocbibind
%  - xcolor
%  - inputenc
%  - hyperref
%  - geometry
%  - graphicx
%  - caption
%  - subcaption
%
% Provides the following commands and enviroments:
%  - disciplie
%  - university
%  - faculty
%  - institute
%  - firstexaminer
%  - secondexaminer
%  - frontmatter
%  - mainmatter
%
% Copyright (C) 2019 Nicolas Essing
% Don't just copy this and change the name.
% Don't use my name in advertising.
% Selling the unmodified original with no work done is not permitted.
% Except from this, do with this what you want to.
% Code is provided with no warranty.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{SimpleThesis}[2019/06/20 A simple class for a thesis]

% this class extends scrreprt
\LoadClassWithOptions{scrreprt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Input encoding and geometry
\RequirePackage[utf8]{inputenc}
\RequirePackage{hyperref}       % make references clickable
\RequirePackage[                % manually set page layout
    paper=a4paper,
    top=25mm,
    bottom=25mm,
    headsep=10mm
  ]{geometry}
\KOMAoptions{twoside=semi,BCOR=10mm}

% Penalize some unwanted behaviour
\widowpenalty=8000 % No page break after first line of paragraph
\clubpenalty=8000  % No page breakes before last line of paragraph
\binoppenalty=7000 % No line break after binary math operators
\relpenalty=5000   % No line break after math relations

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Provide the options master and bachelor to define thesis type.
% Default is bachelor.
\newcommand{\@thesistype}{Bachelor's}
\DeclareOption{master}{\renewcommand{\@thesistype}{Master's}}
\DeclareOption{bachelor}{\renewcommand{\@thesistype}{Bachelor's}}

% Pass all other given options to superclass
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreprt}}
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Provide commands to give information about this thesis
\newcommand{\discipline}[1]{\newcommand{\@discipline}{#1}}
\newcommand{\university}[1]{\newcommand{\@university}{#1}}
\newcommand{\faculty}[1]{\newcommand{\@faculty}{#1}}
\newcommand{\institute}[1]{\newcommand{\@institute}{#1}}
\newcommand{\firstexaminer}[1]{\newcommand{\@firstexaminer}{#1}}
\newcommand{\secondexaminer}[1]{\newcommand{\@secondexaminer}{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Import some general purpose packages
\RequirePackage{graphicx}       % to include images
\RequirePackage{caption}        % to add captions to images
\RequirePackage{subcaption}     % captions in composed images

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Import bibliography management. Use numerical references in
% square brackets and order the references as they were used
\RequirePackage[square,numbers,sort]{natbib}
\RequirePackage[nottoc,numbib]{tocbibind}
\bibliographystyle{unsrtnat}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define custom blue tones and use them for some fonts
\RequirePackage{xcolor}
\definecolor{customblue1}{cmyk}{75,38,0,0}
\definecolor{customblue2}{cmyk}{45,14,0,0}
\definecolor{customblue3}{cmyk}{100,50,0,0}
\addtokomafont{chapter}{\color{customblue3}}
\addtokomafont{section}{\color{customblue1}}
\addtokomafont{subsection}{\color{customblue2}}
\addtokomafont{subsubsection}{\color{customblue2}}

% Make caption label bold, caption text a little gray
\addtokomafont{captionlabel}{\bfseries}
% \addtokomafont{caption}{\color{darkgray}}

% Line distance
\RequirePackage[onehalfspacing]{setspace}

% Disable newline after caption label
\setcapindent{0pt}

% Define the space above a chapter title
\renewcommand*{\chapterheadstartvskip}{\vspace{2.3\baselineskip}}

% Set some properties of paragraphs
\setlength{\parindent}{0em}   % no indent at new paragraph
\setlength{\parskip}{.5em}    % half a line distance between paragraphs

% Count down to subsections
\setcounter{secnumdepth}{3}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% head and foot line
\RequirePackage[headsepline,footsepline,plainfootsepline]{scrlayer-scrpage}
\addtokomafont{headsepline}{\color{customblue3}}
\addtokomafont{footsepline}{\color{customblue3}}
\automark[chapter]{section}
\ihead[]{}
\chead[]{}
\ohead[]{\MakeUppercase\rightmark}
\ifoot[]{}
\cfoot[]{}
\ofoot[\thepage]{\thepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Commands for frontmatter (table of content, abstract, ...),
% mainmatter (actual content)
\newcommand{\frontmatter}{
  \pagenumbering{roman}
  \singlespace
}
\newcommand{\mainmatter}{
  \clearpage
  \pagenumbering{arabic}
  \onehalfspace
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define a command to get the name of a month from its number
\newcommand{\monthword}[1]{
  \ifcase#1\or January\or February\or March\or April\or May\or
               June\or July\or August\or September\or October\or
               November\or December\fi
  }
                                        
% Provide a command for the title page, in a layout normally
% seen in a thesis
\renewcommand{\maketitle}{
  \begin{titlepage}
    \newgeometry{margin=1in}
    \singlespace
    \centering 
    \vspace*{0.8\baselineskip}
    \parbox{.9\textwidth}{
      \centering
      \color{customblue1}
      \Huge
      \@title
    }
    \\ \vspace*{1.3\baselineskip} 
    by
    \\ \vspace*{1.3\baselineskip}
    {
      \Large
      \@author
      \par
    }
    \vspace*{\baselineskip}
    \huge
    \@thesistype{} Thesis in \@discipline
    \\ \vspace*{1.5\baselineskip}
    \large
    submitted to
    \\ \vspace*{2\baselineskip}
    {
      \large
      \textbf{
	\@faculty
      }
      \\
      \@university
    }
    \par \vspace*{2.5\baselineskip}
    in
    \\
    \monthword{\month}{} \the\year{}
    \\ \ \\
    performed at
    \\
    \@institute
    \\ \ \\
    First Examiner: \@firstexaminer
    \\
    Second Examiner: \@secondexaminer
    \cleardoublepage
  \end{titlepage}
}