\chapter{Determination of Simulation Parameters from Experimental Data}
To simulate any material, some parameters are necessary.
For many materials, all necessary coefficients can be found in literature.
This is however not the case for some materials for which interest has only
recently been aroused.
Regarding \sbte, little literature can be found
amid the many publiations on \GSTxxx{2}{2}{5}.

It is then necessary to obtain information from experimental data.
In this work, experimental data from \citet{expdata} on \sbte{} was analysed and
the results used for simulations of this material.
On thin films of \sbte{} ($\SI{30}{\nm}$),
four experiments were done at different temperatures
from $\SI{115}{\celsius}$ to $\SI{130}{\celsius}$.
The observed area was around $\SI{1}{\mm\squared}$.
For each experiment, the area of the material in amorphous phase was measured,
the grains counted
and the radii of 10 grains tracked.
Data was taken at 10 to 50 different times.

This chapter presents the used fits.
As a test, they are applied to simulated data to demonstrate that the values
set into the model can be reconstructed.
Subsequently, they are used on the experimental data to obtain parameters
for \sbte.
The used methods and their implementation should be useable for similar
experiments as well.

\section{Growth Velocity from Grain Size}
  The most direct method to determine the growth velocity is to measure the size
  of a grain as a function of time.
  It can be calculated as the slope of a linear fit on the radius $r(t)$.
  An example is shown in figure~\ref{fig:expfit_v}.
  
  The only thing important to consider is the domain on which the fit is done.
  In an early stage, the velocity might be influenced by the small curvature of
  the grain through the Gibbs-Thompson effect.
  The growth stops when the grain reaches the sample boundarys or another grain.
  
  \begin{figure}[tbp]
    \centering
    \includegraphics[width=.6\linewidth]{../img/exp_v.png}
    \caption{Linear fit to the radius of a grain as a function of time.
    ($\SI{130}{\celsius}$) }
    \label{fig:expfit_v}
  \end{figure}
  
  The uncertainties on the data points were choosen on the basis of their
  scattering around a linear slope.
  Using this, the relative uncertainties on the velocities were about $3\%$
  and agree with the distribution of the values for different grains.
  Using the latter one as uncertainties on the mean values,
  the following results were obtained:
  
  \begin{center}
    \begin{tabular}{c|c}
      $T \ [\si{\celsius}]$ & $v \ [\si{\nm\per\s}]$ \\
      \hline
      $115$ & $\ 4.29  \pm 0.20$ \\
      $120$ & $\ 8.56  \pm 0.49$ \\
      $125$ & $ 18.11  \pm 0.49$ \\
      $130$ & $ 30.9 \ \pm 1.5 \ $
    \end{tabular}
  \end{center}


\section{Nucleation Rate from Counted Grains}
  One experimentally measurable quantity is the number of grains.
  Assuming homogeneous nucleation, it can be used to determine the nucleation
  rate $J$.
  
  The rate of newly formed grains per time, $\frac{dN}{dt}$,
  is given as the product of $J$ and the volume $V_a$ that is free to
  nucleation:
  \begin{equation}
   \frac{dN}{dt} = J \cdot V_a = J \cdot V \cdot f_a
  \end{equation}
  
  From discrete data points, a value can be estimated for each interval between
  two directly consecutive values.
  With a finite time difference $\Delta t$, $\Delta N$ new nuclei and a linear
  interpolated amorphous fraction $f_a$ one gets
  \begin{equation}
   J = \frac{\Delta N}{\Delta t}\frac{1}{f_a} \ .
  \end{equation}
  
  Assuming a Poisson distribution for $\Delta N$ and neglecting uncertainties on
  $\Delta t$ and $f_a$, the uncertainties and fluctuations on $J$ get larger with time,
  as $f_a$ decreases.
  
  A constant nucleation rate can be obtained as a weighted mean value of the
  momentary rates.
  To test this method, a few simulations were done with different nucleation
  rates and different random number generation seeds.
  The method was then used to recreate the used rate.
  One comparison is shown in figure~\ref{fig:expfit_J_test}.
  In most of these tests, the result of the analysis was approximately
  one sigma away from the value used in the simulation.
  
  \begin{figure}[tbp]
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/simtest_J_1.png}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/simtest_J_2.png}
    \end{subfigure}
    \caption{Comparison of the nucleation rate that was set in the simulation
      ($\SI[per-mode=reciprocal]{2.378}{\per\s}$),
      the number of grains in the simulation
      and the mean rate obtained from the presented fit on this data
      ($\SI[per-mode=reciprocal]{2.256\pm0.092}{\per\s}$ on the left and
      $\SI[per-mode=reciprocal]{2.324\pm0.095}{\per\s}$ on the right).
      Both images use the same parameters, but a different seed for random
      number generation.}
    \label{fig:expfit_J_test}
  \end{figure}
  
  For the available data on \sbte{} however,
  the assumption of a constant nucleation rate does not seem to hold.
  This can be seen in the data shown in figure~\ref{fig:expfit_J} and will also
  be discussed in the comparison with simulated data.
  As a workaround, several rates were fitted over different time intervals.
  Inhomogenous nucleation can be an explanation for decreasing rates,
  as the places with higher nucleation probability are crystallised earlier.
  It is however not an explanation for increasing rates.
  Another explanation for the occurring effect in the classical nucleation
  theory is a non constant temperature, which should be possible consider in
  simulations when including the heating process,
  as e.g. done by \citet{sebastiansThesis}.
  Otherwise, a more complex model would be necessary to take these effects into
  account.
  
  Especially for late times and thus small $f_a$ and $\Delta N$,
  the fluctuations can get greater than the mean value,
  which makes it difficult to see if the rate is not constant.
  It can be made easier by integrating the rate again.
  This gives a number of nuclei corrected by the effect of the shrinking
  volume free to nucleation and is called $N_{\text{corrected}}$ here.
  For continuous respectively discrete time, it is calculated as
  \begin{align}
   N_{\text{corrected}} &= \int_{0}^{t} dt' \, \frac{dN}{dt} \frac{1}{f_a} \\
                &\hat{=} \sum \frac{\Delta N}{\Delta t} \frac{1}{f_a}
  \end{align}
  and should be a linear function of time if $J$ is constant.
  Different rates can be seen as different slopes.
  For one measurement, this is shown in figure~\ref{fig:expfit_J}
  
%   TODO show results?
%   Fitting a single nucleation rate to the data yields the following results:
%   \begin{center}
%     \begin{tabular}{c|c}
%       $T \ [}{\celsius}]$ & $J \ [\frac{10^{10}}{\text{m}^3\text{s}}]$ \\
%       \hline
%       $115$ & $\ \ 30.2 \pm \ 2.5$ \\
%       $120$ & $\ 105.5 \pm \ 5.6$ \\
%       $125$ & $\ 351 \pm 16\ \ $ \\
%       $130$ & $2065 \pm \ 6\ $
%     \end{tabular}
%   \end{center}
  
  \begin{figure}[tbp]
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/exp_J_115.png}
      \caption{The momentary nucleation rates, normalized on the same volume.
          The used mean rates are plotted over their domain as a horizontal line.}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/exp_Nc_115.png}
      \caption{The integrated normalized rates from the left image,
          together with the integrated mean rates.}
    \end{subfigure}
    \caption{Estimation of the nucleation rate for $\SI{115}{\celsius}$.
        The resulting rates are $0$, $7.9$, $4.5$ and $13.6$
        in units of $\SI[per-mode=reciprocal]{e3}{\per\s}$.}
    \label{fig:expfit_J}
  \end{figure}
  
\section{Completion from Crystallised fraction}
  In the JMAK theory, the amorphous fraction of the sample as a function of time
  can be written as $f_a = \exp(-k\cdot t^n)$,
  where $n$ depends on only the dimension
  and $k$ can be calculated from $v$ and $J$.
  Thus, a fit of this model to the proposed funtion can give an estimation
  for $k$.
  
  This can be used to calculate either $v$ or $J$ if the other one is known from
  other data or can be taken from literature.
  
  For the present data, both $v$ and $J$ can be determined differently.
  The values for $k$ resulting from both methods were compared,
  with varying consistency.
  Two examples with different consistencies are shown
  in figure~\ref{fig:expfit_jmak_comp}.
  In these test however, a constant nucleation rate was assumed, which is not
  in good agreement with the rates obtained from the counted nuclei.
  More complex equations could be constructed taking different rates into
  account to consider this effect in the fit.
  
  %TODO present JMAK fit, linearised and normal
  
  \begin{figure}[htbp]
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/exp_jmak_120.png}
      \caption{$\SI{120}{\celsius}$: Fit to JMAK model gives
          $k = \SI[per-mode=reciprocal]{2141 \pm 1 e-15}{\per\s\tothe{3}}$,
          from $J$ and $v$ one gets
          $k = \SI[per-mode=reciprocal]{2432 \pm 307 e-15}{\s\tothe{-3}}$.}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/exp_jmak_125.png}
      \caption{$\SI{125}{\celsius}$: Fit to JMAK model gives
          $k = \SI[per-mode=reciprocal]{2199 \pm 5 e-14}{\s\tothe{-3}}$
          from $J$ and $v$ one gets
          $k = \SI[per-mode=reciprocal]{3623 \pm 256 e-14}{\s\tothe{-3}}$.}
    \end{subfigure}
    \caption{Comparison of the results of a fit to $f_c$ with the JMAK theory
        and values obtained from fits to $v$ and $J$.}
    \label{fig:expfit_jmak_comp}
  \end{figure}

\clearpage
\section{Temperature Dependence of Growth Velocity}
  \label{sec:fit_v}
  The estimated growth velocities for different temperatures can be described by
  one temperature dependent model.
  With only four data points available for low temperatures,
  an Arrhenius was used.
  It can sufficiently describe the behaviour, as can be seen in
  figure~\ref{fig:expfit_v_arrhenius}. % chisq/ndf of 3.0
  Fitting an Arrhenius behaviour to the mobility gives similar results.
  For the use of a more complex model, more data is necessary.
  
  Collectively, the growth velocity
  \begin{align}
   v  &= \SI{1.9 \pm 3.0 e15}{\m\per\s}
         \exp\left(-\frac{\SI{1.82\pm0.05}{\eV}}{k_B T}\right)
  \end{align}
  should be usable for \sbte{} in this temperature range and vicinity.
  
  \begin{figure}[htbp]
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/exp_v_arrhenius_lin.png}
      \caption{Arrhenius Plot: $\ln(v)$ over $T^{-1}$}
    \end{subfigure}
    \hfill
    \begin{subfigure}{.49\linewidth}
      \includegraphics[width=\linewidth]{../img/exp_v_arrhenius.png}
      \caption{$v$ over $T$}
    \end{subfigure}
    \caption{Fitting Arrhenius behaviour to groth velocity.}
    \label{fig:expfit_v_arrhenius}
  \end{figure}
  

\section{Classical Nucleation Theory}
  In the data, the nucleation rates are not constant over time,
  an effect not described by the introduced classical nucleation theory.
  The time dependences of the rates are not similar for the different
  measurements, which makes it especially complex to find a general model.
  
  To get an approximation providing values for the nucleation rate for other
  temperatures, the mean rates of each measurement can be used.
  The classical nucleation theory model (equatoin~\ref{eq:cnt_J})
  was fitted to this values.
  For $\dGv$, the introduced model was used
  together with values from other experiments~\citep{expdata,sb2teStructure}
  that are listed in section~\ref{sec:sbteresults} together with all other
  used parameters.
  For $\eta$, an Arrhenius behaviour was assumed with the same activation energy
  as obtained from the fit onto the velocities,
  which is motivated by the discussion in section~\ref{sec:arrheniusviscosity}.
  This leaves the interfacial energy and a prefactor as free parameters,
  where the latter one is the product of several quantities occuring in the
  theoretical equation.
  With more than 4 measurements it should also be possible to leave more than
  two free parameters and thus include e.g. another model for the viscosity.
  As the rates are not constant, this will probably not improve quality of the
  results significantly.
  
  \begin{figure}[tbp]
    \centering
    \includegraphics[width=.5\linewidth]{../img/exp_cnt.png}
    \caption{Classical nucleation theory fit onto experiment data on \sbte.}
    \label{fig:expfit_cnt}
    \label{fig:cntfit}
  \end{figure}
  
  The fit does only approximately describe the data.
  The resulting values of
  \begin{align}
   \lambda^3 \eta_\infty \left(\frac{3 V_m}{4 \pi}\right)^{(2/3)}
          &= \SI{1.2 \pm 27.7 e99}{\J\m\squared\s} \\
   \gamma &= \SI{7.29 \pm 0.49 e-2}{\J\per\m\squared}
  \end{align}
  have quite large uncertainties, but should be usable as an initial guess.
  The interfacial energy is in the expected order of magnitude,
  while the prefactor is several orders of magnitude below values for similar
  materials.
  The fit is shown in figure~\ref{fig:expfit_cnt}.
