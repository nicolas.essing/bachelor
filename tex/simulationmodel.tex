\chapter{Investigation of the Simulation Model}
This chapter deals with the numerical implementation of the phase field model,
which includes improvements to the program
and simple simulations to test the limits of the calculated behaviour.

\section{Implementation Improvements}
  
  \subsection{Laplace Operator}
    In each time step, the Laplace operator of the phase field $\Phi$ has to be
    calculated (cf.~eq.~\ref{eq:pfderivative}),
    next to only a few elementwise multiplications.
    It is a crucial part regarding performance.
    The implementation of the numerical Laplace operator in the original code
    was however not optimized.
    
    % I cache isnan(phi) and dont use find() any more
    % Per 4 runs, original code took 0.2899, improved one 0.2703, new one 0.0886
    Some minor improvements, including the use of locial indexing and caching of
    some intermediate results that are used again, lead to runtime shorter by
    approximately $7\%$.
    
    An important property of the operator used is the consideration of the
    boundary condition $\nabla \Phi = 0$,
    which is assumed not only at the outer boundaries due to the finite size of
    the sample, but also next to all embedded structures.
    Those are implemented by a value of \NaN{} in the field for $\Phi$ and
    typically do not change.
    The effects of this assumption can, to some extend, be calculated independent
    of the rest of the state of $\Phi$.
    This calculation can be done outside of the time evolution loop. \\
    A rewritten implementation using this lead, neglecting the initial work, to
    a runtime shorter by approximately $69\%$ compared to the original code.
    
    The performances for different numbers of runs are compared in
    figure~\ref{fig:laplacian_performance}.
    
    \begin{figure}[htbp]
     \centering
     \includegraphics[width=.6\columnwidth]{../img/perf_laplacian.png}
     \caption{Comparison of the runtime of the Laplace operator implementations
         for different numbers of runs with the same structures.
         The used field was of size $1000 \times 1000 \times 100$
         with a structure embedded in shape of the lettering ``RWTH''.
         (Absolute time values are of course strongly hardware specific.
         The examples in this work were performed on a NVIDIA Tesla K40c GPU.)}
     \label{fig:laplacian_performance}
    \end{figure}
  
  \subsection{Grain Tracking Algorithm}
    In the phase field itself there are no informations encoded about the grain
    distribution. If an area is fully crystallised, the grain sizes and shapes
    can not be reconstructed.
    To receive this informations, a second field $\Psi$ is introduced, which is
    zero in the amorphous phase and holds a number assigning this point to a
    grain where the material is crystallised.
    This field is evolved together with the phase field.
    At positions where $\Phi$ gets over some threshold, $\Psi$ is changed to the
    value of a non zero neighbouring point.
    Such a point should always exist as long as a nucleus does not grow more
    than one grid point in one time step.
    It is however not well defined when two grains grow into each other and
    there are different non zero neighbours.
    In this case, a special value \NaN{} is set to mark a grain boundary.
    
    % maxneigh has three copies, minneigh also. Main code holds 4 copies
    % parallel.
    % maxneigh & minneigh do 6*2+3 runs, main code 8 (including copiing)
    In the original implementation, this was achieved in three steps:
    First choose the largest neighbour and save the result.
    Second, choose the smallest non zero neighbour.
    Third, set the first result to \NaN{} where it is different from the second
    one and keep this as the new field. \\
    This algorithm needs to run over the whole field many times 
    (about 20 times including the copy processes)
    and makes several copies in the process
    (Up to 6 different fields of this size are in the memory at the same time).
    % 100x100x20 sample without grains: 11.8s, with grains: 16.4
    For a normal simulation, the grain tracking took more than $25\%$ of the
    computing time.
    
    A new implementation was written that needs only one copy and six runs over
    the field, one for each direction a neighbouring point can be found.
    First of all, this implementation uses unsigned 32 bit integers instead of
    32 bit single precison floating point numbers, offering more than 300 times
    more equidistant numbers on the same memory and with $20\%$ to $30\%$ better
    performance for basic operations like additions or comparisons. \\
    A stencil algorithm was written that implements the specifications above
    comparing a point with one neighbour at a time.
    A ``branch to composition'' technique~\citep{computerOrganizations} was used
    to avoid expensive branching operations.
    
    Overall, the new implementation needs about $9\%$ the time of the old one
    and should thus shorten the overall runtime by more than $20\%$.
  
  \subsection{Other Improvements and Performance Summary}
    Apart from the discussed changes, some minor improvements were made. \\
    Several quantities are now calculated outside of loops,
    including for example $M \frac{\epsilon^2}{h^2}$,
    where $h$ is the grid point distance. \\
    For simulations with rare nucleation or slow growth, it is not necessary to
    consider nucleation and grain identification each timestep.
    Methods are provided to adjust this behaviour.
    
    An overall comparison was done on a $100 \times 100 \times 20$ pixel sample
    of \GSTxxx{2}{2}{5} at $T=\SI{404}{\K}$, with a small rectangular structure
    embedded.
    A grid spacing of $h=\SI{1}{\nm}$ and a timestep of $dt=\SI{10}{\s}$ were
    choosen.
    Both nucleation and grain identification were done every timestep. \\
    
    Per 150 simulation steps, the original code takes $\SI{10.7}{\s}$ while the new
    code only needs $\SI{4.2}{\s}$, which is less than $40\%$ of the expense.
    The runtime for different amount of steps is compared in
    figure~\ref{fig:overallperformance}.
    
    \begin{figure}[htbp]
     \centering
     \includegraphics[width=.6\columnwidth]{../img/perf_overall.png}
     \caption{Comparison of the overall runtime of the implementations as a
     function of the number of simulation steps.}
     \label{fig:overallperformance}
    \end{figure}

  \subsection{Nucleation}
    The existing nucleation algorithm (described in ref.~\citep{zhiyang}) worked
    only for a limited range of parameters.
    Problems include the convergence of the growth process:
    For $r_c > h$, a single point was set to and kept at $\Phi = 1$
    and the field was evolved until the critical size was reached.
    For some parameters however, this growth process did not reach the desired
    size in reasonable time.
    Another problem occured for $r_c \leq h$, where only a few points were
    changed to $\Phi > 0$.
    This nuclei did not have a sufficiently smooth interface, which lead,
    depending on $\tau$, to large deviations from the desired behaviour.
    
    Different approaches were tested to derive or generate a better initial
    shape, but with little success.
    The currently implemented algorithm uses a predefined functional form
    that has a size slightly above the critical size and uses a similar form as
    the analytical solution for a one dimensional interface.
    For small grain sizes $r_c \lesssim \zeta$,
    deviations from the physical behaviour are expected.

  \subsection{Structure and Reuseability}
    A goal of this work was to make the simulation model useable for simulations
    on other materials. It should be possible to build various simulations using
    different physical models without much work required to read oneself up.
    This requires a clear and reuseable code structure.
    
    The simulation was restructured using object orientated programming.
    Specific physical models are implemented and can be easily added using
    inheritance.
    Physical parameters can be manipulated manually or replaced by functions
    depending on other properties.
    
    To help using the program, documentation is included for almost every class,
    method and property.
    Most internal functionalities, like data type conversions and interactions
    with the GPU device, are hidden and done automatically.
    Consistency checks and error messages are included in most functions.
    For modifications that require to change the existing code,
    explanations are included in all difficult passages.
    
    For all concerns regarding numerical resolution and for all calculated
    quantities such as the critical radius $r_c$ and the
    interface relaxation time $\tau$, methods are provided to check these for a
    simulation using arbitrary physical models.
    This includes simple \texttt{get()} functions as well as a complete summary
    regarding the expected behaviour of the simulation,
    which should be helpful choosing e.g. in which intervals results should be
    exported.
    
    Thus, it should be possible to create, manipulate and control a simulation
    using only the given interface and its documentation without having to
    deal with the actual code.
    Methods to save and recreate a state of the simulation, to inspect and to
    export properties are provided in an easy to use way as well.
    
    As the simulation is implemeted in MATLAB, one improvement yet to be done
    would be to port the code to another language that offers higher performance
    and better usability itself.

\section{Numerical Limits}
  In numerical simulations of a phase field model, there is a conflict of three
  interests. One the one hand, for $\zeta \rightarrow 0$ the phase field model
  convergences to the classical sharp interface description.
  Secondly, for $h \rightarrow 0$, the discretized numerical description
  converges to the analytical solution of the phase field model.
  On the other hand, the computational effort raises with smaller $h$,
  for three dimensional simulations proportional to $h^{-3}$.
  
  In this section, mainly the second aspect is analysed.
  To this end, several tests were created and run comparing the analytical
  solutions with simulation results.
  As the system is always invariant under scaling of all lenghts, times or
  energies, all tests are performed for dimensionless settings.
  
  \subsection{Interface Form Relaxation}
    It was calculated that an interface of a shape different from the
    equilibrium shape would approach this in a time about $\tau$.
    This is especially of interest in the context of ad-hoc implemented
    nucleation, where initial nuclei are inserted that may differ from the
    actual critical shape.
    The predicted behaviour was numerically tested for different settings,
    which is shown in figure~\ref{fig:tau_demo}.
    
    With a dimensionless grid spacing of $1$, a shape with a thickness $\zeta = 3$
    can be sufficiently represented and fit on a sample of size $100$.
    The growth velocity was set to $0$.
    Different values for $\tau$ were simulated starting from several initial
    shapes, including a sharp interface and wider shapes.
    The time step was adjusted for each simulation.
    
    \begin{figure}[tbp]
     \begin{subfigure}{.49\columnwidth}
      \includegraphics[width=\columnwidth]{../img/tau_shape.png}
      \caption{Visualization of the relaxation process for an interface wider
        than the equilibrium shape. In the legend are time values in
        units of $\tau$. The relaxation process is visible.}
     \end{subfigure}
     \hfill
     \begin{subfigure}{.49\columnwidth}
      \includegraphics[width=\columnwidth]{../img/tau_time.png}
      \caption{The integrated difference between the field and the theoretical
        equilibrium shape as a function of time for different values of $\tau$.
        The relaxation is approximately exponential with a slope dependent
        on $\tau$.}
     \end{subfigure}
     \caption{Tests regarding the relaxation of the interface shape.}
     \label{fig:tau_demo}
    \end{figure}
    
    This aspect of the theory seems to hold for arbitrary values of $\tau$.
    Note that for given $\zeta$ and $\tau$ together with $v=0$,
    $r_c = \infty$, which is resolved for any grid point distance.
    As $\tau$ is the only time entering the simulation, with an adjusted time
    step there are little changes: $\dGv$ is $0$, $\epsilon^2$ and $W$ increase
    in the same manner as the time step decreases.
  
  \subsection{Interface Thickness}
    In most cases, the physical interface thickness is smaller than the
    resolution of the simulation.
    For best consistency, the interface thickness set in the simulation should
    be as close as possible to the physical one.
    For values smaller than the grid point distance however, the dicretisation
    of the derivatives leads to larger deviations.
    
    To test this limitation, the ratio of $\zeta$ to $h$ was analysed in
    analytically solveable, one dimensional setups.
    An example is shown in figure~\ref{fig:num_v_DIT}.
    The remaining free parameter $r_c$ was varied over a few orders of magnitude
    compared to $\zeta$.
    This showed that it has no strong influence in this setting apart from
    random changes to the numerical errors.
    The absolute value of the velocity does not matter, as the time step size
    and the simulation time are adapted accordingly.
    
    The results show that an interface thickness larger than the spacing
    reproduces the theoretical results very well.
    For $\zeta = h$, there are small deviations noticeable,
    while smaller values can, depending on $r_c$, lead to large deviations.
    %TODO exact values
    
    \begin{figure}[htbp]
     \centering
     \includegraphics[width=.6\columnwidth]{../img/num_v_DIT.png}
     \caption{Comparison of different interface thicknesses.
         In a one dimensional sample with a grid spacing $h=1$, the analytical
         equilibrium solution was put in centered around $x_0 = 20$ and evolved
         $t=30$ with a velocity $v=1$, so that an interface centered around
         $x_f = 70$ is expected. It can be seen that the numeric implementation
         fulfills the theory for large $\zeta$, while large deviations occur
         for $\zeta \leq h$.}
     \label{fig:num_v_DIT}
    \end{figure}

  \subsection{Critical Radius Length Scale}
    \label{sec:num_rcrit}
    Through the use of a numerical approximation of the Laplace operator,
    an anisotropy is introduced to the model.
    There is only a finite number of neighbouring points considered to
    approximate the derivative.
    The directions to these are treated different from other directions.
    In this model, neighbouring points effect each other only through the
    differential. The rest of the time evolution for a point
    (equation~\ref{eq:pfderivative}) does only depend on the phase at the point
    itself, which is done as an elementwise calculation in the discretized
    implementation.
    This means that a nucleated point leads to nucleation in the neighbouring
    points used by the implementation of the Laplace operator.
    If this happens in each time step without restrictions, a nucleus grows in
    the shape one gets from connecting the used neighbouring points.
    
    To get physically sensible results, the effects of this numerical anisotropy
    must be suppressed.
    As discussed in theory, curvatures are penalized on a length scale of $r_c$.
    The idea behind this analysis is that this penalizing gets weak if this
    length scale is not resolved.
    In this case, the shape of a nucleus should mainly depend on the used
    approximation for the Laplacian, while for $h$ small enough the nuclei
    should grow in spherical shape.
    
    This idea was tested in several simulations and could be confirmed.
    Examples are shown in figure~\ref{fig:numlim_rc}.
    Starting from an diffuse with only slight initial anisotropies, these
    effects can still be seen for $h \geq 0.1 r_c$,
    but vanishes for $h \leq r_c$.
    Starting by contrast with an cubic shape, an even smaller $h$ is necessary.
    
    The use of a higher order approximation of the Laplace operator might
    reduce the effect, as more neighbours are considered and wighted with
    different coefficients depending on the distance.
    This was tested using a second, higher order implementation.
    The results do however show that the limitation to $h$ resp. $r_c$ stay in
    the same order of magnitude.
    The effect of different resulting shapes depending on the used neighbouring
    points could be confirmed.
    
    One additional effect has to be considered that could also be the origin of
    the observed phenomena.
    Normally, the time step $dt$ is choosen according to the numerical stability
    criterion given in equation~\ref{eq:numstab}.
    For smaller values of $r_c$, this leads to larger time steps and thus to
    less precision.
    To exclude this, simulations were done using the smallest necessary time
    step for all tested values $r_c$.
    It was confirmed that the effect can not be suppressed by smaller time
    steps.
    
    \begin{figure}[tbp]
     \centering
     \begin{subfigure}{.32\columnwidth}
      \includegraphics[width=\columnwidth]{../img/num_res_p_1.png}      
      \caption{$r_c = 0.01 \cdot h$}
     \end{subfigure}
     \begin{subfigure}{.32\columnwidth}
      \includegraphics[width=\columnwidth]{../img/num_res_p_2.png}      
      \caption{$r_c = 0.1 \cdot h$}
     \end{subfigure}
     \begin{subfigure}{.32\columnwidth}
      \includegraphics[width=\columnwidth]{../img/num_res_p_3.png}      
      \caption{$r_c = h$}
     \end{subfigure}
     \\
     \begin{subfigure}{.32\columnwidth}
      \includegraphics[width=\columnwidth]{../img/num_res_p_init.png}      
      \caption{Initial shape \\ \ }
     \end{subfigure}
     \begin{subfigure}{.32\columnwidth}
      \includegraphics[width=\columnwidth]{../img/num_res_p_1_27.png}      
      \caption{$r_c = 0.01 \cdot h$, higher order Laplace operator}
     \end{subfigure}
     \begin{subfigure}{.32\columnwidth}
      \includegraphics[width=\columnwidth]{../img/num_res_p_1_cdt.png}      
      \caption{$r_c = 0.01 \cdot h$, smaller time step}
     \end{subfigure}
     \caption{Tests regarding the ratio of $r_c$ and $h$.
         The shown initial shape was evolved with different values for $r_c$ and
         constant $\zeta$ and $v$, until a certain size was reached.
         For the initial shape, the algorithm used in full simulations was
         utilized. It acts only on a cube of smaller size (in this example,
         its side length is about a fourth the length of the simulated sample).
         The boundaries of this cube are an initial disturbance.
         It can be seen that the anisotropy effect is supressed for larger $r_c$
         while it has large influence on the simulation for small $r_c$.
         Additional tests are shown using a different Laplace operator
         implementation and smaller time steps.
         It can be seen that both changes do not largely improve the results.}
     \label{fig:numlim_rc}
    \end{figure}
  
  \subsection{Other Ratios}
    As discussed, there are three independent parameters in the phase field
    evolution equation, which can be expressed through $\zeta$, $v$ and $r_c$.
    In addition to them, there are two numerical settings, the time step $dt$
    and the distance between grid points $h$.
    
    The ratio of $\zeta$ to $h$ has been analysed,
    as well as the on of $r_c$ to $h$.
    Regarding the ratio of $\zeta$ to $r_c$, it can be calculated from
    equation~\ref{eq:doublewellrestriction} that if $\zeta < r_c$ is not
    fulfilled, the free energy is no longer a double well potential and
    $\Phi = 0$ is an unstable equilibrium.
    If the implementation does not restrict $\Phi$ on the interval from 0 to 1,
    this can lead to undesired behaviour.
    In the context of nucleation, this also yields that a critical nucleus does
    not only consist of interfacial region, but has at least to some
    extend an inner, crystallised part.
    Apart from these points, no restrictions were found in the tested cases.
    
    Together with $dt$, the ratio of $v dt$ to the length scales if of interest.
    The numerical stability criterion eq.~\ref{eq:numstab} yields that the ratio
    of $v dt$ to $h$ is about a third of the one of $h$ to $r_c$,
    for which a restriction has been discussed.
    As long as this restriction is fulfilled, the expected growth in one time
    step should be smaller than the grid point spacing.
    
    The ratio of $v dt$ to $\zeta$ and $r_c$ should not be critical, as none of
    them is allowed be significantly smaller than $h$.
    Other ratios are not dimensionless and should thus not be relevant alone,
    but only in comparison to other quantities.
