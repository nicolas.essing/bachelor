% Testing my calculations on tau. Here I want to show that I can vary tau
% independent from the interface shape. For that, I build simulations with
% the same initial and eqilibrium shape but different tau and evolve each
% of them its own tau. The resulting fields should be the same.

% Normalised parameters
spacing = 1;
DIT = 3;
taus = [0.01,1,100,10000];
evolvetaus = 0.5;

% Build a simulation using these parameters
import phasefieldMethod.ExampleSimulation
S = ExampleSimulation(spacing, 42);

% Initial interface form, use a steep or a wider interface compared to the
% eqilibrium shape.
X = 1:100;
% initialfield = 1. * (X<50);
initialfield = 1/2*(1+tanh((50-X)./(3*2*DIT)));
S = S.setField(initialfield);

% For each scale, build a simulation with tau scaled by this value
Ss = cell(length(taus));
for i=1:length(taus)
    tau = taus(i);
    Si = S.setOtherParameters(DIT, 0, tau);
    Si = Si.adjustTimestep(10);
    Si = Si.apply();
    Ss{i} = Si;
end

% Evolve each simulation its tau
for i=1:length(taus)
    Si = Ss{i};
    Ss{i} = Ss{i}.evolve(evolvetaus * Ss{i}.relaxationTime);
end

% Plot all of them togeter with initial and eqilibrium shape
hold on;
plot(initialfield, 'LineWidth',1.5);
markers = '+xs^';
for i=1:length(taus)
    Si = Ss{i};
    m = markers(i);
    plot(Si.phasefield,m);
end
plot( 1/2*(1+tanh((50-X)./(2*DIT))), 'LineWidth',1.5);
legend([{'initial shape'}; ...
        mat2cell(num2str(taus'),ones(1,length(taus))); ...
        {'equilibrium shape'}]);
