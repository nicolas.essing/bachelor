% General tests for a one dimensional setting. Different initial interfaces
% can be set, evolved and compared.

% Values for DIT,   v,   r_c and a boolean whether to use diffuse (true) or
% sharp interface in the initial setting.
testcases = [  2,   1,   0.01, true;
               2,   1,   0.1,  true;
               2,   1,   1.0,  true;
               2,   1,  10.0,  true;
               2,   1, 100.0,  true];

% Settings
spacing = 1;
goodness = 3;
length = 50;
startpos = 9.5;
time = 30.5;

import phasefieldMethod.ExampleSimulation

testcasessize = size(testcases);
numberofcases = testcasessize(1);
Ss = cell(1,numberofcases);
labels = cell(size(Ss));
for i=1:numberofcases
    DIT = testcases(i,1);
    v = testcases(i,2);
    r_c = testcases(i,3);
    initial_interface = testcases(i,4);
    
    % Build a simulation
    S = ExampleSimulation(spacing, 42);
    S = S.setParameters(DIT, v, r_c);
    S = S.adjustTimestep(goodness);
    S = S.apply();
    
    % Initial setup: either the analytical solution or just a sharp
    % interface
    [X,Y,Z] = ndgrid(1:1,1:1,1:length);
    if initial_interface
        phasefield = 1/2*(1+tanh((startpos-Z)*S.spacing ...
                                               /2 ./S.interfaceThickness));
    else
        phasefield = 1. * (Z<startpos);
    end
    S = S.setField(phasefield);
    
    % Build a label
    labels{i} = strcat(  '\zeta=', num2str(DIT), ...
                       ', v=',     num2str(v), ...
                       ', r_c=',   num2str(r_c));

    % Evolve and save
    [S, rt] = S.evolve(time);
    Ss{i} = S;
end

% Plot to comparison
hold on
for i = 1:numel(Ss)
    plot(squeeze(Ss{i}.phasefield(1,1,1:end)));
end
legend(labels);
