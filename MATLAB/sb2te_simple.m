
% Clean up
clear;
gpuDevice(1);

% Set parameters
T_C = 130;
spacing = 200e-9;
DIT = spacing;
r_c = 2e-6;
A = 1e-6;
exportSettings = [0, 10, 0];
folder = '../data/sb2te_simple_130/';

% Build a simulation
import phasefieldMethod.*;

T = T_C + 273.15;
Nxy = round(sqrt(A)/spacing);
phasefield = zeros(Nxy,Nxy,1, 'single','gpuArray');

S = SbTeSimulation(spacing, 42, 10);
S = S.setField(phasefield);
S = S.setParameters(DIT, r_c, T);
S = S.cntNucleationRate();
% Correct projection to 2D
S = S.manualNucleationRate(S.J * 30e-9/spacing);
S = S.adjustTimestep(2);
S = S.apply();

% Choose good timesteps for grain identification and nucleation.
S = S.autoChooseGrainTimestep(1.5);
S = S.autoChooseNucleationTimestep(1);
S = S.apply();

% Tell me how it looks
S.analyse();
assert(isempty(input('Enter everything thats wrong: ','s')),'manual stop');

R = Run.createFromSimulation(S, folder);
R = R.exportSettings(1,1,0);
R = R.setSaveEvery(exportSettings(1), exportSettings(2), exportSettings(3));
R.info = ['Simulating Sb2Te with parameters from fits, using a single ' ...
          'nucleation rate from CNT fit.'];
R = R.saveAnalysis();
R = R.printLine();
R = R.evolveToFraction(0.99);
R = R.backupSelf();
