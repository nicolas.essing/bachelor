% Overall performance test using an example on GST
gpuDevice(1);
clear;
tic;

import phasefieldMethod.GSTSimulation;
import phasefieldMethod.Run;

% SIMULATION SETTINGS
spacing = 1e-9;         % m
% nucleationEvery = 3e-4;
% grainEvery = 5e-3;
% saveEvery = 100;
% printEvery = 10;
% exportEvery = 25;
% foldername = '../data/gst_470K_struct';

S = GSTSimulation(spacing, 42);

% PHYSICAL CONSTANTS
% general
S.IE = 0.06;           % interfaial energy, J/m^2
S.DIT = 0.5e-9;        % diffuse interface thickness, m
S.Tm = 889;            % melting temperature, K
S.L = 625000000;       % latent heat, J/(m^3)

% for nucleation
S.Vm = 2.9e-28;       % volume of a monomer, m^3
S.ninf = 0.012;       % constant in viscosity equation, Pa*s
S.m = 140;            % fragility
S.Tg = 472;           % temperature at which viscosity is 1012Pa
S.jd = 2.99e-10;      % jump distance, m
S.Ea = 2.3 * 1.6e-19; % activation energy,eV
S.Tglass = 534;       % glass transition temperature, K
S.wa = 180;           % weting angle, degree (0<=wa<=180, 180 means homogeneous)

S.T = 404;

phasefield = zeros(100,100,20,'single','gpuArray');
phasefield(45:55,45:55,1:10) = NaN;
S = S.setField(phasefield);

S = S.apply();

S = S.settings(10,10);

S = S.grainSettings(S.dtime, S.dtime);

toc;
for i=[1,2,3]
    S = S.evolve(1500);
    toc;
end
