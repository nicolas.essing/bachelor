% Tests and performance comparison of my an zhiyangs laplace operators

% % Correctness: 1d example
% X = 0:0.1:2*pi;
% F = -cos(X);
% % Calculate laplacian using old and new function
% [structs, neighs] = phasefieldMethod.analyse_structure_7(F);
% lF1 = laplace(F,0.1);
% lF2 = phasefieldMethod.laplacian_7(F,structs, neighs) / (0.1*0.1);
% % Plot together with analytical solution
% hold on;
% plot(X,lF1);
% plot(X,lF2);
% plot(X,-F);
% legend('old','new','analytic');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performance comparison
% The numbers of runs to test
Ns = [0,1,2,4,8];

% Load the rwth structure and build a 
rwthmatfile = matfile('zhiyang/rwthmatrice.mat');
rwthmat = rwthmatfile.na0;
mat = gpuArray(rand([size(rwthmat),100],'single'));
for i=1:20
    mat(:,:,i) = rwthmat;
end

% For each N, run the test functions and save the duration
ts1 = zeros(size(Ns));
ts2 = zeros(size(Ns));
ts3 = zeros(size(Ns));
for i=1:length(Ns)
    N = Ns(i);
    ts1(i) = gputimeit(@() test1(mat,N));
    ts2(i) = gputimeit(@() test2(mat,N));
    ts3(i) = gputimeit(@() test3(mat,N));
end

% Plot the results
hold on;
set(gca,'FontSize',16);
% plot(Ns, ts1);
% plot(Ns, ts2);
% plot(Ns, ts3);
plot(Ns, ts1, 'LineWidth',1.2,'Marker','s');
plot(Ns, ts2, 'LineWidth',1.2,'Marker','s');
plot(Ns, ts3, 'LineWidth',1.2,'Marker','s');
xlabel('number of runs');
ylabel('time [s]');
legend('original','improved','rewritten','Location','northwest');

function mat = test1(mat,N)
    % Test the initial function by just calculating laplacians
    for i=1:N
        mat = laplace(mat,1);
    end
end

function mat = test2(mat,N)
    % Test the slightly improved function
    for i=1:N
        mat = laplace_improved(mat,1);
    end
end

function mat = test3(mat, N)
    % Test the new function
    [structs, neighs] = phasefieldMethod.analyse_structure_7(mat);
    for i=1:N
        mat = phasefieldMethod.laplacian_7(mat, structs, neighs);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Zhi Yangs original method

function[lapop] = laplace(op,dx)
    %commend out line 35,119,121 will give u 27-point stencil
    %6 Surfaces
    [Nx, Ny, Nz] = size(op);

    h0 = op;  %05
    h0(1:Nx,1:Ny,2:Nz)=op(1:Nx,1:Ny,1:Nz-1);
    h0(find(isnan(h0))) = op(find(isnan(h0)));

    h00 = op; %11
    h00(1:Nx,2:Ny,1:Nz)=op(1:Nx,1:Ny-1,1:Nz);
    h00(find(isnan(h00))) = op(find(isnan(h00)));
    lp=h0+h00; clear h00;

    h0 = op; %13
    h0(2:Nx,1:Ny,1:Nz)=op(1:Nx-1,1:Ny,1:Nz);
    h0(find(isnan(h0))) = op(find(isnan(h0)));
    lp = lp+h0;

    h0 = op; %15
    h0(1:Nx-1,1:Ny,1:Nz)=op(2:Nx,1:Ny,1:Nz);
    h0(find(isnan(h0))) = op(find(isnan(h0)));
    lp = lp+h0;

    h0 = op; %17
    h0(1:Nx,1:Ny-1,1:Nz)=op(1:Nx,2:Ny,1:Nz);
    h0(find(isnan(h0))) = op(find(isnan(h0)));
    lp = lp+h0;

    h0 = op; %23
    h0(1:Nx,1:Ny,1:Nz-1)=op(1:Nx,1:Ny,2:Nz);
    h0(find(isnan(h0))) = op(find(isnan(h0)));
    lp = lp+h0;

    if(Nx<0)
        %12 Edges
        h0 = op; %02
        h0(1:Nx,2:Ny,2:Nz)=op(1:Nx,1:Ny-1,1:Nz-1);
        lp = lp + 0.5*h0;

        h0 = op; %04
        h0(2:Nx,1:Ny,2:Nz)=op(1:Nx-1,1:Ny,1:Nz-1);
        lp = lp + 0.5*h0;

        h0 = op; %06
        h0(1:Nx-1,1:Ny,2:Nz)=op(2:Nx,1:Ny,1:Nz-1);
        lp = lp +0.5*h0;

        h0 = op; %08
        h0(1:Nx,1:Ny-1,2:Nz)=op(1:Nx,2:Ny,1:Nz-1);
        lp = lp +0.5*h0;

        h0 = op; %10
        h0(2:Nx,2:Ny,1:Nz)=op(1:Nx-1,1:Ny-1,1:Nz);
        lp = lp +0.5*h0;

        h0 = op; %12
        h0(1:Nx-1,1:Ny-1,1:Nz)=op(2:Nx,2:Ny,1:Nz);
        lp = lp +0.5*h0;

        h0 = op; %16
        h0(2:Nx,1:Ny-1,1:Nz)=op(1:Nx-1,2:Ny,1:Nz);
        lp = lp +0.5*h0;

        h0 = op; %18
        h0(1:Nx-1,2:Ny,1:Nz) = op(2:Nx,1:Ny-1,1:Nz);
        lp = lp +0.5*h0;

        h0 = op; %20
        h0(1:Nx,2:Ny,1:Nz-1)=op(1:Nx,1:Ny-1,2:Nz);
        lp = lp +0.5*h0;

        h0 = op; %22
        h0(2:Nx,1:Ny,1:Nz-1)=op(1:Nx-1,1:Ny,2:Nz);
        lp = lp +0.5*h0;

        h0 = op; %24
        h0(1:Nx-1,1:Ny,1:Nz-1)=op(2:Nx,1:Ny,2:Nz);
        lp = lp +0.5*h0;

        h0 = op; %26
        h0(1:Nx,1:Ny-1,1:Nz-1)=op(1:Nx,2:Ny,2:Nz);
        lp = lp +0.5*h0;

        %8 Corners
        h0 = op; %01
        h0(2:Nx,2:Ny,2:Nz) = op(1:Nx-1,1:Ny-1,1:Nz-1);
        lp = lp + 1/3*h0;

        h0 = op; %03
        h0(1:Nx-1,2:Ny,2:Nz) = op(2:Nx,1:Ny-1,1:Nz-1);
        lp = lp + 1/3*h0;

        h0 = op; %07
        h0(2:Nx,1:Ny-1,2:Nz) = op(1:Nx-1,2:Ny,1:Nz-1);
        lp = lp + 1/3*h0;

        h0 = op; %09
        h0(1:Nx-1,1:Ny-1,2:Nz) = op(2:Nx,2:Ny,1:Nz-1);
        lp = lp + 1/3*h0;

        h0 = op; %19
        h0(2:Nx,2:Ny,1:Nz-1) = op(1:Nx-1,1:Ny-1,2:Nz);
        lp = lp + 1/3*h0;

        h0 = op; %21
        h0(1:Nx-1,2:Ny,1:Nz-1) = op(2:Nx,1:Ny-1,2:Nz);
        lp = lp + 1/3*h0;

        h0 = op; %25
        h0(2:Nx,1:Ny-1,1:Nz-1) = op(1:Nx-1,2:Ny,2:Nz);
        lp = lp + 1/3*h0;

        h0 = op; %27
        h0(1:Nx-1,1:Ny-1,1:Nz-1) = op(2:Nx,2:Ny,2:Nz);
        lp = lp + 1/3*h0;

        lapop = single((3/13/dx/dx)*(lp - 44/3*op));                               %formula for 27-points stencil
    end

    lapop = single((lp-6*op)/(dx*dx));                                        %formula for 7-points stencil
end

% A slightly improved method, deleting the trivial if and fixing all the
% find() stuff
function[lapop] = laplace_improved(op,dx)
    %commend out line 35,119,121 will give u 27-point stencil
    %6 Surfaces
    [Nx, Ny, Nz] = size(op);
    structs = isnan(op);
    
    h0 = op;  %05
    h0(1:Nx,1:Ny,2:Nz)=op(1:Nx,1:Ny,1:Nz-1);
    h0(structs) = op(structs);

    h00 = op; %11
    h00(1:Nx,2:Ny,1:Nz)=op(1:Nx,1:Ny-1,1:Nz);
    h00(isnan(h00)) = op(isnan(h00));
    lp=h0+h00; clear h00;

    h0 = op; %13
    h0(2:Nx,1:Ny,1:Nz)=op(1:Nx-1,1:Ny,1:Nz);
    h0(structs) = op(structs);
    lp = lp+h0;

    h0 = op; %15
    h0(1:Nx-1,1:Ny,1:Nz)=op(2:Nx,1:Ny,1:Nz);
    h0(structs) = op(structs);
    lp = lp+h0;

    h0 = op; %17
    h0(1:Nx,1:Ny-1,1:Nz)=op(1:Nx,2:Ny,1:Nz);
    h0(structs) = op(structs);
    lp = lp+h0;

    h0 = op; %23
    h0(1:Nx,1:Ny,1:Nz-1)=op(1:Nx,1:Ny,2:Nz);
    h0(structs) = op(structs);
    lp = lp+h0;

    lapop = single((lp-6*op)/(dx*dx));                                        %formula for 7-points stencil
end