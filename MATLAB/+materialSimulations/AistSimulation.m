classdef AistSimulation < phasefieldMethod.TestSimulation & ...
                          simulationModels.AllPhysicalSimulation & ...
                          simulationModels.ThompsonSpaepen & ...
                          simulationModels.CombinedViscosity_1 & ...
                          simulationModels.WilsonFrenkelVelocity & ...
                          simulationModels.ClassicalNucleation
    %AISTSIMULATION The simulation model for AIST
    % TODO
    
    properties (SetAccess = protected)
        % Free energy
        Lv = 878.9e6;    % Latent heat [J/m^3]
        L  = 173e-3 * simulationModels.Constants.eV
        Tm = 817;        % Melting temperature [K]
        
        % Viscosity
        Tglass = 700; %TODO
        ninf  = 1.22e-3; % Viscosity for T -> Inf [Pa*s]
        Tg    = 445;     % Glass transition temperature [K]
        m     = 128;     % Fragility
        Ea    = 2.90 * simulationModels.Constants.eV;    % Activation energy [eV]
        
        % Velocity
        jd = 1e-10;
        r_atom = 1.5e-10;
        R_hyd = 0.5e-10;
        
        % Nucleation
        IE = 5.5e-2;     % Interfacial energy [J/m^2]
        Vm;
    end
    
    properties (SetAccess = protected)
        DIT;  % diffuse interface thickness [m]
    end
    
    methods
        function S = AistSimulation(spacing, seed)
            S@phasefieldMethod.TestSimulation(spacing, seed);
            S@simulationModels.AllPhysicalSimulation(spacing, seed);
            S@simulationModels.WilsonFrenkelVelocity(spacing, seed);
            S@simulationModels.ThompsonSpaepen(spacing, seed);
            S@simulationModels.CombinedViscosity_1(spacing, seed);
            S.Vm = S.L / S.Lv;
        end
        
        function S = setParameters(S, DIT, T)
            S.DIT = DIT;
            S.T = T;
        end
        
        function S = apply(S)
            S.J = S.nucleationRate();
            S = apply@simulationModels.AllPhysicalSimulation(S);
        end
    end
    
end
