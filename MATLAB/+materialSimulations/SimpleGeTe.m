classdef SimpleGeTe < phasefieldMethod.TestSimulation & ...
                      simulationModels.ScaleableSimulation & ...
                      simulationModels.FrenkelSimplifiedVelocity & ...
                      simulationModels.ThompsonSpaepen & ...
                      simulationModels.CombinedViscosity11 & ...
                      simulationModels.ArrheniusNucleation
    %GETESIMULATION A simulation model for GeTe. Temperature dependent
    % Arrhenius behaviour for growth velocity and nucleation rate and a
    % numerically set critical radius.
    % The growth velocity of 5.9e14 m/s * exp(-1.77eV/kB/T) is taken from
    % Q. M. Lu and M. Libera, the nucleation rate
    % of 1.0789e64 m^-3*s^-2 * exp(-4.3eV/kB/T) is from A. M. Mio et al
    % (who also give other values for v).
    
    properties (SetAccess = protected)
        DIT = 0;           % interface thickness [m]
        r_c = 0;
    end 
    
    properties (SetAccess = protected)
        % For viscosity
        ninf = 10^(-3.2);
        Tg = 432.1;
        m = 130.7
        Tglass = 454
        Ea = 1.85 * simulationModels.Constants.eV;
        
        % For free energy
        Lv = 17.9e3 / 33.11e-6
        L = 17.9e3 / 33.11e-6 * 55e-30
        Tm = 1000;
        
        % For velocity
        Atilde = 10^(-2);
        
        % For nucleation
        J_Ea = 4.3 * simulationModels.Constants.eV;
        J00 = exp(103);
    end
    
    methods
        function S = SimpleGeTe(spacing, seed)
            S@phasefieldMethod.TestSimulation(spacing, seed);
            S@simulationModels.ScaleableSimulation(spacing, seed);
            S@simulationModels.ThompsonSpaepen(spacing, seed);
            S@simulationModels.CombinedViscosity11(spacing, seed);
            S@simulationModels.FrenkelSimplifiedVelocity(spacing, seed);
            S@simulationModels.ArrheniusNucleation();
        end
        
        function S = setParameters(S, DIT, r_c)
            %SETPARAMETERS(DIT, r_c) - Set the numerical parameters
            % Arguments:
            %   DIT - diffuse interface thickness [m]
            %   r_c - critical radius [m]
            % Returns:
            %   S - The updated object
            
            assert(isscalar(DIT) && isnumeric(DIT), 'DIT has wrong type or size');
            assert(isscalar(r_c) && isnumeric(r_c), 'r_c has wrong type or size');
            S.DIT = single( DIT );
            S.r_c = single( r_c );
        end
        
        function S = apply(S)
            S.J = S.nucleationRate();
            S = apply@simulationModels.ScaleableSimulation(S);
        end
        
        function exportParameters(S, folder, index)
            %EXPORTPARAMETERS(folder, index) - Export the current
            %                                  parameters.
            % In this class, the temperature field is exported.
            % Arguments:
            %   folder - Name of the folder to save in. Must already exist.
            %   index - An index to this export. Will appear in the
            %      filename.
            % Returns nothing.
            
            filename = fullfile(folder,sprintf('temperature_%d.vtk',index));
            phasefieldMethod.mat2vtk(S.T, filename,  S.spacing, ...
                                        [0,0,0], 'temperature');
        end
        
        function backupParams(S, file)
            S.backupParamsCore(file);
            file.DIT = S.DIT;
            file.T = S.T;
            file.vinf = S.vinf;
            file.Ea_v = S.Ea_v;
            file.Jinf = S.Jinf;
            file.Ea_J = S.Ea_J;
        end
    end
    
    methods( Static )
        function S = fromBackup(paramsfile, statefile)
            S = phasefieldMethod.Simulation.fromBackupCore( ...
                                @phasefieldMethod.SimpleSimulation, ...
                                paramsfile, statefile);
            
            S.DIT = paramsfile.DIT;
            S.T = paramsfile.T;
            S.vinf = paramsfile.vinf;
            S.Ea_v = paramsfile.Ea_v;
            S.Jinf = paramsfile.Jinf;
            S.Ea_J = paramsfile.Ea_J;
            S = S.apply();
        end
    end
end