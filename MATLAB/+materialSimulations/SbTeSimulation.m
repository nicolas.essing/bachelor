classdef SbTeSimulation < phasefieldMethod.TestSimulation & ...
                         simulationModels.AllPhysicalSimulation & ...
                         simulationModels.ThompsonSpaepen & ...
                         simulationModels.ArrheniusVelocity & ...
                         simulationModels.ArrheniusViscosity & ...
                         simulationModels.ClassicalNucleation
    %SBTESIMULATION The simulation model for Sb2Te
    % TODO
    
    properties (SetAccess = protected)
        % For free energy
        Tm   = 837.5;             % Melting temperature [K]
        Lv   = 296.4e6;           % Latent heat [J/m^3]
        L;
        
        % For velocity
        v00  = 1.89885855985e+15; % Growth velocity for T -> Inf [m/s]
        Ea   = 2.91633477015e-19; % Activation energy [J]
        
        % For nucleation
        IE = 7.29e-2;
        n00 = 1.2e-99;
        jd = 1;
        Vm = 4*pi/3;
    end
    
    properties (SetAccess = protected)
        DIT;  % diffuse interface thickness [m]
    end
    
    methods
        function S = SbTeSimulation(spacing, seed)
            S@phasefieldMethod.TestSimulation(spacing, seed);
            S@simulationModels.AllPhysicalSimulation(spacing, seed);
            S@simulationModels.ThompsonSpaepen(spacing, seed);
            S@simulationModels.ArrheniusVelocity(spacing, seed);
            S@simulationModels.ArrheniusViscosity(spacing, seed);
            S@simulationModels.ClassicalNucleation();
        end
        
        function S = setParameters(S, DIT, T)
            S.DIT = DIT;
            S.T = T;
        end
        
        function S = apply(S)
            S.J = S.nucleationRate();
            S = apply@simulationModels.AllPhysicalSimulation(S);
        end
        
    end
    
end
