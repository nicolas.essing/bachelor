classdef GSTSimulation < phasefieldMethod.TestSimulation & ...
                         simulationModels.AllPhysicalSimulation & ...
                         simulationModels.ThompsonSpaepen & ...
                         simulationModels.CombinedViscosity11 & ...
                         simulationModels.CombinedVelocity & ...
                         simulationModels.HeterogeneousNucleation
    %GSTSIMULATION A simulation for the model Zhi Yang used for GST.
    
    properties (SetAccess = protected)
        DIT;                 % diffuse interface thickness [m]
        structf;             % structure factor for nucleation
    end
    
    properties (SetAccess = protected)
        IE = 0.06;           % interfacial energy [J/m^2]
        Lv = 625e6;          % latent heat [J/m^3]
        L = 625e6 * 2.9e-28; % latent heat per atom [J]
        Vm = 2.9e-28;        % volume per atom [m^3]
        Tm = 889;            % melting temperature [K]
        ninf = 0.012;        % viscosity for infinite temperature [Pa*s]
        m = 140;             % fragility
        Tg = 472;            % other glass transition temperature [K]
        jd = 2.99e-10;       % jump distance [m]
        Ea = 2.3 * 1.6e-19;  % activation energy [J]
        Tglass = 534;        % glass transition temperature [K]
        r_atom = 1;
        R_hyd = 1;
        wa = 180;            % wetting ange [degrees]
    end
    
    methods
        function S = GSTSimulation(spacing, seed)
            S@phasefieldMethod.TestSimulation(spacing, seed);
            S@simulationModels.AllPhysicalSimulation(spacing, seed);
            S@simulationModels.CombinedVelocity(spacing, seed);
            S@simulationModels.ThompsonSpaepen(spacing, seed);
            S@simulationModels.CombinedViscosity11(spacing, seed);
        end
        
        function S = setParameters(S, DIT, T)
            S = S.assertForm(DIT, 'interface thickness');
            S = S.assertForm(T, 'temperature');
            S.DIT = DIT;
            S.T = T;
        end
        
        function S = apply(S)
            neighs = phasefieldMethod.analyse_structure_27(S.phasefield);
            s = (2+cosd(S.wa)) * (1-cosd(S.wa)) * (1-cosd(S.wa)) /4;
            S.structf = (neighs ==  0) * 1 + ...
                        and((neighs >= 1), (neighs <  7)) * s*(1.2^2) + ...
                        and((neighs >= 7), (neighs < 26)) * s*1.2 + ...
                        (neighs >= 26) * s;
            S.J = S.nucleationRate();
            S = apply@simulationModels.AllPhysicalSimulation(S);
        end
        
        function s = structfactor(S)
            s = S.structf;
        end
        
        function backupParams(S, file)
            % Write the settings and evolution parameters to a .mat file.
            % Arguments:
            %   file - Handle of a .mat file
            % Returns nothing
            
            S.backupParamsCore(file);
            file.IE   = S.IE;
            file.DIT  = S.DIT;
            file.L    = S.L;
            file.Tm   = S.Tm;
            file.T    = S.T;
%             file.Vm   = S.Vm;
            file.ninf = S.ninf;
            file.m    = S.m;
            file.Tg   = S.Tg;
            file.jd   = S.jd;
            file.Ea   = S.Ea;
            file.Tglass = S.Tglass;
            file.wa   = S.wa;
        end
        
    end
    
    methods (Static)
        function S = fromBackup(paramsfile, statefile)
            S = phasefieldMethod.Simulation.fromBackupCore( ...
                       @phasefieldMethod.GSTSimulation, paramsfile, statefile);
            S.IE   = paramsfile.IE;
            S.DIT  = paramsfile.DIT;
            S.L    = paramsfile.L;
            S.Tm   = paramsfile.Tm;
            S.T    = paramsfile.T;
%             S.Vm   = paramsfile.Vm;
            S.ninf = paramsfile.ninf;
            S.m    = paramsfile.m;
            S.Tg   = paramsfile.Tg;
            S.jd   = paramsfile.jd;
            S.Ea   = paramsfile.Ea;
            S.Tglass = paramsfile.Tglass;
            S.wa   = paramsfile.wa;
            S = S.apply();
        end
    end
end
