classdef ScaleableSbTe < phasefieldMethod.TestSimulation & ...
                          simulationModels.ScaleableSimulation & ...
                          simulationModels.ThompsonSpaepen & ...
                          simulationModels.ArrheniusVelocity & ...
                          simulationModels.ArrheniusViscosity & ...
                          simulationModels.ClassicalNucleation
    %SBTESIMULATION The simulation model for Sb2Te
    % TODO
    
    properties (SetAccess = protected)
        % For free energy
        Tm   = 837.5;             % Melting temperature [K]
        Lv   = 296.4e6;           % Latent heat [J/m^3]
        L;
        
        % For velocity
        v00  = 1.89885855985e+15; % Growth velocity for T -> Inf [m/s]
        Ea   = 2.91633477015e-19; % Activation energy [J]
        
        % For nucleation
        IE = 7.29e-2;
        n00 = 1.2e-99;
        jd = 1;
        Vm = 4*pi/3;
    end
    
    properties (SetAccess = protected)
        DIT;  % diffuse interface thickness [m]
        r_c;  % critical radius [m]
    end
    
    properties (SetAccess = private, GetAccess = private)
        printNucleiSizes;
    end
    
    methods
        function S = ScaleableSbTe(spacing, seed, printNucleiSizes)
            %SBTESIMULATION(spacing, seed, printNucleiSizes) - Create a
            %                                         simulation for Sb2Te.
            % Before simulating, you need to pass parameters, define a time
            % step and set an initial phasefield.
            % Arguments:
            %   spacing - Distance between lattice points in meters
            %   seed - Seed for random number generation
            %   printNucleiSizes - How many nuclei sizes are measured and
            %       printed into the infoLine
            % Returns:
            %   S - An instance of this class
            
            S@phasefieldMethod.TestSimulation(spacing, seed);
            S@simulationModels.ScaleableSimulation(spacing, seed);
            S@simulationModels.ThompsonSpaepen(spacing, seed);
            S@simulationModels.ArrheniusVelocity(spacing, seed);
            S@simulationModels.ArrheniusViscosity(spacing, seed);
            S@simulationModels.ClassicalNucleation();
            S.printNucleiSizes = printNucleiSizes;
        end
        
        function S = setParameters(S, DIT, r_c, T)
            S.DIT = DIT;
            S.r_c = r_c;
            S.T = T;
        end
        
        function S = apply(S)
            S.J = S.nucleationRate();
            S = apply@simulationModels.ScaleableSimulation(S);
        end
        
        function l = infoLine(S)
            %INFOLINE() - Print a csv style info line about the current state.
            % Returns:
            %   l - An info line
            
            l = sprintf('%d, %d, %d', S.time, S.graincount, ...
                                        S.transformedFraction);
            for i=1:S.printNucleiSizes
                l = strcat(l,sprintf(', %d', S.nucleusArea(i)));
            end
            l = strcat(l,'\n');
        end
        
        function l = infoLineHeader(S)
            %INFOLINEHEADER() - Print the column titles for the csv style 
            %                   info from method
            % Returns:
            %   l - Column titles
            %
            % If infoLine() is overwritten in a subclass, this method must
            % be overwritten too to provide right information.
            
            l = 'time [s], graincount, transformedFraction';
            for i=1:S.printNucleiSizes
                l = strcat(l, sprintf(', A%d',i));
            end
            l = strcat(l, '\n');
        end
        
    end
    
end
