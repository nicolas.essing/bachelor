% I want to test how the simulation produces the analytical results, here
% the interface form. In theory, the eqilibrium form should be a tanh, and
% a form like this should move form-invariant.
% I test this by compareing a tanh-interface that moved to some point
% with a sharp interface relaxed a this point and the result from theory.
% The experimental setup is just a planar interface, the x- and
% y-dimensions are trivial.

% % GST parameters
% spacing = 1e-9;
% DIT = 3.5e-9;
% v = 6.9914e-11;
% tau = 1.2444e+03;

% Normalised parameters
spacing = 1;
DIT = 3;
r_c = 2;
v = 1;

L = 100;
x0 = 30;
xf = 70.5;

% Build a simulation
import phasefieldMethod.ExampleSimulation
S = ExampleSimulation(spacing, 42);
S = S.setParameters(DIT, v, r_c);

S = S.adjustTimestep(3);
S = S.apply();

% Initial setups:
% First: moving smooth interface
X = 1:L;
phasefield = 1/2*(1+tanh((x0-X)*spacing/2./DIT));
S1 = S.setField(phasefield);
neededtime = (xf-x0)*spacing/v;

% Second: the same interface shifted to where the first one will end.
field = 1/2*(1+tanh((xf-X)*S.spacing/2./DIT));
S2 = S.setField(field, S.grainindices);

% Third: We place a sharp interface centered at where the first will end
% and evovle it without a driving force
S3 = S.setOtherParameters(DIT, 0, S.relaxationTime);
S3 = S3.apply();
S3 = S3.setField( (1. * (X<xf)) );


% Plot initial situation
hold on;
plot(S1.phasefield);
plot(S2.phasefield);
plot(S3.phasefield);
legend('will move','theory','will relax');
input('Close the plot and press enter to continue');

% Time evolution
S1 = S1.evolve(neededtime);
S3 = S3.evolve(2*S.relaxationTime);

hold on
plot(S1.phasefield);
plot(S2.phasefield);
plot(S3.phasefield);
legend('mooved there','theory','relaxed');
