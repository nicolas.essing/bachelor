% Testing my calculations on tau. Here I show an nonequilibrium interface
% and its form after evolution of a few times relative to tau.

% Normalised parameters
spacing = 1;
DIT = 3;
tau = 1;
evolvetaus = [0.3, 1.2, 3];

% Initial interface form, use a steep or a wider interface compared to the
% eqilibrium shape.
X = 1:100;
% initialfield = 1. * (X<50);
initialfield = 1/2*(1+tanh((50-X)./(3*2*DIT)));

% Build a simulation using these parameters
import phasefieldMethod.ExampleSimulation
S = ExampleSimulation(spacing, 42);
S = S.setOtherParameters(DIT, 0, tau);
S = S.adjustTimestep(2);
S = S.apply();
S = S.setField(initialfield);

% Calculate the eqilibrium shape
finalfield = 1/2*(1+tanh((50-X)./(2*DIT)));

% For each given time, evolve the simulation to there and save a snapshot
dts = diff([0,evolvetaus]) .* tau;
fields = cell(1,length(dts));
for i=1:length(dts)
    dt = dts(i);
    S = S.evolve(dt);
    fields{i} = S.phasefield;
end

% Show the snapshots and the eqilibrium shape
hold on;
plot(initialfield,'LineWidth',1);
for i=1:length(fields)
    plot(fields{i},'LineWidth',1);
end
plot(finalfield,'--','LineWidth',1);
xlabel('x','FontSize',16);
ylabel('$\Phi$','Interpreter','LaTeX','FontSize',16);
set(gca,'FontSize',16);
legend([mat2cell(num2str([0,evolvetaus]'),ones(1,length(evolvetaus)+1)); ...
        {'equilibrium'}] ...
       ,'FontSize',16);
