% Testing the simulation for GeTe parameters

% Clean up
clear;
gpuDevice(1);

spacing = 800e-9;
DIT = 2*spacing;
r_c = 3*spacing;
field = zeros(400,400,50,'single','gpuArray');
T = 400;

saveEvery = 0;
printEvery = 50;
exportEvery = 200;

import phasefieldMethod.*;

% Build a Simulation object. Seed is 42.
S = GeTeSimulation(spacing, 42);
S = S.setField(field);
S = S.setParameters(DIT, r_c);
S = S.setTemp(T);
S = S.apply();

% Choose a good time step
S = S.adjustTimestep(2);
S = S.apply();

% Choose good timesteps for grain identification and nucleation.
S = S.autoChooseGrainTimestep(1.5);
S = S.autoChooseNucleationTimestep(1);
S = S.apply();

% Tell me how it looks
S.analyse();
% Enter will start simulating, if anything else is entered it is aborted
assert(isempty(input('Enter everything thats wrong: ','s')),'manual stop');

% Build a Run object, save some information
R = Run.createFromSimulation(S, '../temp/test_1');
R = R.exportSettings(1,1,1);
R.info = ['Simulation of GeTe. Growth velocity and nucleation rate ' ...
          'taken from some papers. Critical radius is used as a ' ...
          'numerical parameter'];
R = R.saveAnalysis();
R = R.printLine();

% Put in the save settings from above.
R = R.setSaveEvery(saveEvery, printEvery, exportEvery);

% Simulate
R = R.evolveToFraction(0.99);

% Save the Run object
R = R.backupSelf();
