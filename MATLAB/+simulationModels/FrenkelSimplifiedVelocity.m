classdef FrenkelSimplifiedVelocity < simulationModels.PhysicalSimulation
    %WILSONFRENKELVELOCITY - A simulation using a velocity from the Wilson
    % and Frenkel model.
    % 
    % Properties that need to be defined by subclasses:
    %   Atilde
    %
    % Methods that need to be provided by subclasses:
    %   freeEnergy
    %   viscosity
    %
    % Methods provided:
    %   velcocity
    
    properties (Abstract, SetAccess = protected)
        Atilde
    end
    
    methods (Abstract)
        freeEnergy(S)
        viscosity(S)
    end
    
    methods
        function S = FrenkelSimplifiedVelocity(spacing, seed)
            S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function v = velocity(S)
            kB = simulationModels.Constants.kB;
            v = S.Atilde ./  S.viscosity() .* ...
                (1 - exp(- S.freeEnergy() ./ (kB .* S.T)) );
        end
    end
end
