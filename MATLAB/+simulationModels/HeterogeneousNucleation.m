classdef HeterogeneousNucleation
    %CLASSICALNUCLEATION - A simulation using classical nucleation theory
    % for the nucleation rate, but includes heterogeneous effects.
    %
    % Properties that need to be defined by subclasses:
    %   jd
    %   IE
    %
    % Methods that need to be provided by subclasses:
    %   volumefreeEnergy
    %   viscosity
    %   structfactor
    %
    % Methods provided:
    %   nucleationRate
    %   apply
    
    properties (Abstract, SetAccess = protected)
        jd;
        IE;
        Vm;
    end
    
    methods (Abstract)
        volumeFreeEnergy(S)
        viscosity(S)
        structfactor(S)
    end
    
    methods
        function J = nucleationRate(S)
            kB = simulationModels.Constants.kB;
            dGv = S.volumeFreeEnergy();
            sf = S.structfactor();
            nc = 32 * pi ./ (3 .* S.Vm) .* (S.IE ./ dGv).^3;
            disp(nc);
            % It is important that the exp() term is in the middle, as the
            % other factors get quite large.
            J = 4 .* nc.^(2/3) .* ...
                dGv.^2  .* sqrt(kB * S.T ./ S.IE.^3) .* ...
                exp( -16 * pi * S.IE.^3 ./ (3 * dGv.^2 .* kB .* S.T) .* sf) ...
                ./ (24 * pi^2 * S.jd.^3 .* S.viscosity());
        end
    end
end
