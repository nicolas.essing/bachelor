classdef WilsonFrenkelVelocity < simulationModels.PhysicalSimulation
    %WILSONFRENKELVELOCITY - A simulation using a velocity from the Wilson
    % and Frenkel model.
    % 
    % Properties that need to be defined by subclasses:
    %   jd
    %   r_atom
    %   R_hyd
    %
    % Methods that need to be provided by subclasses:
    %   freeEnergy
    %   viscosity
    %
    % Methods provided:
    %   velcocity
    
    properties (Abstract, SetAccess = protected)
        jd;     % Jump distance [m]
        r_atom;
        R_hyd;
    end
    
    methods (Abstract)
        freeEnergy(S)
        viscosity(S)
    end
    
    methods
        function S = WilsonFrenkelVelocity(spacing, seed)
            S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function v = velocity(S)
            kB = simulationModels.Constants.kB;
            v = 4 .* kB .* S.T ./ ...
                (3 .* pi .* S.jd.^2 .* S.R_hyd .* S.viscosity() ) .* ...
                (1 - exp(- S.freeEnergy() ./ (kB .* S.T)) );
        end
    end
end
