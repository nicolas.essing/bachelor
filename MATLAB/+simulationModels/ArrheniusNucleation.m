classdef ArrheniusNucleation
    %ARRHENIUSNUCLEATION A arrhenius approximation for the nucleation rate.
    
    properties (Abstract, SetAccess = protected)
        J_Ea;
        J00;
    end
    
    methods
        function J = nucleationRate(S)
            kB = simulationModels.Constants.kB;
            J = S.J00 .* exp( - S.J_Ea ./ kB ./ S.T );
        end
    end
end
