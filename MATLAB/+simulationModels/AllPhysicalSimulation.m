classdef AllPhysicalSimulation < simulationModels.PhysicalSimulation
    %ALLPHYSICALSIMULATION - A simulation using only physically reasonable
    % values.
    %
    % Properties that need to be defined by subclasses:
    %   DIT
    %   IE
    %
    % Methods that need to be provided by subclasses:
    %   volumefreeEnergy
    %   velocity
    %   nucleationRate
    
    properties (Abstract, SetAccess = protected)
        DIT;
        IE;
    end
    
    methods
        function S = AllPhysicalSimulation(spacing, seed)
            S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function S = apply(S)
            S.eps = sqrt(6 .* S.IE .* S.DIT);
            S.W = 3 .* S.IE ./ S.DIT;
            S.dGv = S.volumeFreeEnergy();
            S.M = S.velocity() ./ (6 * S.DIT .* S.dGv);
            S = apply@simulationModels.PhysicalSimulation(S);
        end
    end
end
