classdef MyegaViscosity_1 < simulationModels.PhysicalSimulation
    %MYEGAVISCOSITY - A simulation using the MYEGA model for the viscosity.
    %
    % Properties that need to be defined by subclasses:
    %   ninf - Viscosity for T -> inf [Pa*s]
    %   Tg - Reference temperature in MYEGA equation [K]
    %   m - Fragility [1]
    %
    % Methods provided:
    %   viscosity
    
    properties (Abstract, SetAccess = protected)
        ninf;        % Viscosity for T -> inf [Pa*s]
        Tg;          % Reference temperature in MYEGA equation [K]
        m;           % Fragility [1]
    end
    
    methods
        function S = MyegaViscosity_1(spacing, seed)
            S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function n = viscosity(S)
            n = S.temperaureViscosity(S.T);
        end
    end
    
    methods (Access = protected)
        function n = temperaureViscosity(S, T)
            n = 10.^(log10(S.ninf) + (12-log10(S.ninf)) ...
                 *(S.Tg./T) .* exp(S.m/(12-log10(S.ninf)) ...
                  *(S.Tg./T -1)));
        end
    end
end
