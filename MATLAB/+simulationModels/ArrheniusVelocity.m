classdef ArrheniusVelocity < simulationModels.PhysicalSimulation
    %ARRHENIUSVELOCITY - A simulation with an arrhenius model for the
    % growth velocity.
    %
    % Properties that need to be defined by subclasses:
    %   v00 - velocity for T -> inf [m/s]
    %   Ea - Activation energy [J]
    %
    % Methods provided:
    %   velocity
    
    properties (Abstract, SetAccess = protected)
        v00; % velocity for T -> inf [m/s]
        Ea;  % Activation energy [J]
    end
    
    methods
        function S = ArrheniusVelocity(spacing, seed)
            S = S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function v = velocity(S)
            kB = simulationModels.Constants.kB;
            v  = S.v00 .* exp( - S.Ea ./ (kB * S.T));
        end
    end
end
