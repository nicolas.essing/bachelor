classdef ArrheniusViscosity < simulationModels.PhysicalSimulation
    %ARRHENIUSVISCOSITY - A simulation with an arrhenius model for the
    % viscosity.
    %
    % Properties that need to be defined by subclasses:
    %   n00 - Viscosity for T -> inf [Pa*s]
    %   Ea - Activation energy [J]
    %
    % Methods provided:
    %   viscosity
    
    properties (Abstract, SetAccess = protected)
        n00; % Viscosity for T -> inf [Pa*s]
        Ea;  % Activation energy [J]
    end
    
    methods
        function S = ArrheniusViscosity(spacing, seed)
            S = S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function n = viscosity(S)
            kB = simulationModels.Constants.kB;
            n  = S.n00 .* exp( S.Ea ./ (kB * S.T));
        end
    end
end
