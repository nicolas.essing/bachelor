classdef ClassicalNucleation < simulationModels.HeterogeneousNucleation
    %CLASSICALNUCLEATION - A simulation using classical nucleation theory
    % for the nucleation rate.
    % Uses the more general heterogeneous model with a structfactor set to
    % one.
    %
    % Properties that need to be defined by subclasses:
    %   jd
    %   IE
    %
    % Methods that need to be provided by subclasses:
    %   volumefreeEnergy
    %   viscosity
    %
    % Methods provided:
    %   structfactor
    %   nucleationRate
    %   apply
    
    methods (Abstract)
        volumeFreeEnergy(S)
        viscosity(S)
    end
    
    methods
        function s = structfactor(S)
            s = 1;
        end
    end
end
