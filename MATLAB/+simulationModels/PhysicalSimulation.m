classdef PhysicalSimulation < phasefieldMethod.Simulation
    %PHYSICALSIMULATION - The superclass for all physical models.
    % Defines a temperature.
    %
    % Properties:
    %   T - Temperature [K]. Can be a field or a scalar.
    %
    % Methods
    %   setTemp
    
    properties
        T;  % Temperature
    end
    
    methods
        function S = PhysicalSimulation(spacing, seed)
            S@phasefieldMethod.Simulation(spacing, seed);
        end
        
        function S = setTemp(S, T)
            S = S.assertForm(T, 'temperature');
            S.T = T;
        end
    end
end