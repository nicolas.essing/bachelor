classdef CombinedViscosity_1 < simulationModels.ArrheniusViscosity & ...
                               simulationModels.MyegaViscosity_1
    %COMBINEDVISCOSITY - A simulation with a combined model for the
    % viscosity, using the MYEGA model for temperatures above the glass
    % transition temperature Tglass and an Arrhenius model for lower
    % temperatures.
    %
    % Properties that need to be defined by subclasses:
    %   Tglass
    %   Ea
    %   ninf
    %   Tg
    %   m
    %
    % Methods provided:
    %   viscosity
    
    properties (Abstract, SetAccess = protected)
        Tglass; % Glass transition temperature
    end
    
    properties (SetAccess = protected)
        n00;
    end
    
    methods
        function S = CombinedViscosity_1(spacing, seed)
            S@simulationModels.ArrheniusViscosity(spacing, seed);
            S@simulationModels.MyegaViscosity_1(spacing, seed);
            % Make the transition between the models continuous
            kB = simulationModels.Constants.kB;
            S.n00 = S.temperaureViscosity(S.Tglass) / ...
                   exp( S.Ea ./ (kB * S.Tglass));
        end
        
        function n = viscosity(S)
            n  = viscosity@simulationModels.MyegaViscosity_1(S);
            n2 = viscosity@simulationModels.ArrheniusViscosity(S);
            n(S.T<S.Tglass) = n2(S.T<S.Tglass);
        end
    end
end
