classdef Constants
    % A class that defines some general used constants.
    
    properties (Constant)
        kB = 1.380649e-23;   % Boltzmann constant, [J/K]
        eV = 1.602176634e-19 % Electron volt [J]
        Na = 6.02214076e23   % Avogadro constant [1/mol]
        zeroC = 273.15       % Zero degrees celsius [K]
    end
end