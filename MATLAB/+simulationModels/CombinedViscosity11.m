classdef CombinedViscosity11 < simulationModels.ArrheniusViscosity & ...
                               simulationModels.MyegaViscosity11
    %COMBINEDVISCOSITY - A simulation with a combined model for the
    % viscosity, using the MYEGA model for temperatures above the glass
    % transition temperature Tglass and an Arrhenius model for lower
    % temperatures.
    %
    % Properties that need to be defined by subclasses:
    %   Tglass
    %   Ea
    %   ninf
    %   Tg
    %   m
    %
    % Methods provided:
    %   viscosity
    
    properties (Abstract, SetAccess = protected)
        Tglass; % Glass transition temperature
    end
    
    properties (SetAccess = protected)
        n00;
    end
    
    methods
        function S = CombinedViscosity11(spacing, seed)
            S@simulationModels.ArrheniusViscosity(spacing, seed);
            S@simulationModels.MyegaViscosity11(spacing, seed);
            % Make the transition between the models continuous
            kB = simulationModels.Constants.kB;
%             S.n00 = S.temperaureViscosity(S.Tglass) / ...
            S.n00 = (10^(log10(S.ninf)+(12-log10(S.ninf))*(S.Tg/S.Tglass)*...
                     exp((S.m/(12-log10(S.ninf))-1)*(S.Tg/S.Tglass-1)))) / ...
                    exp( S.Ea ./ (kB * S.Tglass));
        end
        
        function n = viscosity(S)
            n  = viscosity@simulationModels.MyegaViscosity11(S);
            n2 = viscosity@simulationModels.ArrheniusViscosity(S);
            n(S.T<S.Tglass) = n2(S.T<S.Tglass);
        end
    end
end
