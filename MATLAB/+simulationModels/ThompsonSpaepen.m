classdef ThompsonSpaepen < simulationModels.PhysicalSimulation
    %TSModel A Simulation that uses the Thomson and Saepen approximation
    % for the Gibbs free energy.
    %
    % Properties that need to be defined by subclasses:
    %   L - Latent heat per atom [J]
    %   Lv - Latent heat per volume [J/m^3]
    %   Tm - Melting temperature [K]
    %
    % Methods provided:
    %   freeEnergy
    %   volumeFreeEnergy
    
    properties (Abstract, SetAccess = protected)
        L;  % Latent heat per atom [J]
        Lv; % Latent heat per volume [J/m^3]
        Tm; % Melting temperature [K]
    end
    
    methods
        function S = ThompsonSpaepen(spacing, seed)
            S@simulationModels.PhysicalSimulation(spacing, seed);
        end
        
        function dG = freeEnergy(S)
            dG = S.L*(S.Tm - S.T)*2.*S.T/S.Tm./(S.Tm+S.T);
        end
        
        function dGv = volumeFreeEnergy(S)
            dGv = S.Lv*(S.Tm - S.T)*2.*S.T/S.Tm./(S.Tm+S.T);
        end
    end
end