classdef ScaleableSimulation < phasefieldMethod.Simulation
    %ALLPHYSICALSIMULATION - A simulation using the critical radius length
    % scale as a numerical parameter, which allows to use a larger spacing
    % and thus simulate larger samples.
    %
    % Properties that need to be defined by subclasses:
    %   DIT
    %   r_c
    %
    % Methods that need to be provided by subclasses:
    %   velocity
    
    methods
        function S = ScaleableSimulation(spacing, seed)
            S@phasefieldMethod.Simulation(spacing, seed);
        end
        
        
        function S = apply(S)
            v = S.velocity();
            S.M = 1;
            S.eps = sqrt(v .* S.r_c ./ 2);
            S.W = v .* S.r_c ./ (4 * S.DIT.^2);
            S.dGv = v ./ (6 * S.DIT);
            S = apply@phasefieldMethod.Simulation(S);
        end
    end
end
