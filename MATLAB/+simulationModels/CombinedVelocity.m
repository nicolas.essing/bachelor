classdef CombinedVelocity < simulationModels.ArrheniusVelocity & ...
                            simulationModels.WilsonFrenkelVelocity
    %COMBINEDVELOCITY - A simulation with a combined model for the groth
    % velocity.
    % 
    % Properties that need to be defined by subclasses:
    %   jd
    %   r_atom
    %   R_hyd
    %   Ea
    %
    % Methods that need to be provided by subclasses:
    %   freeEnergy
    %   viscosity
    %
    % Methods provided:
    %   velcocity
    
    properties (Abstract, SetAccess = protected)
        Tglass; % Glass transition temperature
    end
    
    properties (SetAccess = protected)
        v00;
    end
    
    methods
        function S = CombinedVelocity(spacing, seed)
            S@simulationModels.ArrheniusVelocity(spacing, seed);
            S@simulationModels.WilsonFrenkelVelocity(spacing, seed);
            % Make the transition between the models continuous
            kB = simulationModels.Constants.kB;
            Ttemp = S.T;
            S.T = S.Tglass;
            S.v00 = S.velocity() / exp( - S.Ea ./ (kB * S.Tglass));
            S.T = Ttemp;
        end
        
        function v = velocity(S)
            v  = velocity@simulationModels.WilsonFrenkelVelocity(S);
            v2 = velocity@simulationModels.ArrheniusVelocity(S);
            v(S.T<S.Tglass) = v2(S.T<S.Tglass);
        end
    end
end
