% I want to run a simulation with the physical parameters for Sb2Te. As I
% need a spatial resolution of approximately the critical radius, I can
% only simulate a small sample. Nucleation rate is that low that no two
% nuclei will appear, so simulate without it and place a nucleus.

import phasefieldMethod.*

spacing = .4e-9;

% Constants and parameters
kB = 1.380649e-23; % Boltzmann constant
IE = 5.5e-2; % from AIST paper
L = 878.9e6; % from AIST paper
Tm = 817; % from AIST paper
vinf = 4.55725664239e+14; % from fit
Ea = 21122.9264158 * kB; % from fit

T = 273.15 + 115; % 115 celsius

% Appropriate other parameters;
DIT = spacing/2; % should be ok

% create a simulation and set parameters
S = SimpleSimulation(spacing, 42);
S = S.setParameters(IE, DIT, vinf, Ea, L, Tm, 0);
S = S.setTemp(T);
S = S.apply();

% I can not simulate the whole area with a spacing this small, but the
% layer was about 30nm thick
field = zeros(800,800,75,'single','gpuArray');
positions = sub2ind(size(field),400,400,20);
field = plant_nuclei(field, 0, field, positions, S.dGv, S.W, S.eps, S.spacing);
S = S.setField(field);

S = S.apply();
S = S.adjustTimestep(10);
S = S.apply();

R = Run.createFromSimulation(S, '../data/sb2te_micro');
R = R.setSaveEvery(5,.5,1);
R = R.evolveToFraction(.9, 30);
R = R.backupSelf();