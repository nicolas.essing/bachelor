% I want to test the independce of the velocity from the diffuse interface
% thickness. In theory, the velocity should be invariant when multipling
% the thickness with a factor and dividing the mobility through it.
% This is implemented in the class ExampleSimulation, whith the possibility
% to keep different other parameters constant.
% It can be seen that it is important that the initial setup is already in
% the stable form; the relaxation takes time and leads to less movement.
% Also, it is important that the spacing is not too large compared to the
% critical radius.

% Scale with these values
scales = [0.5, 1, 2, 5];
% Use diffuse initial setting (true) or a sharp one (false)
initial_interface = true;
% Use constant tau (true) or r_c (false)
use_tau = true;

% Normalised parameters
spacing = 1;
DIT = 1;
v = 1;
tau = 3;
r_c = 1;

% Settings
goodness = 8;
length = 100;
startpos = 30;
time = 40;

import phasefieldMethod.ExampleSimulation

Ss = cell(size(scales));
for i=1:numel(scales)
    sc = scales(i);
    
    % Build a simulation
    S = ExampleSimulation(spacing, 42);
    if use_tau
        S = S.setOtherParameters(DIT * sc, v, tau);
    else
        S = S.setParameters(DIT * sc, v, r_c);
    end
    S = S.adjustTimestep(goodness);
    S = S.apply();
    
    % Initial setup: either the analytical solution or just a sharp
    % interface
    [X,Y,Z] = ndgrid(1:1,1:1,1:length);
    if initial_interface
        phasefield = 1/2*(1+tanh((startpos-Z)*S.spacing ...
                                               /2 ./S.interfaceThickness));
    else
        phasefield = 1. * (Z<startpos);
    end
    S = S.setField(phasefield);

    % Evolve and save
    [S, rt] = S.evolve(time);
    Ss{i} = S;
end

% Plot to comparison
hold on
labels = cell(size(Ss));
for i = 1:numel(Ss)
    S = Ss{i};
    %Plot
    plot(squeeze(S.phasefield(1,1,1:end)));
    labels{i} = num2str(scales(i));
end
exppos = v*time + startpos;
line([exppos,exppos],[0,1.1], 'Color','k');
line([exppos-1,exppos+1],[.5,.5], 'Color','k');
legend(labels);