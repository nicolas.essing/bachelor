% I want a clean run with clear defined parameters to test my fit routine.

% Arbitary spacing and stuff
spacing = 6.25e-8;
field = zeros(4000,4000,1,'single','gpuArray');
DIT = spacing;
r_c = spacing;

% With v=spacing/s, keep J 8e13 - 4e15
v = 6e-8;
J = 4e15;

% Build a simulation and put the parameters in
import phasefieldMethod.ExampleSimulation;
import phasefieldMethod.Run;
S = ExampleSimulation(spacing, 1234); % Used 42 and 1234
S = S.setField(field);
S = S.setParameters(DIT, v, r_c);
S = S.setNucleation(J);
S = S.adjustTimestep(2);
S = S.apply();
S = S.autoChooseGrainTimestep(1.2);
S = S.autoChooseNucleationTimestep(1);
S = S.apply();

% Show me what will happen
S.analyse();

% Run and save and stuff
R = Run.createFromSimulation(S,'../data/testana_8');
R = R.setSaveEvery(0,5,0);
R.info = 'A test run with well defined parameters (see analysis) to test my fitting routines';
R = R.saveAnalysis();
R = R.printLine();
R = R.evolveToFraction(0.99);
R = R.backupSelf();