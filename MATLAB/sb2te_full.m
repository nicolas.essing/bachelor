% I want to make a simulation of the whole experimental setup I have data
% from. DIT is considered a numerical parameter and set according
% to my resolution, r_c is taken a litte larger cause I thougt I saw a 
% Gibbs-Thomson effect. J and v are played around with to get good
% consensus with the experimental data.

% Clean up
clear;
gpuDevice(1);

% Enter temperature in C, values for J and v are choosen accordingly
T_C = 130;

% Observed area and fit results for this temperature
switch T_C
    case 115
        A = 6.503969659711159e-07;
        % Rate from 0 to 1.7e3 was zero, I do not simulate that
        updateJ = [9.7e3, 20e3] - 1.7e3;
        rates = [0.00786542155099889, 0.0044665614885977065, 0.013621316181970664];
    case 120
        A = 1.0470240115476765e-06;
        updateJ = [1e3, 9.5e3];
        rates = [0.0022214859480595814, 0.038678935992777747, 0.15255393465480316];
    case 125
        A = 1.3371373862923354e-06;
        updateJ = [2e3, 3.3e3];
        rates = [0.08031437744816548, 0.2173604801408437, 0.8392752530885558];
    case 130
        A = 1.0980560633760372e-06;
        updateJ = [250, 1100, 1900];
        rates = [0.08347581566196202, 0.4755922410719736, 2.364274911068733, 0.362598553648519];
    otherwise
        assert(false,'not a valid temperature');
end

% Choose how often to save: (print, export) tuple
switch T_C
    case 115
        exportSettings = [4*60, 0]; %20*60];
    case 120
         exportSettings = [2*60, 0]; %5*60];
    case 125
         exportSettings = [60, 0]; %2*60];
    case 130
         exportSettings = [20, 0]; %30];
end

import phasefieldMethod.*;

T = T_C + 273.15;

% A pixel was about 1.6 micrometers. The observed area was about 1mm x 1mm,
% exact value is A. Julian wrote in his mail that the thickness was about
% 30nm. As my simulation is only 2D, the height does not really matter.
% I choose a spacing that keeps the overall simulation size acceptable, and
% build a simulation for the experimental size.
spacing = 200e-9;
Nxy = round(sqrt(A)/spacing);
phasefield = zeros(Nxy,Nxy,1, 'single','gpuArray');

% Calculate J for the simulated volume as J = dN/dt / V
Js = rates ./ (Nxy^2 * spacing^3);

% Taking DIT to a value that should give good results numerically.
% Critical radius is a little large, but I think I kind of saw a
% Gibbs-Thomons effect in the data on this scale...
DIT = spacing;
r_c = 2e-6;

% Build a Simulation object. Seed is 42, track size of first 10 grains.
S = SbTeSimulation(spacing, 42, 10);
S = S.setField(phasefield);
S = S.setParameters(DIT, r_c, T);
S = S.manualNucleationRate(Js(1));
S = S.adjustTimestep(2);
S = S.apply();

% Choose good timesteps for grain identification and nucleation.
S = S.autoChooseGrainTimestep(1.5);
S = S.autoChooseNucleationTimestep(1);
S = S.apply();

% Tell me how it looks
S.analyse();

assert(isempty(input('Enter everything thats wrong: ','s')),'manual stop');

% The data was taken every 25 minutes for 115C, every 2 for 130C.
% Overall time was 9 hours for 115C, 40 minutes for 130C.
R = Run.createFromSimulation(S, sprintf('../data/sb2te_%d',T_C));
R = R.exportSettings(1,1,0);
R.info = ['Simulating Sb2Te with parameters from fits, using a few ' ...
          'different nucleation rates.'];
% Put in the save settings from above.
R = R.setSaveEvery(0, exportSettings(1), exportSettings(2));

% Evolve the time differences and as long as necessary afterwards
evolveTimes = [diff([0,updateJ]), Inf];

% For each different nucleation rate, save the analysis beforehand and then
% simulate the time
R = R.saveAnalysis();
R = R.printLine();
R = R.evolveToFraction(0.99, evolveTimes(1));
for i=2:length(Js)
    R.S = R.S.manualNucleationRate(Js(i));
    R = R.backupParams();
    R = R.backupState();
    R.saveAnalysis();
    R = R.evolveToFraction(0.99, evolveTimes(i));
end

R = R.backupSelf();