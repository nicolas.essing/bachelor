% I want to test the theory that I need a spatial resolution on the order
% of magnitude of the critical radius to not get cornered nuclei.
% I place an initial nucleus in the center of a 2D field (120x120px) and
% let it grow. Interface thickness and growth velocity are kept constant,
% the critical radius is scaled.

% Initial parameters: resolution, growth velocity, interface thickness and
% critical radius.
spacing = 1;
DIT = 2;
v = 1;
r_c = 1;

% Scale with these values
scales = [.01, .1, 1];
% Initial setting:
initial_setting = 'plant';

% Settings: Side lenght (in px) of the setup, name where to save
L = 120;
exportFolder = '../data/spatial_resolution_4';

import phasefieldMethod.*;

% for each scale, build a simulation
Ss = cell(size(scales));
for i=1:length(scales)
    sc = scales(i);
    S = ExampleSimulation(spacing,42);
    S = S.setParameters(DIT, v, sc*r_c);
    S = S.apply();
    
    switch initial_setting
        case '1d'
            % Use analytical solution for planar layers
            [X,Y] = ndgrid(1:L,1:L);
            R = sqrt((X-L/2).^2+(Y-L/2).^2);
            S = S.setField(1/2*(1+tanh((S.r_c-R)*spacing/(2*DIT))));
        case 'plant'
             % The algorithm used
            field = zeros(L,L,1,'single','gpuArray');
            pos = sub2ind(size(field),round(L/2),round(L/2));
            field = phasefieldMethod.plant_nuclei(field, 0, field, pos,...
                                        S.dGv, S.W, S.eps, S.spacing, 0.5);
        case 'square'
            field = zeros(L,L,1,'single','gpuArray');
            a = floor(L/20);
            field(9*a:11*a, 9*a:11*a) = 1;
    end
   
    S = S.setField(field);
    % For smaller scales I seem to need higher time resolution
    S = S.adjustTimestep(10);
    S = S.apply();
    Ss{i} = S;
end

%%

% For each simulation, make a Run object, simulate to an appropriate size
% and save
Rs = cell(size(scales));
for i = 1:length(scales)
    folder = sprintf('%s/%d', exportFolder, i);
    R = Run.createFromSimulation(Ss{i}, folder);
    R = R.setSaveEvery(0, L*spacing/v/100, 0);
    R.info = sprintf(['Simulations with different critical radii to ' ...
              'show the influence of this parameter.\n This things ' ...
              'done with initial setting "%s",\nbut this time with ' ...
              'smaller time steps for smaller r_c.'], initial_setting);
    R = R.saveAnalysis();
    R = R.export();
    R = R.evolveToFraction(.6);
    R = R.export();
    R = R.backupSelf();
    Rs{i} = R;
end