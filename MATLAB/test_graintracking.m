% In this file I have test code and performane tests for the grain tracking
% algorithm.
% Some of the testing code might need to be placed in the same file as the
% function definition.

% % Testing the stencil kernel:
% numers = uint32([0, 42, 973, bitcmp(uint32(0))]);
% fprintf('r, n: kernel(r,n,1)\n');
% for r=numers
%     for n=numers
%         % To display, change to double and convert nan
%         res = double([r, n, kernel(r, n, true)]);
%         res(res == bitcmp(uint32(0))) = nan;
%         fprintf('%d, %d: %d\n', res);
%     end
% end

% % Test if the function works properly:
% % Define some field of indices
% pm = [0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 1 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 0 3 0 0 0 0 0 0 0 0 0 0 0; ...
%       0 3 3 0 0 0 0 0 0 0 0 0 0 2; ...
%       0 0 0 0 0 0 0 0 0 0 0 0 2 2; ...
%       0 0 0 0 0 0 0 0 0 0 0 2 0 0];
% % Convert to uint32. userres is for interactive behaviour.
% m = uint32(pm);
% import phasefieldMethod.zero2neighbour;
% userres = [];
% while isempty(userres)
%     disp(pm);
%     m = zero2neighbour(m, m==0);
%     pm = double(m);
%     pm(pm==bitcmp(uint32(0))) = -1;
%     userres = input('Enter to continue (type anything to stop)\n', 's');
% end

% Speed comparison with random input
m = rand(1000,1000,100,'gpuArray');
m = round(m*1000);
m(m>100) = 0;
m2 = uint32(m);
t1 = gputimeit(@() test1(m));
fprintf('Zhi Yangs code: %f\n', t1);
t2 = gputimeit(@() test2(m2));
fprintf('Improved code: %f\n', t2);
fprintf('Thats %f%%\n', t2/t1*100);

function m = test1(m)
    % Zhiyangs method
    % (function maybefind is defined at the bottom, either find() or using
    % logical indexing.)
    opr1=m; opr2=m;
    [maxop1]=laplaceG1(m,1234);
    [maxop2]=laplaceG2(m,1234);
    opr1(maybefind(opr1==0)) = maxop1(maybefind(opr1==0));
    opr2(maybefind(opr2==0)) = maxop2(maybefind(opr2==0));
    opr1(maybefind(opr1 ~= opr2))=NaN;
    m=opr1;
end
    
function m = test2(m)
    % My code
    m = phasefieldMethod.zero2neighbour(m,m==0);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Zhi Yangs original method

function[maxop1] = laplaceG1(op,istep)

op(maybefind(op==0))=NaN;
op = op*(-1)^(istep);

[Nx, Ny, Nz] = size(op);

h0 = op;  %05
h0(1:Nx,1:Ny,2:Nz)=op(1:Nx,1:Ny,1:Nz-1);
maxop = h0;

h0 = op; %11
h0(1:Nx,2:Ny,1:Nz)=op(1:Nx,1:Ny-1,1:Nz);
maxop = max(maxop,h0);

h0 = op; %13
h0(2:Nx,1:Ny,1:Nz)=op(1:Nx-1,1:Ny,1:Nz);
maxop = max(maxop,h0);

h0 = op; %15
h0(1:Nx-1,1:Ny,1:Nz)=op(2:Nx,1:Ny,1:Nz);
maxop = max(maxop,h0);

h0 = op; %17
h0(1:Nx,1:Ny-1,1:Nz)=op(1:Nx,2:Ny,1:Nz);
maxop = max(maxop,h0);

h0 = op; %23
h0(1:Nx,1:Ny,1:Nz-1)=op(1:Nx,1:Ny,2:Nz);
maxop = max(maxop,h0);

maxop(maybefind(isnan(maxop)))=0;
maxop1=abs(maxop);
end

function[maxop2] = laplaceG2(op,istep)

op(maybefind(op==0))=NaN;
op = op*(-1)^(istep+1);

[Nx, Ny, Nz] = size(op);

%if(Nx<0)
h0 = op;  %05
h0(1:Nx,1:Ny,2:Nz)=op(1:Nx,1:Ny,1:Nz-1);
maxop = h0;

h0 = op; %11
h0(1:Nx,2:Ny,1:Nz)=op(1:Nx,1:Ny-1,1:Nz);
maxop = max(maxop,h0);

h0 = op; %13
h0(2:Nx,1:Ny,1:Nz)=op(1:Nx-1,1:Ny,1:Nz);
maxop = max(maxop,h0);

h0 = op; %15
h0(1:Nx-1,1:Ny,1:Nz)=op(2:Nx,1:Ny,1:Nz);
maxop = max(maxop,h0);

h0 = op; %17
h0(1:Nx,1:Ny-1,1:Nz)=op(1:Nx,2:Ny,1:Nz);
maxop = max(maxop,h0);

h0 = op; %23
h0(1:Nx,1:Ny,1:Nz-1)=op(1:Nx,1:Ny,2:Nz);
maxop = max(maxop,h0);

maxop(maybefind(isnan(maxop)))=0;
maxop2=abs(maxop);
end

% Set this function to find() to use Zhiyangs original code, set to res = x
% to consider trivial improvements
function res = maybefind(x)
    res = find(x);
%     res = x;
end