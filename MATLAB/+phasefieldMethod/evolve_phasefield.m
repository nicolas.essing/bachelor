function phasefield = evolve_phasefield(phasefield, Meps2dt_h2, MWdt, ...
                                       MdGvdt, structs, structneighbours)
%EVOLVE_PHASEFIELD(phasefield, Meps2dt_h2, MWdt, MdGvdt, structs,
%                  structneighbours) - Evolve the phase field.
%
% Returns the given phasefield evoled one timestep by predictor 
% corrector scheme.
%
% Arguments:
%   phasefield - The ordering parameter phasefield
%   Meps2dt_h2 - Mobility M times gradient energy coefficient eps
%       squared times time step dt divided by grid spacing h
%       squared.
%   MWdt - 6 times mobility M times depth of double-well W energy term
%       times timestep dt.
%   MdGvdt - 2 times mobility M times free energy difference per volume
%       dGv times time step.
%   structs - Logical array where structures are embedded
%   strucneighbours - Matrix of how many neighbouring points are
%       structures (see laplacian for details)
% Returns:
%   phasefield - The evolved phasefield.

    % explicit euler
    delta = phasefield_derivative(phasefield, Meps2dt_h2, MWdt, ...
                                  MdGvdt, structs, structneighbours);
    % predictor corrector
    delta2 = phasefield_derivative(phasefield + delta, Meps2dt_h2, MWdt, ...
                                   MdGvdt, structs, structneighbours);
    phasefield = phasefield + (delta + delta2) / 2;
end

function df = phasefield_derivative(phasefield, Meps2dt_h2, MWdt, MdGvdt, ...
                                    structs, structneighbours)
%PHASEFIELD_DERIVATIVE(phasefield, Meps2dt_h2, MWdt, MdGvdt, structs,
%                      structneighbours) - Derivative of phase field.
%
% The derivative of the phasefield with respect to time, multiplied
% with a time difference.
%
% Arguments:
%   phasefield - The ordering parameter phasefield
%   Meps2dt_h2 - Mobility M times gradient energy coefficient eps
%       squared times time step dt divided by grid spacing h squared.
%   MWdt - 6 times mobility M times depth of double-well W energy term
%       times timestep dt.
%   MdGvdt - 2 times mobility M times free energy difference per volume
%       dGv times time step.
%   structs - Logical array where structures are embedded
%   strucneighbours - Matrix of how many neighbouring points are
%       structures (see laplacian for details)
% Returns:
%   phasefield - The difference in the phasefield.
    
    df = Meps2dt_h2 .* phasefieldMethod.laplacian_7(phasefield, ...
                                           structs,structneighbours) ...
         + MdGvdt .* phasefield .* (1-phasefield) ...
         - MWdt .* phasefield .* (1-phasefield) .* (1-2*phasefield);
end