classdef TestSimulation < phasefieldMethod.Simulation
    %TestSimulation A Simulation with additional informations available.
    % This class defines the dependend properties
    %   interfaceThickness
    %   interfaceEnergy
    %   growthVelocity
    %   relaxationTime
    %   criticalRadius
    % that are calculated from the parameters in the superclass Simulation.
    % See their doc for details.
    % Additionally, some helpfull methods are added:
    %   exportSlice
    %   checkResolution
    
     properties (Dependent)
        % Diffuse interface thickness [m]
        interfaceThickness;
        
        % interfacial energy [J/m^2]
        interfaceEnergy;
        
        % groth velocity [m/s]
        growthVelocity;
        
        % characteristic time for relaxation in equilibrium shape [s]
        relaxationTime;
        
        % critical radius for nucleation and characteristic length [m]
        criticalRadius;
    end
    
    methods
        function S = TestSimulation(spacing, seed)
            S = S@phasefieldMethod.Simulation(spacing, seed);
            S.J = 0;
        end
        
        function exportSlice(S, folder, index, height)
            filename = fullfile(folder,sprintf('slice_%d_%d.vtk',height,index));
            field = S.phasefield(:,:,height);
            field(S.structures) = -1;
            phasefieldMethod.mat2vtk(field, filename,  S.spacing, ...
                                        [0,0,0], 'phasefield');
        end
        
        function A = nucleusArea(S, index)
            %NUCLEUSAREA(index) - Area of the index-th nucleus as seen from
            %                     above (z-axis).
            % Returns:
            %   A - Projected area of index-th nucleus.
            %
            % Grain tracking must be enabled.
            
            % Locate points of the index-th grain, project to surface by
            % logical or in z direction, sum over s and y direction
            A = sum(sum( any( (S.grainindices == index), 3 ) ));
            A = A*(S.spacing*S.spacing);
        end
        
        function badness = checkResolution(S)
            %CHECKRESOLUTION() - Compare the spacing to the characteristic
            %                    lenght scale.
            % Returns:
            %   badness - Grid spacing divided by critical radius
            
            badness = gather( max(max(max( S.spacing ./ S.criticalRadius ))) );
        end
        
        function analyse(S, f)
            %ANALYSE(outputstream) - Print some information to a file or
            %   the standard output.
            % Arguments:
            %   outputstream - (optional) A file handle from fopen to write
            %       into. Default is writing to standart output.
            
            if nargin < 2
                % If no outputstream is given, use standart (fID=1)
                f = 1;
            end
            
            [Nx, Ny, Nz] = size(S.phasefield);
            DIT = mean(mean(mean( S.interfaceThickness )));
            r_c =  mean(mean(mean( S.criticalRadius )));
            tau =  mean(mean(mean( S.relaxationTime )));
            v =  mean(mean(mean( S.growthVelocity )));
            J =  mean(mean(mean( S.J )));
            
            fprintf(f, 'State:\n');
            fprintf(f, 'Simulated time: %e s\n', S.time);
            fprintf(f, 'Transformed fraction: %e\n', S.transformedFraction);
            fprintf(f, '\n');
            fprintf(f, 'In the following setting:\n');
            fprintf(f, 'Simulating %e x %e x %e m^3 \n', ...
                    Nx*S.spacing, Ny*S.spacing, Nz*S.spacing);
            fprintf(f, '          (%d x %d x %d px)\n', Nx, Ny, Nz);
            fprintf(f, 'with resolution:\n');
            fprintf(f, '  spacing:  %e m\n', S.spacing);
            fprintf(f, '  timestep: %e s\n', S.dtime);
            fprintf(f, '\nPhysical parameters:\n');
            fprintf(f, '  DIT: %e m\n', DIT);
            fprintf(f, '  r_c: %e m\n', r_c);
            fprintf(f, '  tau: %e s\n', tau);
            fprintf(f, '  v:   %e m/s\n', v);
            fprintf(f, '  J:   %e 1/m^3/s\n', J);
            fprintf(f, '\n');
            fprintf(f, 'time resolution badness:    %.3f\n', ...
                    S.checkNumericalStability());
            fprintf(f, 'spacial resolution badness: %.3f\n', ...
                    S.checkResolution());
            fprintf(f, '\n');
            growthtime = S.spacing/v;
            fprintf(f, 'time per growht of one pixel: %e s (%.1f steps)\n', ...
                    growthtime, growthtime/S.dtime);
            fprintf(f, '        compare to grainEvery %e s \n', S.grainEvery);
            nucleationtime = 1/(J*numel(S.phasefield)*S.spacing^3);
            fprintf(f, 'time per nucleation event:    %e s (%.1f steps)\n', ...
                    nucleationtime, nucleationtime/S.dtime);
            fprintf(f, '   compare to nucleationEvery %e s \n', ...
                    S.nucleationEvery);
            fprintf(f, '\n');
            halftime3d = (3*log(2)/(pi*J*v^3))^(1/4);
            halftime2d = (3*log(2)/(pi*J*v^2 * ...
                               min([Nx,Ny,Nz])*S.spacing))^(1/3);
            fprintf(f, 'half crystallised (JMAK 3D) after: %e s (%.1f steps)\n', ...
                    halftime3d, halftime3d/S.dtime);
            fprintf(f, 'half crystallised (JMAK 2D) after: %e s (%.1f steps)\n', ...
                    halftime2d, halftime2d/S.dtime);
        end
        
        function S = autoChooseGrainTimestep(S, goodness)
            %AUTOCHOOSEGRAINTIMESTEP(goddness) - Automaticly choose a
            %                                  timestep for grain tracking.
            % If the optional argument is given, choose a timestep smaller
            % than the maximal stable one by this factor.
            % Arguments:
            %   goodness - Choose a timestep smaller by this factor
            % Returns:
            %   S - The updated object
            
            if nargin < 2
                goodness = 1;
            end
            
            v = max(max(max( S.growthVelocity() )));
            dt = S.spacing / v / goodness;
            S = S.grainSettings(dt, S.grainTreshold);
        end
        
        function S = autoChooseNucleationTimestep(S, goodness)
            %AUTOCHOOSENUCLEATIONTIMESTEP(goddness) - Automaticly choose a
            %                                      timestep for nucleation.
            % If the optional argument is given, choose a timestep smaller
            % than the maximal stable one by this factor.
            % Arguments:
            %   goodness - Choose a timestep smaller by this factor
            % Returns:
            %   S - The updated object
            
            if nargin < 2
                goodness = 1;
            end
            
            % Time to grow on px
            v = max(max(max( S.growthVelocity() )));
            dt1 = S.spacing / v;
            % Mean time for a nucleaion event to happen
            J = mean(mean(mean( S.J )));
            dt2 = 1/(J*numel(S.phasefield)*S.spacing^3);
            % Take something with an order of magnitude in between
            dt = sqrt(dt1*dt2) / goodness;
            S = S.settings(S.dtime, dt);
        end
        
        function DIT = get.interfaceThickness(S)
            DIT = S.eps ./ sqrt(2*S.W);
        end
        
        function IE = get.interfaceEnergy(S)
            IE = (S.eps / 3) .* sqrt(S.W / 2);
        end
        
        function v = get.growthVelocity(S)
            v = 6 .* S.M .* S.interfaceThickness .* S.dGv;
        end
        
        function tau = get.relaxationTime(S)
            tau = 1 ./ (S.M .* S.W);
        end
        
        function r = get.criticalRadius(S)
            r = S.eps ./ 3 .* sqrt(2*S.W) ./ S.dGv;
        end
    end
end

