classdef Simulation
    %SIMULATION The most general phase field simulation considered here.
    % It consists of a field for the order parameter and can keep track of
    % grains. The order parameter is evolved according to the equations in
    % the file evolve_phasefield.m with the physical parameters specified
    % in this object. Nucleation is implemented in a classical nucleation
    % theory formalism.
    % See doc on the methods on how to use this class.
    %
    % Properties:
    %  (Parameters for the evolution: read-only, can be scalars or fields of
    %  the same size as the phasefield)
    %     M - Mobility [m^3/J/s]
    %     eps - gradient energy coefficient [sqrt(J/m)]
    %     W - double well depth [J/m^3]
    %     dGv - free energy difference between phases [J/m^3]
    %     J - nucleation rate [1/s/m^3]
    %  (Evolving fields: read-only, gpuArrays)
    %     phasefield - The field of the order parameter, single precision
    %          floats.
    %   grainindices - Field of indices of grains, uint32. Can be a scalar
    %          as long as grain identification is disabled, to save storage.
    %   graincount - Total number of grains, equal to the maximum value of
    %          grainindices. Thif varaible is of type uint32.
    %  (Settings, read-only)
    %     spacing - distance between lattice points [m]
    %     dtime - finite time step [s]
    %     time - Evolved time [s]
    %     nucleationEvery - time step between nucleation events [s]
    %     grainEvery - time step for grain identification [s]
    %     grainTreshold - treshold in order parameter for grain
    %          identificaion
    %  (Derived quantities)
    %     transformed fraction - Overall fraction of phasefield with order
    %          parameter 1. Equals to the mean over all elements
    %
    % Methods:
    %   settings
    %   setParameters
    %   grainSettings
    %   setField
    %   apply
    %   checkNumericalStability
    %   adjustTimestep
    %   evolve
    %   backupState
    %   backupParams
    %   fromBackup
    %   export
    %   infoLine
    %   infoLineHeader
    %
    % About inheritance:
    %  Subclasses can be definded to use different physical models for the
    %  parameters in the phase field evolution equation. They must define a
    %  constructor calling the superclass constructor with the two
    %  this classes constructor needs.
    %  If the class derives this parameters from other pyhsical parameters,
    %  it should have them as properties and overwrite setParameters()
    %  appropriately. Then apply() has to be overwritten, see its doc for
    %  details.
    %  If the parameters should be used for saving and recreating states of
    %  the simulation, backupParams and fromBackup have to be replaced. If 
    %  additional state information should be saved, backupState can be
    %  overwritten too.
    %  Methods infoLine() and infoLineHeader() can be overwritten to give
    %  other or additional information.
    
    % Parameters and evolving fields
    properties (SetAccess = protected)
        M;   % mobility [m^3/J/s]
        eps; % gradient energy coefficient [sqrt(J/m)]
        W;   % double well depth [J/m^3]
        dGv; % free energy difference between phases [J/m^3]
        J;   % nucleation rate [1/s/m^3]
        
        phasefield;
        grainindices = 0;
        graincount = 0;
    end
    
    % Settings
    properties (SetAccess = private)
        spacing;             % distance between lattice points, m
        dtime = 0;           % finite time step, s
        time = 0;            % simulated time, s
        nucleationEvery = 0; % time step between nucleation events, s
        grainEvery = 0;      % time step for grain identification, s
        grainTreshold = .5;  % treshold in order parameter for grain tracking
    end
    
    % Internal stuff
    properties (SetAccess = private, GetAccess = private, Hidden)
        % This are the forms in which the parameters enter the phase field
        % evolution equation. Save them like this to save computation time.
        Meps2dt_h2;
        MWdt;
        MdGvdt;
        
        % Save the shape of this simulations phasefield to assert that all
        % given fields have the same size.
        shape = [];
        
        % These information are necessary for efficient calculation of the
        % laplacian, see there for details.
        structures;
        structneighbours;
        
        % The random number stream for nucleation events
        randomnumberstream;
    end
    
    % Derived properties
    properties (SetAccess = private, Dependent)
        % Overall fraction of the phase field with
        % order parameter one (equals to the mean over
        % the whole field)
        transformedFraction; 
    end
    
    methods
        
        % Core functionality that might be replaced by subclasses
        
        function S = Simulation(spacing, seed)
            %SIMULATION(spacing, seed) - Create a simulation.
            % Before simulating, you need to pass parameters, define a time
            % step and set an initial phasefield.
            % Arguments:
            %   spacing - Distance between lattice points in meters
            %   seed - Seed for random number generation
            % Returns:
            %   S - An instance of this class
            %
            % All subclasses must define a consturctor to call this
            % superclass constructor to provide these parameters.
            
            assert(isscalar(spacing) && isnumeric(spacing), ...
                   'spacing must be scalar');
            
            S.spacing = gpuArray(double(spacing));
            S.randomnumberstream = ...
                    parallel.gpu.RandStream('Threefry4x64-20','Seed',seed);
        end
        
        function S = setParameters(S, M, eps, W, dGv, J)
            %SETPARAMETERS(M, eps, W, dGv, J) - Set physical paramters for
            %                                   growth and nucleation.
            % Must be called at least once before evolving. Note that the
            % changes only take effect after apply() is called.
            % Arguments
            %   M - mobility [m^3/J/s]
            %   eps - gradient energy coefficient [sqrt(J/m)]
            %   W - double well depth [J/m^3]
            %   dGv - bulk energy [J/m^3]
            %   J - nucleation rate [1/m^3/s]
            % Returns:
            %   S - The updated object
            %
            % This method can be overwritten by subclasses that derive
            % these parameters from other quantities. All the parameters
            % must be set, through this method ore additional ones.
            
            S = S.assertForm(M, 'M');
            S = S.assertForm(eps, 'eps');
            S = S.assertForm(W, 'W');
            S = S.assertForm(dGv, 'dGv');
            S = S.assertForm(J, 'J');
            
            S.M   = single(M);
            S.eps = single(eps);
            S.W   = single(W);
            S.dGv = single(dGv);
            S.J   = single(J);
        end
        
        function S = apply(S)
            %APPLY() - Apply changes from new parameters.
            % Generate the parameters that appear in the phase field
            % equation from the physical ones given.
            % Returns:
            %   S - The updated object
            %
            % This method should be overwritten by any subclass that
            % derives the parameters in this class from other quantities.
            % Overwriting method must calculate M, eps, dGv, W and J and
            % subsequently call this superclass method.
            
            S.Meps2dt_h2 = gpuArray(single( S.M .* S.eps .* S.eps ...
                            .* S.dtime ./ (S.spacing .* S.spacing)));
            S.MWdt = gpuArray(single( 2 * S.M .* S.W .* S.dtime ));
            S.MdGvdt = gpuArray(single( 6 * S.M .* S.dGv .* S.dtime ));
        end
        
        function backupState(S, file)
            %BACKUPSTATE(file) -  Write the current state of this
            %                     simulation into a .mat file.
            % Arguments:
            %   file - Handle of a .mat file
            % Returns nothing
            %
            % Can be overwritten to save additional state information, but
            % should call this superclass method. All additional
            % information that is evolved should be saved here and used to
            % recreate the state in fromBackup()
            
            file.phasefield = S.phasefield;
            file.grainindices = S.grainindices;
            file.graincount = S.graincount;
            file.time = S.time;
            file.rngstate = S.randomnumberstream.State;
        end
        
        function backupParams(S, file)
            %BACKUPPARAMS(file) - Write the settings and evolution
            %                     parameters to a .mat file.
            % Arguments:
            %   file - Handle of a .mat file
            % Returns nothing
            %
            % Can be overwritten to save additional or other parameters.
            % All parameters that determine the evolution of the phase
            % field should be saved here and used to recreate the state
            % in fromBackup()
            % Overwriting functions must call backupParamsCore(file).
            
            S.backupParamsCore(file);
            file.M = S.M;
            file.eps = S.eps;
            file.W = S.W;
            file.dGv = S.dGv;
            file.J = S.J;
        end
        
        % Information, derived quantities
        
        function exportPhasefield(S, folder, index)
            %EXPORTPHASEFIELD(folder, index) - Export the current
            %                                  phasefield to a .vtk file.
            % Arguments:
            %   folder - Name of the folder to save in. Must already exist.
            %   index - An index to this export. Will appear in the
            %      filename.
            % Returns nothing.
            
            filename = fullfile(folder,sprintf('phasefield_%d.vtk',index));
            field = S.phasefield;
            field(S.structures) = -1;
            phasefieldMethod.mat2vtk(field, filename,  S.spacing, ...
                                        [0,0,0], 'phasefield');
        end
        
        function exportGrainindices(S, folder, index)
            %EXPORTGRAININDICES(folder, index) - Export the current grain
            %                                 indices field to a .vtk file.
            % Arguments:
            %   folder - Name of the folder to save in. Must already exist.
            %   index - An index to this export. Will appear in the
            %      filename.
            % Returns nothing.
            
            filename = fullfile(folder,sprintf('grainindices_%d.vtk',index));
            % convert to double and set grain borders from unit32 max value
            % to -1.
            field = double(S.grainindices);
            field(field == double(bitcmp(uint32(0)))) = -1;
            phasefieldMethod.mat2vtk(field, filename,  S.spacing, ...
                                        [0,0,0], 'grainindices');
        end
        
        function exportParameters(S, folder, index)
            %EXPORTPARAMETERS(folder, index) - Export the current
            %                                  parameters.
            % This function does nothing, but can be overwritten to export
            % parameters.
            % Arguments:
            %   folder - Name of the folder to save in. Must already exist.
            %   index - An index to this export. Will appear in the
            %      filename.
            % Returns nothing.
        end
        
        function l = infoLine(S)
            %INFOLINE() - Print a csv style info line about the current state.
            % Returns:
            %   l - An info line
            
            l = sprintf('%d, %d, %d\n', S.time, S.graincount, ...
                                        S.transformedFraction);
        end
        
        function perc = get.transformedFraction(S)
            % The fraction of the phasefield that has a phase with order
            % parameter one
            perc = double( gather( mean(mean(mean( ...
                                      S.phasefield( ~S.structures ) ))) ));
        end
    end
    
    % Core functionality that does not need to be overwritten
    methods (Sealed)
        function S = settings(S, dtime, nucleationDtime)
            %SETTINGS(dtime, nucleationDtime) - Update Settings.
            % Arguments:
            %   dtime - Time step size
            %   nucleationDtime - (optional) Time step between identifiing
            %       grains. Can be set to zero to disable grain
            %       identification.
            % Returns:
            %   S - The updated object
            
            if nargin < 3
                nucleationDtime = S.nucleationEvery;
            end
            
            assert(isscalar(dtime) && isnumeric(dtime), ...
                   'time step must be scalar');
            assert(isscalar(nucleationDtime) && isnumeric(dtime), ...
                   'time step must be scalar');
           
            S.dtime = double(dtime);
            S.nucleationEvery = double(nucleationDtime);
        end
        
        function S = grainSettings(S, dtime, treshold)
            %GRAINSETTINGS(dtime, treshold) - Change settings for grain
            %                                 identification.
            % Arguments:
            %   dtime - update the grain indices with these intervals
            %   treshold - count points with a phase > treshold to a grain
            % Returns:
            %   S - The updated object
            
            assert(isscalar(dtime) && isnumeric(dtime), ...
                   'time step must be scalar');
            assert(isscalar(treshold) && isnumeric(dtime), ...
                   'treshold must be scalar');
            
            % If grain tracking is enabled and the grain indices field is
            % only a scalar (which it may be as long as grain tracking was
            % disabled), and if this is not because the field has not been
            % initialised yet, provide a grainindeces field.
            if dtime ~= 0 && isscalar(S.grainindices) ...
                    && ~isempty(S.phasefield)
                S.grainindices = zeros(S.shape,'uint32','gpuArray');
            end
            
            S.grainEvery = double(dtime);
            S.grainTreshold = gpuArray(single(treshold));
        end
        
        function S = setField(S, phasefield, grainindices)
            %SETFIELD(phasefield, grainindices) - Initialise or update the
            %                              phasefield and the grainindices.
            % Arguments:
            %   phasefield - The field of order parameter
            %   grainindices - (optinal) A field of indices for the grains,
            %       if grain identification is considered and initial
            %       grains are placed.
            % Returns:
            %   S - The updated object
            
            S = S.assertForm(phasefield, 'phasefield', false);
            
            if nargin < 3
                % If no grainindices field is given, create a default. If
                % grain tracking is enabled, use an empty field. If it is
                % disabled, set a scalar to save storage.
                if S.grainEvery == 0
                    grainindices = zeros(size(phasefield),'uint32','gpuArray');
                else
                    grainindices = uint32(0);
                end
            else
                % If a grainindices field is given, check its form. It is
                % allowed to be a scalar if grain tracking is disabled.
                allowScalar = (S.grainEvery == 0);
                S = S.assertForm(grainindices, 'grainindices', allowScalar);
            end
            
            S.phasefield = gpuArray(single(phasefield));
            [S.structures, S.structneighbours] = ...
                phasefieldMethod.analyse_structure_7(phasefield);
            S.grainindices = uint32(grainindices);
            S.graincount = max(max(max(grainindices)));
        end
        
        function badness = checkNumericalStability(S)
            %CHECKNUMERICALSTABILITY() - Check numerical stability by Von
            %                            Neumann stability analysis.
            % Returned value is used time step divided by maximal useable
            % time step. Thus, a returned value smaller than one should be
            % ok, a greater one will not be stable.
            % Returns:
            %   badness - used timestep over maximal usable
            
            maxdtime = min(min(min( S.spacing^2 ./ (6 * S.M .* S.eps.^2) )));
            badness = S.dtime / gather(maxdtime);
        end
        
        function S = adjustTimestep(S, goodness)
            %ADJUSTTIMESTEP(goodness) - Automatically adjust time step to
            %                    fullfill the numerical stability condtion.
            % If the optional argument is given, choose a timestep smaller
            % than the maximal stable one by this factor.
            % Arguments:
            %   goodness - Choose a timestep smaller by this factor
            % Returns:
            %   S - The updated object
            
            if nargin == 1
                goodness = 1;
            end
            
            assert(isscalar(goodness) && isnumeric(goodness), ...
                   'goodness must be a scalar');
            
            S.dtime = double(gather( min(min(min( ...
                    S.spacing^2 ./ (6 * S.M .* S.eps.^2) ))) )) / goodness;
        end
        
        function [S, done] = evolve(S, time)
            %EVOLVE(time) - Evolves the phasefield, tracking grains and
            %               planting nuclei
            % Arguments:
            %   time - The time to simulate
            % Returns:
            %   S - The updated Simulation object
            %   done - The actual simulated time. Might differ from given
            %       time by up to one timestep
            
            % iterate over (at least) the given time. The overall number of
            % steps is used to consider rare nucleation or grain
            % identification (for example, if the temperature is externally
            % changed more frequent)
            steps = ceil(time/S.dtime);
            
            for step = 1 : steps
                S.time = S.time + S.dtime;
                
                % evolve the phasefield with external function
                S.phasefield = phasefieldMethod.evolve_phasefield( ...
                    S.phasefield, S.Meps2dt_h2, S.MWdt, S.MdGvdt, ...
                    S.structures, S.structneighbours);
                
                % Update grain indices every grainEvery time intervall.
                % This modulo goes from 0 to grainEvery in steps of
                % dtime, and the condition is fullfilled when time is
                % approximately a multiple of grainEvery. As mod(time,0) is
                % time, and time > dtime for every time, a grainEvery of 0
                % disables grain tracking.
                if mod(S.time, S.grainEvery) < S.dtime
                    update_at = and( (S.grainindices == 0), ...
                                     (S.phasefield > S.grainTreshold));
                    S.grainindices = phasefieldMethod.zero2neighbour( ...
                                          S.grainindices, update_at);
                end
                
                % Consider nucleation every nucleationEvery time intervall.
                % See grain indentification for the mod() construction.
                if mod(S.time, S.nucleationEvery) < S.dtime
                    % Nucleation probability. Exclude points already
                    % crystalised (with some tolerance as the interface
                    % region is theoretically infinite)
                    % -expm1() is equal to 1-exp(), but more precise
                    P = -expm1(-S.nucleationEvery*S.spacing^3*S.J) ...
                        .* (S.phasefield < 0.5);
                    % Random numbers for comparison (is on GPU as the
                    % random number stream is configured accordingly)
                    R = rand(S.randomnumberstream, ...
                             size(S.phasefield));
                    % Where to plant nuclei
                    positions = ( P > R );
                    % If there are some to plant, do so
                    % (sum is about 60 times faster than max)
                    if sum(sum(sum(positions))) > 0
                        [S.phasefield, S.graincount, S.grainindices] = ...
                            phasefieldMethod.plant_nuclei( ...
                                 S.phasefield, S.graincount,S.grainindices, ...
                                 find(positions), S.dGv, S.W, S.eps, ...
                                 S.spacing, S.grainTreshold);
                    end
                end
            end
            done = steps*S.dtime;
        end
    
    end
    
    % Static methods that might be overwritten by subclasses
    methods (Static)
        function l = infoLineHeader()
            %INFOLINEHEADER() - Print the column titles for the csv style 
            %                   info from method
            % Returns:
            %   l - Column titles
            %
            % If infoLine() is overwritten in a subclass, this method must
            % be overwritten too to provide right information.
            
            l = 'time [s], graincount, transformedFraction\n';
        end
        
        function S = fromBackup(paramsfile, statefile)
            %FROMBACKUP(paramsfile, statefile) - Create a Simulation object
            %                                    from backups.
            % Arguments:
            %   paramsfile - A file handle of a .mat file in which the
            %       parameters of a Simulation have been saved by
            %       backupParams method.
            %   statefile - A file handle of a .mat file in which the state
            %      of a simulation has been saved by backupState
            % Returns:
            %   S - An instance with saved parameters and state
            %
            % Subclasses that derive the parameter of this class from other
            % quantities should overwrite this method. It must recreate the
            % state of the simulation that was saved. For that, all
            % overwriting methods must call fromBackupCore(f, paramsfile,
            % statefile) where f is a function pointer to the constructor
            % of the subclass.
            % In the first file the data can be found that is saved with
            % backupParams(), in the second one the data that is saved with
            % backupState().
            
            S = phasefieldMethod.Simulation.fromBackupCore( ...
                       @phasefieldMethod.Simulation, paramsfile, statefile);
            % Do not use setParameters, wich might be overwritten.
            S.M = paramsfile.M;
            S.eps = paramsfile.eps;
            S.W = paramsfile.W;
            S.dGv = paramsfile.dGv;
            S.J = paramsfile.J;
        end
    end
    
    % Core functions that must be called by some methods, if they are
    % overwritten
    methods (Sealed, Access = protected)
        function S = assertForm(S, x, name, allowScalar)
            %ASSERTFORM - Raises an error if x is neither a scalar nor of
            %             the size the fields in this simulation are.
            % The first time this function is called with a non scalar
            % input, the size is saved and used as only valid size for all
            % futher function calls.
            % Arguments:
            %   x - Any argument to test
            %   name - Name of the variable (used for the error message)
            %   allowScalar - (optional) Can be set to false if x is not
            %       allowed to be scalar, e.g. for the phasefield.
            % Returns:
            %   S - The updated object
            
            if nargin < 4
                allowScalar = true;
            end
            
            % If no shape is saved and x is not a scalar, save the shape of
            % x as reference
            if isempty(S.shape) && ~isscalar(x)
                S.shape = size(x);
            end
            
            % Assert that either x is a scalar or the size of x is the
            % saved size (which will give an error if the size vectors have
            % different length, so test for number of dimensions first)
            assert((allowScalar && isscalar(x)) || ...
                   (ndims(x) == length(S.shape) && all(size(x)==S.shape)), ...
                   'The size of %s does not fit', name)
        end
        
        function backupParamsCore(S, file)
            %BACKUPPARAMSCORE(file) - Writes the core parameters of this
            %                         simulation to a file.
            % Saves the spacing, time step, nucleation time step and the
            % two settings for grain tracking.
            % Arguments:
            %   file - Handle of a .mat file
            % Returns nothing
            %
            % Must be called by backupParams and any method overwriting it.
            
            file.spacing = S.spacing;
            file.dtime = S.dtime;
            file.nucleationEvery = S.nucleationEvery;
            file.grainEvery = S.grainEvery;
            file.grainTreshold = S.grainTreshold;
        end
    end
    
    methods (Static, Sealed, Access = protected)
        function S = fromBackupCore(constructor, paramsfile, statefile)
            %FROMBACKUPCORE(constructor, paramsfile, statefile) - Creates a
            %                   Simulation or subclass of Simulation using
            %                   the core parameters and states from a file.
            % Core parameters are those that can not be changed by
            % subclasses: spacing, random number generator properties, time
            % step and nucleation time step, grain settings and the fields.
            % Arguments:
            %   constructor - Function handle to the constructor of this
            %       class or a subclass
            %   paramsfile - Handle of a file in which parameters were saved.
            %   statefile - Handle of a file in which a state was saved.
            
            S = constructor(paramsfile.spacing, 42);
            set(S.randomnumberstream,'State',statefile.rngstate);
            S = S.settings(paramsfile.dtime, paramsfile.nucleationEvery);
            S = S.grainSettings(paramsfile.grainEvery, paramsfile.grainTreshold);
            S = S.setField(statefile.phasefield, statefile.grainindices);
            S.time = statefile.time;
        end
    end
end