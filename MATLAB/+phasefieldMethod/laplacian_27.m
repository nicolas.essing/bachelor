function lplc = laplacian_27(mat, undef, neighbours)
%LAPLACIAN_27(mat, undef, neighbours) - Laplace operator
%
% Custom numerical laplace operator, using 27-point stencil, for a field
% with boundary condition that the derivative of the field is zero at
% the boundarys.
% The distance between two points is assumed to be 1, for a different
% distance divide the result by the squared distance.
%
% Arguments:
%   mat - The numerical representation ot the field
%   undef - A logical matrix of same size as mat, which is 1 where the
%       field is not defined, i.e. where mat is NaN.
%   neighbours - A matrix of same size as mat, with each element giving
%       the number of neighbouring (in sense of the 27-point stencil)
%       points that are not free to the field. The boundary of the matrix
%       has to be considered in this sense as well.
% Returns:
%   laplace - The numerical approximated laplace of the field
% 
% Note that the result is arbitary outside of where the field is
% defined.
    
    [Nx, Ny, Nz] = size(mat);
    
    % set matrix to zero where it is undefined.
    mat(undef) = 0;
    
    % calculate laplace ignoring boundary effects
    lplc = -88*mat;
    
     % 6 points on the faces, weighted with 6
    lplc(2:Nx,:,:)   = lplc(2:Nx,:,:) + mat(1:Nx-1,:,:) *6;
    lplc(1:Nx-1,:,:) = lplc(1:Nx-1,:,:) + mat(2:Nx,:,:) *6;
    lplc(:,2:Ny,:)   = lplc(:,2:Ny,:) + mat(:,1:Ny-1,:) *6;
    lplc(:,1:Ny-1,:) = lplc(:,1:Ny-1,:) + mat(:,2:Ny,:) *6;
    lplc(:,:,2:Nz)   = lplc(:,:,2:Nz) + mat(:,:,1:Nz-1) *6;
    lplc(:,:,1:Nz-1) = lplc(:,:,1:Nz-1) + mat(:,:,2:Nz) *6;
    
    % 12 points on the edges, weighted with 3
    lplc(2:Nx,2:Ny,:)     = lplc(2:Nx,2:Ny,:) + mat(1:Nx-1,1:Ny-1,:) *3;
    lplc(2:Nx,1:Ny-1,:)   = lplc(2:Nx,1:Ny-1,:) + mat(1:Nx-1,2:Ny,:) *3;
    lplc(2:Nx,:,2:Nz)     = lplc(2:Nx,:,2:Nz) + mat(1:Nx-1,:,1:Nz-1) *3;
    lplc(2:Nx,:,1:Nz-1)   = lplc(2:Nx,:,1:Nz-1) + mat(1:Nx-1,:,2:Nz) *3;
    lplc(1:Nx-1,2:Ny,:)   = lplc(1:Nx-1,2:Ny,:) + mat(2:Nx,1:Ny-1,:) *3;
    lplc(1:Nx-1,1:Ny-1,:) = lplc(1:Nx-1,1:Ny-1,:) + mat(2:Nx,2:Ny,:) *3;
    lplc(1:Nx-1,:,2:Nz)   = lplc(1:Nx-1,:,2:Nz) + mat(2:Nx,:,1:Nz-1) *3;
    lplc(1:Nx-1,:,1:Nz-1) = lplc(1:Nx-1,:,1:Nz-1) + mat(2:Nx,:,2:Nz) *3;
    lplc(:,2:Ny,2:Nz)     = lplc(:,2:Ny,2:Nz) + mat(:,1:Ny-1,1:Nz-1) *3;
    lplc(:,2:Ny,1:Nz-1)   = lplc(:,2:Ny,1:Nz-1) + mat(:,1:Ny-1,2:Nz) *3;
    lplc(:,1:Ny-1,2:Nz)   = lplc(:,1:Ny-1,2:Nz) + mat(:,2:Ny,1:Nz-1) *3;
    lplc(:,1:Ny-1,1:Nz-1) = lplc(:,1:Ny-1,1:Nz-1) + mat(:,2:Ny,2:Nz) *3;
    
    % 8 points in the corners, weighted with 2
    lplc(2:Nx,2:Ny,2:Nz)       = lplc(2:Nx,2:Ny,2:Nz) + mat(1:Nx-1,1:Ny-1,1:Nz-1) *2;
    lplc(2:Nx,2:Ny,1:Nz-1)     = lplc(2:Nx,2:Ny,1:Nz-1) + mat(1:Nx-1,1:Ny-1,2:Nz) *2;
    lplc(2:Nx,1:Ny-1,2:Nz)     = lplc(2:Nx,1:Ny-1,2:Nz) + mat(1:Nx-1,2:Ny,1:Nz-1) *2;
    lplc(2:Nx,1:Ny-1,1:Nz-1)   = lplc(2:Nx,1:Ny-1,1:Nz-1) + mat(1:Nx-1,2:Ny,2:Nz) *2;
    lplc(1:Nx-1,2:Ny,2:Nz)     = lplc(1:Nx-1,2:Ny,2:Nz) + mat(2:Nx,1:Ny-1,1:Nz-1) *2;
    lplc(1:Nx-1,2:Ny,1:Nz-1)   = lplc(1:Nx-1,2:Ny,1:Nz-1) + mat(2:Nx,1:Ny-1,2:Nz) *2;
    lplc(1:Nx-1,1:Ny-1,2:Nz)   = lplc(1:Nx-1,1:Ny-1,2:Nz) + mat(2:Nx,2:Ny,1:Nz-1) *2;
    lplc(1:Nx-1,1:Ny-1,1:Nz-1) = lplc(1:Nx-1,1:Ny-1,1:Nz-1) + mat(2:Nx,2:Ny,2:Nz) *2;
    
    % correct the boundary effects and use the point distance
    lplc = (lplc + mat .* neighbours) / 26;
end

