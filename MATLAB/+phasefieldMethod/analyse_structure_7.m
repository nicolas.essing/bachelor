function [undef, neigh] = analyse_structure_7( mat )
%ANALYSE_STRUCTURE_7(mat) - Analyse the structures in a field.
%
% Analyses the structures in a field using a 7 point stencil.
% In this context, structures in the field are equivalent to NaN values
% in the given numerical representation of the field.
%
% Arguments:
%   mat - The numerical representation of the field
% Returns:
%   undef - A logical matrix of same size as mat, which is 1 where
%       the field is not defined
%   neighbours - A matrix of same size as mat, with each element
%       giving the number of neighbouring (in sense of the
%       7-point stencil) points that are not free to the
%       field. The boundary of the matrix is also considered
%       in this sense.
    
    % use build in function to search for NaN
    undef = isnan(mat);
    
    % extract dimensions and make a number like copy of nanindices
    [Nx, Ny, Nz] = size(undef);
    neigh = zeros(Nx,Ny,Nz,'single','gpuArray');
    
    % for each direction, add one if the neighbour is NaN
    neigh(2:Nx,:,:)   = neigh(2:Nx,:,:) + undef(1:Nx-1,:,:);
    neigh(1:Nx-1,:,:) = neigh(1:Nx-1,:,:) + undef(2:Nx,:,:);
    neigh(:,2:Ny,:)   = neigh(:,2:Ny,:) + undef(:,1:Ny-1,:);
    neigh(:,1:Ny-1,:) = neigh(:,1:Ny-1,:) + undef(:,2:Ny,:);
    neigh(:,:,2:Nz)   = neigh(:,:,2:Nz) + undef(:,:,1:Nz-1);
    neigh(:,:,1:Nz-1) = neigh(:,:,1:Nz-1) + undef(:,:,2:Nz);
    
    % add 1 at each border
    neigh(1,:,:)  = neigh(1 ,:,:) + 1;
    neigh(Nx,:,:) = neigh(Nx,:,:) + 1;
    neigh(:,1,:)  = neigh(:,1 ,:) + 1;
    neigh(:,Ny,:) = neigh(:,Ny,:) + 1;
    neigh(:,:,1)  = neigh(:,:,1 ) + 1;
    neigh(:,:,Nz) = neigh(:,:,Nz) + 1;

end