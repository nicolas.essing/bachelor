function [phasefield, graincount, grainindices] = ...
    plant_nuclei(phasefield,graincount,grainindices,positions,...
                 dGv,W,eps,spacing,treshold)
    % Plants nuclei in a phasefield at given positions and with given size.
    % Arguments:
    %   phasefield - The phasefield of the ordering parameter
    %   graincount - How many nuclei already are in the field
    %   grainindices - The field assinging each crystalised point a grain
    %                  index
    %   positions - The positions where to plant the nuclei in the
    %               phasefield (as a list of linear indices, as returned
    %               e.g. from find())
    %   rcrit - The field of critical radii
    %   W - depth of double-well energy term
    %   eps - gradient energy coefficient
    %   spacing - The difference between neighbouring points
    %   treshold - Grain tracking treshold
    % Returns:
    %   phasefield - The new phasefield including the nuclei
    
    [Nx, Ny, Nz] = size(phasefield);
    [xs, ys, zs] = ind2sub(size(phasefield), positions);
        
    % For each positon (shuffled, as some nuclei my overlap and this
    % prevents a bias towards lower positions) try planting a nucleus
    for index=randperm(length(xs))
        
        % Read out coordinates
        xi = xs(index);
        yi = ys(index);
        zi = zs(index);
        
        % Get field values at this position
        dGvi = get_at(dGv,xi,yi,zi);
        Wi   = get_at(W,  xi,yi,zi);
        epsi = get_at(eps,xi,yi,zi);
        
        DIT = epsi/sqrt(2*Wi);
        ri = 1/3*epsi*sqrt(2*Wi)/dGvi;

        % Cut out a cube to plant the nucleus in (cubesize is half the side
        % length of the cube used)
        cubesize = ceil( (ri+5*DIT)/spacing );
        xl = max(1,xi-cubesize);
        xr = min(Nx,xi+cubesize);
        yl = max(1,yi-cubesize);
        yr = min(Ny,yi+cubesize);
        zl = max(1,zi-cubesize);
        zr = min(Nz,zi+cubesize);
        cube = phasefield(xl:xr, yl:yr, zl:zr);

        % If some of the cube for this nucleus is already crystallised,
        % one might abort and just go to the next nucleus.
        % Use some treshold > 0 as the interface region is theoretically
        % infinit.
        % This is however not used, as it has a too large influence on the
        % nuclei count.
        % if mean(mean(mean(cube))) > 0.5
        %     continue
        % end

        structs = isnan(cube);
        
        [X,Y,Z] = ndgrid(xl:xr, yl:yr, zl:zr);
        R = sqrt((X-xi).^2+(Y-yi).^2+(Z-zi).^2);
        cube = shape(R*spacing,DIT,ri);
        cube(structs) = NaN;
        
        %TODO relax shape

        % Put the cube back in the phase field, without lowering the field
        % where it is already crystallised
        phasefield(xl:xr, yl:yr, zl:zr) = max(cube, ...
                                          phasefield(xl:xr, yl:yr, zl:zr));

        % Update the grain count and grain identification field: Set the
        % new number everywhere in the cube where the phase field is over
        % the treshold, if the index there is still 0. Update at least the
        % center point.
        graincount = graincount + 1;
        grainindices(xl:xr, yl:yr, zl:zr) = ...
                            graincount .* uint32(cube > treshold) .* ...
                            uint32(grainindices(xl:xr, yl:yr, zl:zr) == 0);
        grainindices(xi,yi,zi) = graincount;
    end
end

function phi = shape(R,DIT,r_c)
    % The functional form of the nucleus
    
    % Planar interface solution:
    % phi = 1/2*(1+tanh( (r_c-R)/2/DIT ));
    % Smoothend form:
    prefactor = 2*DIT*log(1+exp(2*r_c/(2*DIT)));
    phi0 = 2*DIT * log(cosh(r_c/(2*DIT))) / prefactor;
    phi = ( 2*DIT*log(cosh((r_c-R)/(2*DIT)))+R.*tanh((r_c-R)/(2*DIT)) ) ...
          / prefactor + 1 - phi0;
end

function value = get_at(field, x, y, z)
    % Get the value of field at a position.
    % If field is a scalar, return its value, else field(x,y,z)
    if isscalar(field)
        value = field;
    else
        value = field(x,y,z);
    end
end