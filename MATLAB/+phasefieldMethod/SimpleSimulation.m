classdef SimpleSimulation < phasefieldMethod.Simulation
    %SIMPLESIMULATION A simple model, where two of the parameters for the
    % free energy are calculated from an interfacial energy IE and a
    % interface thickness DIT. For the third parameter, a model depending
    % on temperature T, melting temperature Tm and latent heat L is used.
    % Instead of the mobility, the growth velocity is a free parameter, for
    % which an Arrhenius behaviour is assumed.
    % Thus, only the nucleation rate J remains from the general model.
    
    properties
        IE;   % interfacial energy [J/m^2]
        DIT;  % diffuse interface thickness [m]
        vinf; % growth velocity for infinite temperature [m/s]
        Ea;   % activation energy [J]
        L;    % latent heat [J/m^3]
        Tm;   % melting temperature [K]
        T;    % temperature [K]
    end
    
    properties ( Constant )
        kB = 1.380649e-23;   % Boltzmann constant, [J/K]
        eV = 1.602176634e-19 % Electron volt [J]
    end
    
    methods
        function S = SimpleSimulation(spacing, seed)
            S = S@phasefieldMethod.Simulation(spacing, seed);
        end
        
        function S = setParameters(S, IE, DIT, vinf, Ea, L, Tm, J)
            %SETPARAMETERS(IE, DIT, vinf, Ea, L, Tm, J) - Set the physical,
            %                                independent, scalar parameters
            % Arguments:
            %   IE - interfacial energy [J/m^2]
            %   DIT - diffuse interface thickness [m]
            %   vinf - growth velocity for infinite temperature [m/s]
            %   Ea - activation energy [J]
            %   L - latent heat [J/m^3]
            %   Tm - melting temperature [K]
            % Returns:
            %   S - The updated object
            
            assert(isscalar(IE) && isnumeric(IE), 'IE has wrong type or size');
            assert(isscalar(DIT) && isnumeric(DIT), 'DIT has wrong type or size');
            assert(isscalar(Ea) && isnumeric(Ea), 'Ea has wrong type or size');
            assert(isscalar(vinf) && isnumeric(vinf), 'vinf has wrong type or size');
            assert(isscalar(L) && isnumeric(L), 'L has wrong type or size');
            assert(isscalar(Tm) && isnumeric(Tm), 'Tm has wrong type or size');
            assert(isscalar(J) && isnumeric(J), 'J has wrong type or size');
            
            S.IE = single( IE );
            S.DIT = single( DIT );
            S.Ea = single( Ea );
            S.vinf = single( vinf );
            S.L = single( L );
            S.Tm = single(Tm );
            
            S.eps = sqrt(6*IE*DIT);
            S.W = 3*IE./DIT;
            
            S.J = single( J );
        end
        
        function S = setTemp(S, T)
            % Initialise or update the temperature field
            
            S = S.assertForm(T, 'T');
            
            S.T = T;
        end
        
        function S = apply(S)
            S.eps =  sqrt(6 * S.IE .* S.DIT );
            S.W =  3 * S.IE ./ S.DIT;
            S.dGv =  S.L .* (S.Tm - S.T) ./ S.Tm .* (2*S.T) ./ (S.T + S.Tm);
            v = S.vinf .* exp(-S.Ea ./ (S.kB * S.T));
            S.M = v ./ (6 * S.DIT .* S.dGv);      
            S = apply@phasefieldMethod.Simulation(S);
        end
        
        function backupParams(S, file)
            S.backupParamsCore(file);
            file.IE = S.IE;
            file.DIT = S.DIT;
            file.Ea = S.Ea;
            file.vinf = S.vinf;
            file.L = S.L;
            file.Tm = S.Tm;
            file.T = S.T;
            file.J = S.J;
        end
    end
    
    methods( Static )
        function S = fromBackup(paramsfile, statefile)
            S = phasefieldMethod.Simulation.fromBackupCore( ...
                                @phasefieldMethod.SimpleSimulation, ...
                                paramsfile, statefile);
            S.IE = paramsfile.IE;
            S.DIT = paramsfile.DIT;
            S.Ea = paramsfile.Ea;
            S.vinf = paramsfile.vinf;
            S.L = paramsfile.L;
            S.Tm = paramsfile.Tm;
            S.T = paramsfile.T;
            S.J = paramsfile.J;
            S = S.apply();
        end
    end
end