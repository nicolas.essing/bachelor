function laplace = laplacian_7(mat, undef, neighbours)
%LAPLACIAN_7(mat, undef, neighbours) - Laplace operator
%
% Custom numerical laplace operator, using 7-point stencil, for a field
% with boundary condition that the derivative of the field is zero at
% the boundarys.
% The distance between two points is assumed to be 1, for a different
% distance divide the result by the squared distance.
%
% Arguments:
%   mat - The numerical representation ot the field
%   undef - A logical matrix of same size as mat, which is 1 where the
%       field is not defined, i.e. where mat is NaN.
%   neighbours - A matrix of same size as mat, with each element giving
%       the number of neighbouring (in sense of the 7-point stencil)
%       points that are not free to the field. The boundary of the matrix
%       has to be considered in this sense as well.
% Returns:
%   laplace - The numerical approximated laplace of the field
% 
% Note that the result is arbitary outside of where the field is
% defined.
    
    [Nx, Ny, Nz] = size(mat);
    
    % set matrix to zero where it is undefined.
    mat(undef) = 0;
    
    % calculate laplace ignoring boundary effects
    laplace = -6*mat;
    laplace(2:Nx,:,:) = laplace(2:Nx,:,:) + mat(1:Nx-1,:,:);
    laplace(1:Nx-1,:,:) = laplace(1:Nx-1,:,:) + mat(2:Nx,:,:);
    laplace(:,2:Ny,:) = laplace(:,2:Ny,:) + mat(:,1:Ny-1,:);
    laplace(:,1:Ny-1,:) = laplace(:,1:Ny-1,:) + mat(:,2:Ny,:);
    laplace(:,:,2:Nz) = laplace(:,:,2:Nz) + mat(:,:,1:Nz-1);
    laplace(:,:,1:Nz-1) = laplace(:,:,1:Nz-1) + mat(:,:,2:Nz);
    
    % correct the boundary effects and use the point distance
    laplace = laplace + mat .* neighbours;
end

