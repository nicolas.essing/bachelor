classdef ExampleSimulation < phasefieldMethod.TestSimulation
    %EXAMPLESIMULATION A simple model for numerical testing.
    % The mobility is set to one, as it is not an indepentend parameter.
    % All physical settings can be achieved by changing the other
    % parameters.
    % This class derives the parameters from some properties one gets from
    % analytical analysis of the equation and gives assistance in playing
    % with them.
    
    methods
        function S = ExampleSimulation(spacing, seed)
            S = S@phasefieldMethod.TestSimulation(spacing, seed);
            S.J = 0;
        end
        
        function S = setParameters(S, DIT, v, r_c)
            %SETPARAMETERS(DIT, v, r_c) - Set the independent, scalar
            %                             parameters
            % Arguments:
            %   DIT - diffuse interface thickness [m]
            %   v - growth velocity [m/s]
            %   r_c - critical radius for nucleation and also
            %       characteristic length scale for growht [m]
            
            S.M = 1;
            S.eps = sqrt( v .* r_c ./ 2 );
            S.W = v .* r_c ./ (4 * DIT .* DIT);
            S.dGv = v ./ (6 * DIT);
        end
        
        function S = setOtherParameters(S, DIT, v, tau)
           %SETOTHERPARAMETERS(DIT, v, tau) - Set other independent, scalar
            %                                 parameters
            % Arguments:
            %   DIT - diffuse interface thickness [m]
            %   v - growth velocity [m/s]
            %   tau - time constant for relaxation into equilibrium shape
            %       [s]
            
            S.M = 1;
            S.eps = sqrt(2/tau)*DIT;
            S.W = 1/tau;
            S.dGv = v/(6*DIT);
        end
        
        function S = setNucleation(S, J)
            S.J = J;
        end
    end
end