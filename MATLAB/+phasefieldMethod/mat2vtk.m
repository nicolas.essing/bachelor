function mat2vtk(mat, filename, spacing, origin, name)
%MAT2VTK(mat, filename, spacing, origin, name) - Writes a 3D gpuArray to
%                                                .vtk file
%
% Writes the 3D gpuArray mat into a version 2.0 vtk file.
%
% Arguments:
%   mat - The 3D gpuArray to be written.
%   filename - Name of the file to write into. If it already exists, it
%       will be overwritten.
%   spacing - Distance between grid points for a correct scaling in
%       ParaView. Default is 1.
%   origin - A vector of three doubles giving the center of the field.
%       Default is [0, 0, 0].
%   name - Optional, an identifier for the data.
    
    % Provide default parameters
    if nargin < 5
        name = 'matlab_exported_field';
        if nargin < 4
            origin = [0,0,0];
            if nargin < 3
                spacing = 1;
            end
        end
    end
    
    % Get size, move data from GPU to CPU memory
    [Nx, Ny, Nz]= size(mat);
    mat = gather(mat);
    spacing = gather(spacing);
    
    % Open the file and write information
    file = fopen(filename, 'W');
    fprintf(file, '# vtk DataFile Version 2.0\n');
    fprintf(file, '%s\n', name);
    fprintf(file, 'BINARY\n');
    fprintf(file, 'DATASET STRUCTURED_POINTS\n');
    fprintf(file, 'DIMENSIONS %d %d %d\n',Nx,Ny,Nz);
    fprintf(file, 'ASPECT_RATIO %d %d %d\n', spacing, spacing, spacing);
    fprintf(file, 'ORIGIN %d %d %d\n', origin);
    fprintf(file, 'POINT_DATA %d\n',Nx*Ny*Nz);
    fprintf(file, 'SCALARS %s float 1\n', name);
    fprintf(file, 'LOOKUP_TABLE default\n');
    fwrite(file, mat(:), 'single', 'ieee-be');
    
    fclose(file);
end

