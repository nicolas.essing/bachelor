function [undef, neigh] = analyse_structure_27( mat )
%ANALYSE_STRUCTURE_27(mat) - Analyse the structures in a field.
%
% Analyses the structures in a field using a 27 point stencil.
% In this context, structures in the field are equivalent to NaN values
% in the given numerical representation of the field.
%
% Arguments:
%   mat - The numerical representation of the field
% Returns:
%   undef - A logical matrix of same size as mat, which is 1 where
%       the field is not defined
%   neighbours - A matrix of same size as mat, with each element
%       giving the number of neighbouring (in sense of the
%       27-point stencil) points that are not free to the
%       field, weighted with the same factor as in a 27 point stencil
%       laplace operator. The boundary of the matrix is also considered in
%       this sense.
    
    [Nx, Ny, Nz] = size(mat);
    undef = isnan(mat);
    neigh = zeros(Nx,Ny,Nz, 'single', 'gpuArray');
    
    % 6 points on the faces, weighted with 6
    neigh(2:Nx,:,:)   = neigh(2:Nx,:,:) + undef(1:Nx-1,:,:) *6;
    neigh(1:Nx-1,:,:) = neigh(1:Nx-1,:,:) + undef(2:Nx,:,:) *6;
    neigh(:,2:Ny,:)   = neigh(:,2:Ny,:) + undef(:,1:Ny-1,:) *6;
    neigh(:,1:Ny-1,:) = neigh(:,1:Ny-1,:) + undef(:,2:Ny,:) *6;
    neigh(:,:,2:Nz)   = neigh(:,:,2:Nz) + undef(:,:,1:Nz-1) *6;
    neigh(:,:,1:Nz-1) = neigh(:,:,1:Nz-1) + undef(:,:,2:Nz) *6;
    
    % 12 points on the edges, weighted with 2
    neigh(2:Nx,2:Ny,:)     = neigh(2:Nx,2:Ny,:) + undef(1:Nx-1,1:Ny-1,:) *3;
    neigh(2:Nx,1:Ny-1,:)   = neigh(2:Nx,1:Ny-1,:) + undef(1:Nx-1,2:Ny,:) *3;
    neigh(2:Nx,:,2:Nz)     = neigh(2:Nx,:,2:Nz) + undef(1:Nx-1,:,1:Nz-1) *3;
    neigh(2:Nx,:,1:Nz-1)   = neigh(2:Nx,:,1:Nz-1) + undef(1:Nx-1,:,2:Nz) *3;
    neigh(1:Nx-1,2:Ny,:)   = neigh(1:Nx-1,2:Ny,:) + undef(2:Nx,1:Ny-1,:) *3;
    neigh(1:Nx-1,1:Ny-1,:) = neigh(1:Nx-1,1:Ny-1,:) + undef(2:Nx,2:Ny,:) *3;
    neigh(1:Nx-1,:,2:Nz)   = neigh(1:Nx-1,:,2:Nz) + undef(2:Nx,:,1:Nz-1) *3;
    neigh(1:Nx-1,:,1:Nz-1) = neigh(1:Nx-1,:,1:Nz-1) + undef(2:Nx,:,2:Nz) *3;
    neigh(:,2:Ny,2:Nz)     = neigh(:,2:Ny,2:Nz) + undef(:,1:Ny-1,1:Nz-1) *3;
    neigh(:,2:Ny,1:Nz-1)   = neigh(:,2:Ny,1:Nz-1) + undef(:,1:Ny-1,2:Nz) *3;
    neigh(:,1:Ny-1,2:Nz)   = neigh(:,1:Ny-1,2:Nz) + undef(:,2:Ny,1:Nz-1) *3;
    neigh(:,1:Ny-1,1:Nz-1) = neigh(:,1:Ny-1,1:Nz-1) + undef(:,2:Ny,2:Nz) *3;
    
    % 8 points in the corners, weighted with 2
    neigh(2:Nx,2:Ny,2:Nz)       = neigh(2:Nx,2:Ny,2:Nz) + undef(1:Nx-1,1:Ny-1,1:Nz-1) *2;
    neigh(2:Nx,2:Ny,1:Nz-1)     = neigh(2:Nx,2:Ny,1:Nz-1) + undef(1:Nx-1,1:Ny-1,2:Nz) *2;
    neigh(2:Nx,1:Ny-1,2:Nz)     = neigh(2:Nx,1:Ny-1,2:Nz) + undef(1:Nx-1,2:Ny,1:Nz-1) *2;
    neigh(2:Nx,1:Ny-1,1:Nz-1)   = neigh(2:Nx,1:Ny-1,1:Nz-1) + undef(1:Nx-1,2:Ny,2:Nz) *2;
    neigh(1:Nx-1,2:Ny,2:Nz)     = neigh(1:Nx-1,2:Ny,2:Nz) + undef(2:Nx,1:Ny-1,1:Nz-1) *2;
    neigh(1:Nx-1,2:Ny,1:Nz-1)   = neigh(1:Nx-1,2:Ny,1:Nz-1) + undef(2:Nx,1:Ny-1,2:Nz) *2;
    neigh(1:Nx-1,1:Ny-1,2:Nz)   = neigh(1:Nx-1,1:Ny-1,2:Nz) + undef(2:Nx,2:Ny,1:Nz-1) *2;
    neigh(1:Nx-1,1:Ny-1,1:Nz-1) = neigh(1:Nx-1,1:Ny-1,1:Nz-1) + undef(2:Nx,2:Ny,2:Nz) *2;
    
    % At the borders of the field, there are neighbours not defined. In the
    % edges and corners even more. Add all of them with their weight.
    
    % 1*6+4*3+4*2=26 on the sides
    neigh(1     ,2:Ny-1,2:Nz-1) = neigh(1     ,2:Ny-1,2:Nz-1) + 26;
    neigh(Nx    ,2:Ny-1,2:Nz-1) = neigh(Nx    ,2:Ny-1,2:Nz-1) + 26;
    neigh(2:Nx-1,1     ,2:Nz-1) = neigh(2:Nx-1,1     ,2:Nz-1) + 26;
    neigh(2:Nx-1,Ny    ,2:Nz-1) = neigh(2:Nx-1,Ny,    2:Nz-1) + 26;
    neigh(2:Nx-1,2:Ny-1,1     ) = neigh(2:Nx-1,2:Ny-1,1     ) + 26;
    neigh(2:Nx-1,2:Ny-1,Nz    ) = neigh(2:Nx-1,2:Ny-1,Nz    ) + 26;
    
    % 2*6+7*3+6*2=45 in the edges
    neigh(1 ,1 ,2:Nz-1) = neigh(1 ,1 ,2:Nz-1) + 45;
    neigh(Nx,1 ,2:Nz-1) = neigh(Nx,1 ,2:Nz-1) + 45;
    neigh(1 ,Ny,2:Nz-1) = neigh(1 ,Ny,2:Nz-1) + 45;
    neigh(Nx,Ny,2:Nz-1) = neigh(Nx,Ny,2:Nz-1) + 45;
    neigh(1 ,2:Ny-1,1 ) = neigh(1 ,2:Ny-1,1 ) + 45;
    neigh(Nx,2:Ny-1,1 ) = neigh(Nx,2:Ny-1,1 ) + 45;
    neigh(1 ,2:Ny-1,Nz) = neigh(1 ,2:Ny-1,Nz) + 45;
    neigh(Nx,2:Ny-1,Nz) = neigh(Nx,2:Ny-1,Nz) + 45;
    neigh(2:Nx-1,1 ,1 ) = neigh(2:Nx-1,1 ,1 ) + 45;
    neigh(2:Nx-1,Ny,1 ) = neigh(2:Nx-1,Ny,1 ) + 45;
    neigh(2:Nx-1,1 ,Nz) = neigh(2:Nx-1,1 ,Nz) + 45;
    neigh(2:Nx-1,Ny,Nz) = neigh(2:Nx-1,Ny,Nz) + 45;
    
    % 3*6+9*3+7*2=59 in the corners
    neigh( 1, 1, 1) = neigh( 1, 1, 1) + 59;
    neigh(Nx, 1, 1) = neigh(Nx, 1, 1) + 59;
    neigh( 1,Ny, 1) = neigh( 1,Ny, 1) + 59;
    neigh(Nx,Ny, 1) = neigh(Nx,Ny, 1) + 59;
    neigh( 1, 1,Nz) = neigh( 1, 1,Nz) + 59;
    neigh(Nx, 1,Nz) = neigh(Nx, 1,Nz) + 59;
    neigh( 1,Ny,Nz) = neigh( 1,Ny,Nz) + 59;
    neigh(Nx,Ny,Nz) = neigh(Nx,Ny,Nz) + 59;
end