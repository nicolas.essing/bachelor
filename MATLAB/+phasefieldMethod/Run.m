classdef Run
    % This class represent the evolution of a simulation over the whole
    % simulated time.
    % Properties:
    %   S - current Simulation object
    %   info - a string describing what this run is for
    % Methods:
    %   createFromSimulation
    %   createFromSave
    %   setSaveEvery
    %   evolve
    %   evolveToFraction
    %   evolveTillConverged
    %   getSave
    %   getState
    %   backupParams
    %   backupState
    %   backupSelf
    %   export
    %   printLine
    %   saveAnalysis
    
    properties
        % current state of Simulation
        S;
    end
    
    properties (SetAccess = protected)
        % name of the folder to save in
        folder;
        % settings when to save/...
        interruptEvery = 0;
        saveEvery = 0;
        printEvery = 0;
        exportEvery = 0;
        % settings what to save
        exportField = 1;
        exportGrains = 0;
        exportParams = 0;
        % times of the backups
        param_backup_times = [];
        state_backup_times = [];
        % file for csv infos and index of last export
        csvfile;
        lastExportIndex = 0;
    end
    
    properties (Dependent)
        % An info string about the simulation, why it was done, which
        % parameters were used, ...
        % Saved in info.txt in the save folder.
        info;
    end
    
    methods (Static)
        function R = createFromSimulation(S, saveAt)
            % Create a new object for an existing Simulation.
            % Arguments:
            %   S - The Simulatin object
            %   saveAt - Name of a folder to save in
            R = phasefieldMethod.Run.createFromSimulationCore( ...
                     @phasefieldMethod.Run, S, saveAt);
        end
        
        function R = createFromSave(folder)
            % Load an object from saved data.
            % Arguments:
            %   folder - Name of the folder the data was saved in
            R = phasefieldMethod.Run.createFromSaveCore( ...
                     @phasefieldMethod.Run, folder);
        end
    end
    
    methods (Sealed)
        function R = setSaveEvery(R, save, print, export)
            % Change the time difference for automatic saves, prints and
            % exports.
            % Arguments:
            %   save - Time difference between saves
            %   print - Time difference between prining an info line
            %   export - Time difference between exports
            
            if nargin < 4
                export = R.exportEvery;
                if nargin < 3
                    print = R.printEvery;
                end
            end
            
            assert(isscalar(save) && isnumeric(save) && save>=0, ...
                   'time must be scalar');
            assert(isscalar(print) && isnumeric(print) && print>=0, ...
                   'time must be scalar');
            assert(isscalar(export) && isnumeric(export) && export >= 0, ...
                   'time must be scalar');
            
            R.saveEvery = save;
            R.printEvery = print;
            R.exportEvery = export;
            
            % find minimal value greater 0 (0 if all are zero)
            times = [save, print, export];
            R.interruptEvery = min(times(times>0));
            if isempty(R.interruptEvery)
                R.interruptEvery = 0;
            end
        end
        
        function R = exportSettings(R, phasefield, grains, parameters)
            %EXPORTSETTINGS(phasefield, grains, parameters) - Set what
            %                               information should be exported.
            % Arguments:
            %   phasefield - Boolean, whether to export the phasefield.
            %   grains - Boolean, whether to export the grain indices.
            %   parameters - Boolean, whether to export the parameters.
            
            assert(isscalar(phasefield), 'setting must be scalar');
            assert(isscalar(grains), 'setting must be scalar');
            assert(isscalar(parameters), 'setting must be scalar');
            
            R.exportField = phasefield;
            R.exportGrains = grains;
            R.exportParams = parameters;
        end
        
        function R = evolve(R, time)
            % Evolve a given time, automatically saving and stuff.
            % Arguments:
            %   time - The time to evolve
            
            tillnextsave = (floor(R.S.time/R.interruptEvery) + 1) ...
                      * R.interruptEvery - R.S.time;
            
            % If interruptEvery was 0 (no saves need to be done), the above
            % formula will give NaN, which will lead to the condition for
            % the loop being false and all the time will be "remaining"
            
            while time > tillnextsave
                % Evolve, save, and substract done time from to be done
                [R, done] = R.innerloop(tillnextsave);
                time = time - done;
                % Calculate time till next save
                tillnextsave = (floor(R.S.time/R.interruptEvery) + 1) ...
                      * R.interruptEvery - R.S.time;
            end
            % evolve remaining time
            R.S = R.S.evolve(time);
        end
        
        function R = evolveToFraction(R, fract, maxtime)
            % Evolve unitl the phasefield until a certain fraction of
            % crystallisation is reached
            % Arguments:
            %   fract - Crystallised fraction to evolve to
            %   maxtime - (optional) A maximal time to evolve
            
            if nargin < 3
                maxtime = Inf;
            end
            
            assert(isscalar(maxtime) && isnumeric(maxtime) && ...
                   fract < 1 && fract > 0, 'invalid fraction, must be 0<=f<=1');
            assert(isscalar(maxtime) && isnumeric(maxtime) && maxtime > 0, ...
                   'time must be positive scalar');
            
            % check the convergence every interruptEvery if this is > 0,
            % otherwise every 10 timesteps
            timestep = R.interruptEvery;
            if timestep <= 0
                timestep = 10 * R.S.dtime;
            end
            % Convert maxtime argument from time difference to absolute
            % time
            maxtime = maxtime + R.S.time;
            while (R.S.transformedFraction < fract) && (R.S.time < maxtime)
                R = R.innerloop(timestep);
            end
        end
        
        function R = evolveTillConverged(R, treshold, maxtime)
            % Evolve the phasefield until the change in transformed
            % fraction per time falls under a treshold.
            % Arguments:
            %   treshold - The method will simulate until the change in
            %       transformed fraction falls under this value.
            %   maxtime - (optional) A maximal time to evolve
            
            if nargin < 3
                maxtime = Inf;
                if nargin < 2
                    treshold = 0;
                end
            end
            
            assert(isscalar(treshold) && isnumeric(treshold) && treshold >= 0, ...
                   'invalid fraction, must be 0<=f<=1');
            assert(isscalar(maxtime) && isnumeric(maxtime) && maxtime > 0, ...
                   'time must be positive scalar');
            
            % check the convergence every interruptEvery if this is > 0,
            % otherwise every 10 timesteps
            timestep = R.interruptEvery;
            if timestep <= 0
                timestep = 10 * R.S.dtime;
            end
            % Convert maxtime argument from time difference to absolute
            % time
            maxtime = maxtime + R.S.time;
            % Convert treshold from change per time to change per step
            treshold = treshold*timestep;
            % Save initial transformed fraction
            lastValue = R.S.transformedFraction;
            while (abs(R.S.transformedFraction -lastValue) > treshold) ...
                    && (R.S.time < maxtime)
                R = R.innerloop(timestep);
            end
        end
        
        function S = getSave(R, index)
            % Recreate a saved state of the Simulation
            % Arguments:
            %   i - Index of state save
            % Returns:
            %	S - Simulation
            
           % Open save file and put saved data into the copied Simulation
            statefilename = strcat('state_',num2str(index),'.mat');
            statefile = matfile(fullfile(R.folder,statefilename));
            
            % Read out last saved parameters before that state
            starttime = R.state_backup_times(index);
            [~, index2] = max(R.param_backup_times(R.param_backup_times <= starttime));
            paramfilename = strcat('params_',num2str(index2),'.mat');
            paramfile = matfile(fullfile(R.folder,paramfilename));
            
            S = R.S.fromBackup(paramfile, statefile);
        end
        
        function S = getState(R, time)
            % Recreate the Simulation to an arbitrary time.
            
            % Read out last backup of state and its index
            [~, index] = max(R.state_backup_times( ...
                R.state_backup_times <= time) );
            
            S = R.getSave(index);
            
            % Evolve to requested time
            S = S.evolve(time - S.time);
        end
        
        function R = backupParams(R)
            % Make a backup of the parameters of the simulation and
            % register it in this object.
            % This should be done manually every time they are externally
            % changed.
            
            % create a file to save at
            index = length(R.param_backup_times) + 1;
            filename = strcat('params_',num2str(index),'.mat');
            file = matfile(fullfile(R.folder,filename), 'Writable',true);
            % write to file and register the backup
            R.S.backupParams(file);
            R.param_backup_times = [R.param_backup_times, R.S.time];
        end
        
        function R = backupState(R)
            % Make a backup of the current state of the simulation and
            % register it in this object.
            % This will be done automatically every set timestep when
            % evolving the Simulation via this object, but can be done
            % manually as well.
            
            index = length(R.state_backup_times) + 1;
            filename = strcat('state_',num2str(index),'.mat');
            file = matfile(fullfile(R.folder,filename), 'Writable',true);
            R.S.backupState(file);
            R.state_backup_times = [R.state_backup_times, R.S.time];
        end
        
        function R = backupSelf(R)
            % Make a backup of this object. This saves the current state of
            % the simulation and the list of backups.
            % It should be called before exiting your program.
            % Returns self
            file = matfile(fullfile(R.folder,'meta.mat'),'Writable',true);
            file.param_backup_times = R.param_backup_times;
            file.state_backup_times = R.state_backup_times;
            file.S = R.S;
            file.saveEvery = R.saveEvery;
            file.printEvery = R.printEvery;
            file.exportEvery = R.exportEvery;
            file.lastExportIndex = R.lastExportIndex;
        end
    end
    
    methods
        function R = export(R)
            % Export the current state.
            R.lastExportIndex = R.lastExportIndex + 1;
            if R.exportField
                R.S.exportPhasefield(fullfile(R.folder,'exports'), ...
                           R.lastExportIndex);
            end
            if R.exportGrains
                R.S.exportGrainindices(fullfile(R.folder,'exports'), ...
                           R.lastExportIndex);
            end
            if R.exportParams
                R.S.exportParameters(fullfile(R.folder,'exports'), ...
                           R.lastExportIndex);
            end
        end
        
        function R = printLine(R)
            % Print an info line into the csv file.
            fprintf(R.csvfile, R.S.infoLine());
        end
        
        function info = get.info(R)
            % Construct the filename. If the file exists, return its
            % contents, otherwise an empty string.
            filename = fullfile(R.folder,'info.txt');
            if exist(filename,'file')
                info = fileread(filename);
            else
                info = '';
            end
        end
        
        function R = set.info(R, info)
            file = fopen(fullfile(R.folder,'info.txt'), 'w');
            fwrite(file, info);
            fclose(file);
        end
        
        function R = saveAnalysis(R)
            % Append the current analysis of the Simulation to a file
            file = fopen(fullfile(R.folder,'analysis.txt'), 'a');
            R.S.analyse(file);
            fprintf(file, '\n');
            fprintf(file, '-------------------------------------------\n');
            fprintf(file, '\n');
            fclose(file);
        end
    end
    
    methods (Static, Access = protected)
        function R = createFromSimulationCore(constructor, S, saveAt)
            % Create a new object for an existing Simulation.
            % Arguments:
            %   S - The Simulatin object
            %   saveAt - Name of a folder to save in
            
            assert(isa(S, 'phasefieldMethod.Simulation'), ...
                   'S must be a simulation object');
            assert(~exist(saveAt,'dir'), 'Folder already exists');
            
            R = constructor();
            
            % Fill in arguments
            R.S = S;
            R.folder = saveAt;
            
            % Create folders and files to save at
            mkdir(saveAt);
            mkdir(fullfile(saveAt,'exports'));
            R.csvfile = fopen(fullfile(saveAt,'csv.csv'), 'w');
            
            % Write initial data
            fprintf(R.csvfile, R.S.infoLineHeader());
            R = R.backupParams();
            R = R.backupState();
            R = R.backupSelf();
        end
        
        function R = createFromSaveCore(constructor, folder)
            % Load an object from saved data.
            % Arguments:
            %   folder - Name of the folder the data was saved in
            
            % Create an empty object
            R = constructor();
            
            % Open file and read saved values
            metafile = matfile(fullfile(folder,'meta.mat'));
            R.param_backup_times = metafile.param_backup_times;
            R.state_backup_times = metafile.state_backup_times;
            R.S = metafile.S;
            R.folder = folder;
            R = R.setSaveEvery(metafile.saveEvery, ...
                               metafile.printEvery, metafile.exportEvery);
            R.csvfile = fopen(fullfile(R.folder,'csv.csv'), 'a');
            R.lastExportIndex = metafile.lastExportIndex;
        end
    end
    
    methods (Access = protected)
        function R = Run()
            % The constructor is private to prevent incomplete objects.
            % Objects can be created through static methods.
        end
        
        function [R, donetime] = innerloop(R, time)
            % Evolves the simulation to next save and saves
            % Returns:
            %   R - Updated Run object
            %   donetime - time simulated
            
            % Evolve till there and get actually evolved time
            [R.S, donetime] = R.S.evolve(time);
            % Save/print/export if appropriate time has passed
            % The mod() goes from 0 to saveEvery in steps of
            % interruptEvery, and I want it to save in the last slot, so
            % I shift the time back one slot
            if mod(R.S.time - R.interruptEvery, R.saveEvery) < R.interruptEvery
                R = R.backupState();
            end
            if mod(R.S.time - R.interruptEvery, R.printEvery) < R.interruptEvery
                R = R.printLine();
            end
            if mod(R.S.time - R.interruptEvery, R.exportEvery) < R.interruptEvery
                R = R.export();
            end
        end
    end
end