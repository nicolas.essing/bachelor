% Using the simulation method for a GST example

import materialSimulations.GSTSimulation;
import phasefieldMethod.Run;

% SIMULATION SETTINGS
spacing = 1e-9;         % m
timestepgoodness = 10;
nucleationEvery = 3e-4;
grainEvery = 5e-3;
saveEvery = 100;
printEvery = 10;
exportEvery = 25;
foldername = '../data/gst_470K_struct';

S = GSTSimulation(spacing, 42);

% PHYSICAL CONSTANTS
S.DIT = 0.5e-9;        % diffuse interface thickness, m
S.T = 470;

phasefield = zeros(400,400,50,'single','gpuArray');
phasefield(150:250,150:250,1:10) = NaN;
S = S.setField(phasefield);

% Calculate internal parameters from the settings above, use them to
% determine an appropriate time resolution and apply the changes coming
% from the new time step.
S = S.apply();
S = S.adjustTimestep(timestepgoodness);
S = S.apply();

% Enable nucleation and grain tracking
S = S.settings(S.dtime, nucleationEvery);
S = S.grainSettings(S.dtime, grainEvery);

S.analyse();

%%
R = Run.createFromSimulation(S, foldername);
R = R.setSaveEvery(saveEvery*S.dtime,printEvery*S.dtime,exportEvery*S.dtime);

%%
R = R.evolveToFraction(.95);
R.backupSelf();
