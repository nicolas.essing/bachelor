% Testing my calculations on tau. Here I show that the overall difference
% to the equilibrium shape goes exponentially to zero, with the factor
% being (at least related to) tau.

% Normalised parameters
spacing = 1;
DIT = 4;
taus = [0.5, 2, 8];
evolvetimes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.7, 0.9, 1.1, 1.4, 1.7, 2, 2.5, 3];

% Initial interface form, use a steep or a wider interface compared to the
% eqilibrium shape.
X = 1:100;
initialfield = 1. * (X<50);
% initialfield = 1/2*(1+tanh((50-X)./(3*2*DIT)));

% Build a simulation with the initial field and some initial parameters
import phasefieldMethod.ExampleSimulation
S = ExampleSimulation(spacing, 42);
S = S.apply();
S = S.setField(initialfield);

% Calculate the equilibrium field for comparison and the difference of this
% field to the initial one.
finalfield = 1/2*(1+tanh((49.5-X)./(2*DIT)));
initialdiff = sum(abs(initialfield-finalfield));

% For each tau, calculate the difference for all required times
alldiffs = cell(1,length(taus));
alltimes = cell(1,length(taus));
for i=1:length(taus)
    % Put this tau into a simulation
    tau = taus(i);
    Si = S.setOtherParameters(DIT, 0, tau);
    Si = Si.adjustTimestep(2);
    Si = Si.apply();
    % The times to stop at
%     dts = diff([0,evolvetaus]) .* taus;
    dts = diff([0,evolvetimes]);
    % Initialise space for the differences and times
    diffs = zeros(1,length(dts)+1);
    times = zeros(1,length(dts)+1);
    % Fill in data from first point
    diffs(1) = initialdiff;
    times(1) = 0;
    for j=1:length(dts)
        % Simulate and save
        dt = dts(j);
        Si = Si.evolve(dt);
        diffs(j+1) = gather(sum(abs(Si.phasefield - finalfield)));
        times(j+1) = Si.time;
    end
    % Save results from this tau
    alldiffs{i} = diffs;
    alltimes{i} = times;
end

hold on;
for i=1:length(taus)
    plot(alltimes{i}, alldiffs{i}, 'LineWidth',1);
end
xlabel('t','FontSize',16);
ylabel('$||\Phi-\Phi_{eq}||$','Interpreter','latex','FontSize',16);
set(gca,'FontSize',16);
legend(num2str(taus'));
